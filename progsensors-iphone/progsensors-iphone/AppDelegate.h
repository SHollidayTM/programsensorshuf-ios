//
//  AppDelegate.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/23/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VCMain.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    VCMain *vcMain;
}

@property (strong, nonatomic) UIWindow *window;


@end

