//
//  ModuleGetSensors.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 1/27/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "ModuleGetKey.h"
#import "ModuleParseAndReturnProds.h"

@interface ModuleGetProdsByConfig : NSObject{
    NSString *configID;
    BOOL attemptedOnce;
}

+ (id)sharedInstance;
- (void)go:(NSString *)sentConfigID;

@end
