//
//  ModuleGetSensors.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 1/27/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "ModuleGetProdsByConfig.h"

@implementation ModuleGetProdsByConfig

+ (id)sharedInstance{
    static ModuleGetProdsByConfig *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)go:(NSString *)sentConfigID{
    attemptedOnce = NO;
    configID = sentConfigID;
    [self tryToConnect];
}

- (void)tryToConnect{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@Product/ByVehicle", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"apiUrl"]]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *securityKey = [[NSUserDefaults standardUserDefaults] stringForKey:@"securityKey"];
    //NSString *body = [NSString stringWithFormat:@"{\"securityKey\":\"%@\",\"configID\":%@,\"manufacturers\":[\"Huf\",\"Huf (formerly Beru)\"]}", securityKey, configID];
    NSString *body = [NSString stringWithFormat:@"{\"securityKey\":\"%@\",\"configID\":%@}", securityKey, configID];
    [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        if(error){
            [self tryAgain];
            return;
        }
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse *)response;
        if(httpResp.statusCode != 200){
            [self tryAgain];
            return;
        }else{
            NSJSONSerialization *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            [[ModuleParseAndReturnProds sharedInstance] parseAndReturn:json];
        }
    }];
    [task resume];
}

- (void)tryAgain{
    if(!attemptedOnce){
        attemptedOnce = YES;
        [[ModuleGetKey sharedInstance] getSecurityKey:^(BOOL success){
            if(success){
                [self tryToConnect];
            }else{
                NSDictionary *dic = @{@"text":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_errorTryAgain"]};
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnError" object:dic];
            }
        }];
    }else{
        NSDictionary *dic = @{@"text":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_errorTryAgain"]};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnError" object:dic];
    }
}

@end
