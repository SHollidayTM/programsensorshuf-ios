//
//  Sensor.h
//  alligator-iphone
//
//  Created by Scott Holliday on 10/26/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SensorTPMS : NSObject

@property (assign) BOOL programmable;
@property (nonatomic, strong) NSString *catalogID;
@property (nonatomic, strong) NSString *partNumber;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *sameAsNumber;
@property (nonatomic, strong) NSString *alternateNumber;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *relatedPartNumber;
@property (nonatomic, strong) NSString *manufacturer;
@property (nonatomic, strong) NSString *frequency;
@property (nonatomic, strong) NSString *torqueNut;
@property (nonatomic, strong) NSString *torqueScrew;
@property (nonatomic, strong) NSString *torqueCore;
@property (nonatomic, strong) NSString *housingColor;
@property (nonatomic, strong) NSString *type;
@property (nonatomic, strong) NSString *partDesign;
@property (nonatomic, strong) NSString *valveMaterial;
@property (nonatomic, strong) NSString *shape;
@property (nonatomic, strong) NSString *attCategory;
@property (nonatomic, strong) NSString *idLength;
@property (nonatomic, strong) NSString *idType;
@property (nonatomic, strong) NSString *madeFor;

@end
