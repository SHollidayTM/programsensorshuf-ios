//
//  Action.h
//  progsensors-ipad
//
//  Created by Scott Holliday on 5/31/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Action : NSObject

@property (assign) int configID;
@property (assign) int actionID;

@end
