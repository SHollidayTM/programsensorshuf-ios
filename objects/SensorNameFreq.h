//
//  SensorNameFreq.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/15/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SensorNameFreq : NSObject

@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *frequency;
@property (nonatomic, strong) NSString *valveMaterial;

@end
