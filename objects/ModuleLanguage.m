//
//  ModuleLanguage.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 5/6/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "ModuleLanguage.h"

@implementation ModuleLanguage

+ (id)sharedInstance{
    static ModuleLanguage *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (id)init{
    if(self = [super init]){
        NSString *language;
        if(![[NSUserDefaults standardUserDefaults] integerForKey:@"language"] || [[NSUserDefaults standardUserDefaults] integerForKey:@"language"] == 0){
            language = [[[NSLocale preferredLanguages] objectAtIndex:0] substringToIndex:2];
        }else{
            switch([[NSUserDefaults standardUserDefaults] integerForKey:@"language"]){
                case 1:
                    language = @"en";
                    break;
                case 2:
                    language = @"es";
                    break;
                case 3:
                    language = @"fr";
                    break;
            }
        }
        NSString *strPath = [[NSBundle mainBundle] pathForResource:language ofType:@"xml"];
        if(!strPath){
            strPath = [[NSBundle mainBundle] pathForResource:@"en" ofType:@"xml"];
        }
        NSData *langData = [NSData dataWithContentsOfFile:strPath];
        NSXMLParser *parser = [[NSXMLParser alloc] initWithData:langData];
        [parser setDelegate:self];
        [parser parse];
    }
    return self;
}

- (void)parserDidStartDocument:(NSXMLParser *)parser{
    _phrases = [[NSMutableDictionary alloc] init];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict{
    if([elementName isEqualToString:@"string"]){
        currAtttribute = [attributeDict objectForKey:@"name"];
    }else{
        currAtttribute = nil;
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string{
    if(currAtttribute){
        string = [string stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
        if([string length] > 0){
            [_phrases setValue:string forKey:currAtttribute];
        }
    }
}

@end
