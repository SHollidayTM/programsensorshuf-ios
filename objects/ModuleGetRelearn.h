//
//  ModuleGetRelearns.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/7/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModuleLanguage.h"
#import "ModuleGetKey.h"
#import "Relearn.h"

@interface ModuleGetRelearn : NSObject{
    NSString *configID;
    BOOL attemptedOnce;
    BOOL alreadySent;
}

+ (id)sharedInstance;
- (void)go:(NSString *)sentConfigID;

@end
