//
//  ModuleGetNewVehicles.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/5/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "ModuleGetNewVehicles.h"

@implementation ModuleGetNewVehicles

+ (id)sharedInstance{
    static ModuleGetNewVehicles *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)go{
    attemptedOnce = NO;
    attachments = [[NSMutableArray alloc] init];
    vehicles = [[NSMutableArray alloc] init];
    [self tryToConnect];
}

- (void)tryToConnect{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@Vehicle/GetNew", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"apiUrl"]]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *securityKey = [[NSUserDefaults standardUserDefaults] stringForKey:@"securityKey"];
    NSString *body = [NSString stringWithFormat:@"{\"securityKey\":\"%@\",\"regionCode\":\"NorthAmerica\",\"minChangeDate\":\"%@\",\"programmableOnly\":1}", securityKey, [[NSUserDefaults standardUserDefaults] stringForKey:@"newVehicleDate"]];
    [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    [request setTimeoutInterval:600];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        if(error){
            [self tryAgain];
            return;
        }
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse *)response;
        if(httpResp.statusCode != 200){
            [self tryAgain];
            return;
        }else{
            NSDictionary *wholeThing = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            NSArray *arrAttachments = [wholeThing objectForKey:@"Attachments"];
            for(int i = 0; i < [arrAttachments count]; i++){
                NSDictionary *dic = [arrAttachments objectAtIndex:i];
                Attachment *attachment = [[Attachment alloc] init];
                [attachment setAttachmentID:[[dic objectForKey:@"AttachmentID"] intValue]];
                [attachment setLevel:[dic objectForKey:@"Level"]];
                [attachment setName:[dic objectForKey:@"Name"]];
                [attachment setText:[dic objectForKey:@"Base64Text"]];
                [attachment setUrl:[dic objectForKey:@"DocumentUrl"]];
                [attachments addObject:attachment];
            }
            NSArray *arrVehicles = [wholeThing objectForKey:@"Configs"];
            for(int i = 0; i < [arrVehicles count]; i++){
                NSDictionary *dic = [arrVehicles objectAtIndex:i];
                Vehicle *vehicle = [[Vehicle alloc] init];
                [vehicle setConfigID:[dic objectForKey:@"VehicleConfigurationID"]];
                [vehicle setYear:[dic objectForKey:@"AcesYearID"]];
                [vehicle setMakeID:[dic objectForKey:@"AcesMakeID"]];
                [vehicle setModelID:[dic objectForKey:@"AcesModelID"]];
                [vehicle setMakeName:[dic objectForKey:@"MakeName"]];
                [vehicle setModelName:[dic objectForKey:@"ModelName"]];
                [vehicle setQualifierID:@""];
                [vehicle setQualifier:@""];
                if(![[dic objectForKey:@"QualifierID"] isKindOfClass:[NSNull class]]){
                    [vehicle setQualifierID:[dic objectForKey:@"QualifierID"]];
                    [vehicle setQualifier:[dic objectForKey:@"QualifierText"]];
                }
                [vehicle setIndirect:[[dic objectForKey:@"IndirectSystemFlag"] intValue]];
                NSArray *arrCommands = @[[dic objectForKey:@"Commands"]];
                vehicle.commandWords = [[NSMutableArray alloc] init];
                for(int x = 0; x < [arrCommands count]; x++){
                    NSDictionary *command = [arrCommands objectAtIndex:x];
                    CommandWord *cmdWord = [[CommandWord alloc] init];
                    unsigned result = 0;
                    NSScanner *scanner;
                    if(![[command objectForKey:@"CheckCommand"] isKindOfClass:[NSNull class]]){
                        [cmdWord setCheckWord:[[command objectForKey:@"CheckCommand"] intValue]];
                    }
                    if(![[command objectForKey:@"SelectCommand"] isKindOfClass:[NSNull class]]){
                        scanner = [NSScanner scannerWithString:[command objectForKey:@"SelectCommand"]];
                        [scanner scanHexInt:&result];
                        [cmdWord setSelectWord:result];
                    }
                    if(![[command objectForKey:@"SoftwareVersion"] isKindOfClass:[NSNull class]]){
                        scanner = [NSScanner scannerWithString:[command objectForKey:@"SoftwareVersion"]];
                        [scanner scanHexInt:&result];
                        [cmdWord setSoftwareVersion:result];
                    }
                    [vehicle.commandWords addObject:cmdWord];
                }
                NSArray *arrAttachIDs = [dic objectForKey:@"AttachmentIDs"];
                vehicle.attachmentIDs = [[NSMutableArray alloc] init];
                for(int x = 0; x < [arrAttachIDs count]; x++){
                    [vehicle.attachmentIDs addObject:[arrAttachIDs objectAtIndex:x]];
                }
                vehicle.sensorNameFreqs = [[NSMutableArray alloc] init];
                if(![[dic objectForKey:@"Sensors"] isKindOfClass:[NSNull class]]){
                    NSArray *arrSensors = [dic objectForKey:@"Sensors"];
                    for(int x = 0; x < [arrSensors count]; x++){
                        NSDictionary *sensor = [arrSensors objectAtIndex:x];
                        SensorNameFreq *sensorNameFreq = [[SensorNameFreq alloc] init];
                        [sensorNameFreq setName:[sensor objectForKey:@"SensorName"]];
                        if(![[sensor objectForKey:@"Frequency"] isKindOfClass:[NSNull class]]){
                            NSString *freq = [[sensor objectForKey:@"Frequency"] stringByReplacingOccurrencesOfString:@" MHz" withString:@""];
                            [sensorNameFreq setFrequency:freq];
                            NSString *fileName = [NSString stringWithFormat:@"%@.jpg", [sensorNameFreq name]];
                            NSFileManager *mgrFile = [NSFileManager defaultManager];
                            NSString *strDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                            NSString *strPath = [strDir stringByAppendingPathComponent:fileName];
                            if(![mgrFile fileExistsAtPath:strPath]){
                                UIImage *img = [UIImage imageNamed:fileName];
                                if(img){
                                    [UIImagePNGRepresentation(img) writeToFile:strPath atomically:NO];
                                }else{
                                    NSString *imageUrl = [sensor objectForKey:@"SensorImageUrl"];
                                    if(![imageUrl isKindOfClass:[NSNull class]]){
                                        NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
                                        [imgData writeToFile:strPath atomically:NO];
                                    }
                                }
                            }
                            [vehicle.sensorNameFreqs addObject:sensorNameFreq];
                        }
                    }
                }
                [vehicles addObject:vehicle];
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnAttachments" object:attachments];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnVehicles" object:vehicles];
        }
    }];
    [task resume];
}

- (void)tryAgain{
    if(!attemptedOnce){
        attemptedOnce = YES;
        [[ModuleGetKey sharedInstance] getSecurityKey:^(BOOL success){
            if(success){
                [self tryToConnect];
            }else{
                NSDictionary *dic = @{@"text":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_errorTryAgain"]};
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnVehiclesError" object:dic];
            }
        }];
    }else{
        NSDictionary *dic = @{@"text":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_errorTryAgain"]};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnVehiclesError" object:dic];
    }
}

@end
