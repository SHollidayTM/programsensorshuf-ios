//
//  ModuleGetKey.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 3/29/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModuleGetKey : NSObject

+ (id)sharedInstance;
- (void)getSecurityKey:(void (^)(BOOL success))callback;

@end
