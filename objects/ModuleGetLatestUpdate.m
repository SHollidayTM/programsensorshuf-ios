//
//  ModuleGetLatestUpdate.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 5/26/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "ModuleGetLatestUpdate.h"

@implementation ModuleGetLatestUpdate

+ (id)sharedInstance{
    static ModuleGetLatestUpdate *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)go{
    attemptedOnce = NO;
    [self tryToConnect];
}

- (void)tryToConnect{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@Updates/LastUpdateDate", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"apiUrl"]]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *securityKey = [[NSUserDefaults standardUserDefaults] stringForKey:@"securityKey"];
    NSString *body = [NSString stringWithFormat:@"{\"securityKey\":\"%@\",\"regionCode\":\"NorthAmerica\"}", securityKey];
    [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        if(error){
            [self tryAgain];
            return;
        }
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse *)response;
        if(httpResp.statusCode != 200){
            [self tryAgain];
            return;
        }else{
            NSString *strDate = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
            NSDateFormatter *dFormat = [[NSDateFormatter alloc] init];
            [dFormat setDateFormat:@"yyyy-MM-dd"];
            NSDate *lastUpdate = [dFormat dateFromString:[strDate substringWithRange:NSMakeRange(1, 10)]];
            NSDate *localDate = [dFormat dateFromString:[[NSUserDefaults standardUserDefaults] stringForKey:@"newVehicleDate"]];
            NSDictionary *dic = @{@"lastUpdate":lastUpdate, @"localDate":localDate};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"LatestUpdate" object:dic];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
        }
    }];
    [task resume];
}

- (void)tryAgain{
    if(!attemptedOnce){
        attemptedOnce = YES;
        [[ModuleGetKey sharedInstance] getSecurityKey:^(BOOL success){
            if(success){
                [self tryToConnect];
            }else{
                NSDictionary *dic = @{@"text":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_errorTryAgain"]};
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnError" object:dic];
            }
        }];
    }else{
        NSDictionary *dic = @{@"text":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_errorTryAgain"]};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnError" object:dic];
    }
}

@end
