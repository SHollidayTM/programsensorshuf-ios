//
//  VINToYMMModule.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 12/7/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "ModuleVINToYMM.h"

@implementation ModuleVINToYMM

+ (id)sharedInstance{
    static ModuleVINToYMM *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)returnYMM:(NSString *)sentVIN{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:@"http://services.chromedata.com/Description/7a?wsdl"]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"text/xml;charset=utf-8" forHTTPHeaderField:@"Content-Type"];
    NSString *soapMsg = [NSString stringWithFormat:@"<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:urn=\"urn:description7a.services.chrome.com\"><soapenv:Header/><soapenv:Body><urn:VehicleDescriptionRequest><urn:accountInfo number=\"283234\" secret=\"845b638aea2d4f37\" country=\"US\" language=\"en\" behalfOf=\"?\"/><urn:vin>%@</urn:vin></urn:VehicleDescriptionRequest></soapenv:Body></soapenv:Envelope>", sentVIN];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[soapMsg length]];
    [request setHTTPBody: [soapMsg dataUsingEncoding:NSUTF8StringEncoding]];
    [request addValue:msgLength forHTTPHeaderField:@"Content-Length"];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        NSDictionary *dic;
        if(error){
            dic = @{@"error":error};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ByYMMFromVIN" object:dic];
            return;
        }
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse*)response;
        if(httpResp.statusCode != 200){
            dic = @{@"error":[NSString stringWithFormat:@"Return code %ld.", (long)httpResp.statusCode]};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ByYMMFromVIN" object:dic];
        }else{
            year = nil;
            make = nil;
            model = nil;
            NSXMLParser *parser = [[NSXMLParser alloc] initWithData:data];
            [parser setDelegate:self];
            [parser parse];
        }
    }];
    [task resume];
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict{
    if([elementName isEqualToString:@"vinDescription"]){
        year = [attributeDict objectForKey:@"modelYear"];
        make = [attributeDict objectForKey:@"division"];
        model = [attributeDict objectForKey:@"modelName"];
    }
}

- (void)parserDidEndDocument:(NSXMLParser *)parser{
    if(year && make && model){
        NSDictionary *dic = @{@"year":year, @"make":make, @"model":model};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ByYMMFromVIN" object:dic];
    }else{
        NSDictionary *dic = @{@"error":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_noResults"]};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ByYMMFromVIN" object:dic];
    }
}

- (void)parser:(NSXMLParser *)parser parseErrorOccurred:(NSError *)parseError{
    NSDictionary *dic = @{@"error":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_errorTryAgain"]};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ByYMMFromVIN" object:dic];
}

@end
