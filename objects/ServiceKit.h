//
//  ServiceKit.h
//  alligator-iphone
//
//  Created by Scott Holliday on 10/26/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ServiceKit : NSObject

@property (nonatomic, strong) NSString *catalogID;
@property (nonatomic, strong) NSString *partNumber;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *category;
@property (nonatomic, strong) NSString *sameAsNumber;
@property (nonatomic, strong) NSString *alternateNumber;
@property (nonatomic, strong) NSString *desc;
@property (nonatomic, strong) NSString *relatedPartNumber;
@property (nonatomic, strong) NSString *manufacturer;
@property (nonatomic, strong) NSString *finish;
@property (nonatomic, strong) NSString *cap;
@property (nonatomic, strong) NSString *hexNut;
@property (nonatomic, strong) NSString *metalWasher;
@property (assign) BOOL metalValveIncluded;
@property (assign) BOOL completeValveAssembly;
@property (nonatomic, strong) NSString *core;
@property (nonatomic, strong) NSString *torqueCore;
@property (nonatomic, strong) NSString *torqueNut;
@property (nonatomic, strong) NSString *torqueScrew;
@property (nonatomic, strong) NSString *grommet;
@property (nonatomic, strong) NSString *valveLength;

@end
