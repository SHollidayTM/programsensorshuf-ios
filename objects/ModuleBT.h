//
//  ModuleBT.h
//  progsensors-ipad
//
//  Created by Scott Holliday on 5/13/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreBluetooth/CoreBluetooth.h>
#import "ModuleLanguage.h"
#import "BTObject.h"
#import "CommandWord.h"

@interface ModuleBT : NSObject <CBCentralManagerDelegate, CBPeripheralDelegate>{
    CBCentralManager *cbMgr;
    NSTimer *tmrSearch;
    int counter;
    CBPeripheral *cbpDeviceHC;
    CBPeripheral *cbpDeviceOBD;
    NSString *NS_WRITE_UUID;
    NSString *NS_READ_UUID;
    CBCharacteristic *chrReadHC;
    CBCharacteristic *chrWriteHC;
    CBCharacteristic *chrReadOBD;
    CBCharacteristic *chrWriteOBD;
    int cmdWordsCount;
    NSArray *cmdWords;
    CommandWord *currCmdWord;
    uint8_t mhz;
    BOOL checkingFreq;
    NSData *writeData;
    uint8_t iByte0;
    uint8_t iByte1;
    uint8_t iByte2;
    uint8_t iByte3;
    uint8_t pByte;
    uint8_t tByte;
    uint8_t bByte;
    NSMutableData *readingData;
    NSData *resultData;
    BOOL copy;
    BOOL copySet;
    BOOL writing;
    BOOL searchingOBD;
    BOOL activateWithTool;
    BOOL canceled;
    NSData *fileData;
    int partCounter;
    int partTotal;
    UInt8 *totalParts;
    BOOL chunking;
    int chunkCount;
    int chunkLength;
    NSData *chunkData;
    BOOL checkingFirmware;
    NSString *fwVersion;
}

@property (nonatomic, strong) NSMutableArray *btObjects;
@property (nonatomic, strong) BTObject *btObjectHC;
@property (nonatomic, strong) BTObject *btObjectOBD;
@property (nonatomic, strong) NSArray *datIds;
@property (assign) BOOL creating;

+ (id)sharedInstance;
- (void)searchForDevices:(NSData *)dataBTObject obd:(BOOL)sentOBD;
- (void)stopSearching;
- (void)checkSensor:(NSArray *)sentCmdWords mhz:(uint8_t)sentMhz chk:(BOOL)sentCheckingFreq;
- (void)createSensor:(NSArray *)sentCmdWords mhz:(uint8_t)sentMhz;
- (void)copySensor:(NSArray *)sentCmdWords mhz:(uint8_t)sentMhz;
- (void)copySetSensor:(NSArray *)sentCmdWords mhz:(uint8_t)sentMhz;
- (void)createFromSet:(NSData *)datId;
- (void)updateFirmware;
- (void)discoverOBDProtocol;
- (void)doRelearn;
- (void)cancel;

@end
