//
//  ModuleBT.m
//  progsensors-ipad
//
//  Created by Scott Holliday on 5/13/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "ModuleBT.h"

@implementation ModuleBT

+ (id)sharedInstance{
    static ModuleBT *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)searchForDevices:(NSData *)dataBTObject obd:(BOOL)sentOBD{
    searchingOBD = sentOBD;
    if(!searchingOBD){
        if(dataBTObject){
            _btObjectHC = [NSKeyedUnarchiver unarchiveObjectWithData:dataBTObject];
        }
    }
    NS_WRITE_UUID = @"2A1C";
    NS_READ_UUID = @"2A1E";
    if(!cbMgr){
        cbMgr = [[CBCentralManager alloc] initWithDelegate:self queue:nil];
    }else{
        _btObjects = [[NSMutableArray alloc] init];
        [cbMgr scanForPeripheralsWithServices:nil options:nil];
    }
    counter = 0;
    tmrSearch = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timeoutSearch) userInfo:nil repeats:YES];
}

- (void)timeoutSearch{
    counter++;
    if(counter > 9){
        [tmrSearch invalidate];
        [self stopSearching];
    }
}

- (void)centralManagerDidUpdateState:(CBCentralManager *)central{
    NSString *strMsg = @"";
    switch([central state]){
        case CBCentralManagerStatePoweredOff:
            strMsg = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_blePoweredOff"];
            break;
        case CBCentralManagerStatePoweredOn:{
            _btObjects = [[NSMutableArray alloc] init];
            [cbMgr scanForPeripheralsWithServices:nil options:nil];
            break;
        }
        case CBCentralManagerStateResetting:
            strMsg = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_bleResetting"];
            break;
        case CBCentralManagerStateUnauthorized:
            strMsg = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_bleUnauthorized"];
            break;
        case CBCentralManagerStateUnknown:
            strMsg = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_bleUnknown"];
            break;
        case CBCentralManagerStateUnsupported:
            strMsg = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_bleUnsupported"];
            break;
    }
    if([strMsg length] > 0){
        if(tmrSearch){
            [tmrSearch invalidate];
        }
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:strMsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:ctrAlert animated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
    }
}

- (void)centralManager:(CBCentralManager *)central didDiscoverPeripheral:(CBPeripheral *)peripheral advertisementData:(NSDictionary *)advertisementData RSSI:(NSNumber *)RSSI{
    if([[[peripheral name] substringToIndex:2] isEqualToString:@"DT"] || [[[peripheral name] substringToIndex:2] isEqualToString:@"RG"]){
        BTObject *btObject = [[BTObject alloc] init];
        [btObject setPeripheral:peripheral];
        if(!searchingOBD){
            //if([[[peripheral name] substringToIndex:2] isEqualToString:@"DT"]){
                [btObject setName:[[peripheral name] stringByReplacingOccurrencesOfString:[[peripheral name] substringToIndex:2] withString:@"HC"]];
                [btObject setObdDevice:NO];
                [_btObjects addObject:btObject];
            //}
            [self stopSearching];
            //if(_btObjectHC){
            //    if([[btObject name] isEqualToString:[_btObjectHC name]]){
            //        [self stopSearching];
            //    }
            //}
        }else{
            if([[[peripheral name] substringToIndex:2] isEqualToString:@"DB"]){
                [btObject setName:[[peripheral name] stringByReplacingOccurrencesOfString:[[peripheral name] substringToIndex:2] withString:@"OBD"]];
                [btObject setObdDevice:YES];
                [_btObjects addObject:btObject];
            }
        }
    }
}

- (void)stopSearching{
    if(tmrSearch){
        [tmrSearch invalidate];
    }
    [cbMgr stopScan];
    if([_btObjects count] > 0){
        if([_btObjects count] > 1){
            UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_chooseDevice"] message:nil preferredStyle:UIAlertControllerStyleAlert];
            for(BTObject *btObject in _btObjects){
                UIAlertAction *act = [UIAlertAction actionWithTitle:[btObject name] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] dismissViewControllerAnimated:YES completion:nil];
                    if(!searchingOBD){
                        _btObjectHC = btObject;
                        cbpDeviceHC = [_btObjectHC peripheral];
                        [cbMgr connectPeripheral:cbpDeviceHC options:nil];
                    }else{
                        _btObjectOBD = btObject;
                        cbpDeviceOBD = [_btObjectOBD peripheral];
                        [cbMgr connectPeripheral:cbpDeviceOBD options:nil];
                    }
                }];
                [ctrAlert addAction:act];
            }
            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:ctrAlert animated:YES completion:nil];
        }else{
            if(!searchingOBD){
                _btObjectHC = [_btObjects objectAtIndex:0];
                cbpDeviceHC = [_btObjectHC peripheral];
                [cbMgr connectPeripheral:cbpDeviceHC options:nil];
            }else{
                _btObjectOBD = [_btObjects objectAtIndex:0];
                cbpDeviceOBD = [_btObjectOBD peripheral];
                [cbMgr connectPeripheral:cbpDeviceOBD options:nil];
            }
        }
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_scanUnsuccessful"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:ctrAlert animated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
    }
}

- (void)centralManager:(CBCentralManager *)central didRetrieveConnectedPeripherals:(NSArray *)peripherals{
}

- (void)centralManager:(CBCentralManager *)central didRetrievePeripherals:(NSArray *)peripherals{
}

- (void)centralManager:(CBCentralManager *)central didFailToConnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    if(!searchingOBD){
        _btObjectHC = nil;
    }else{
        _btObjectOBD = nil;
    }
    [self connectError];
}

- (void)centralManager:(CBCentralManager *)central didConnectPeripheral:(CBPeripheral *)peripheral{
    if(!searchingOBD){
        [_btObjectHC setConnected:YES];
        [cbpDeviceHC setDelegate:self];
        [cbpDeviceHC discoverServices:nil];
        readingData = [[NSMutableData alloc] init];
        NSData *dataBTObject = [NSKeyedArchiver archivedDataWithRootObject:_btObjectHC];
        [[NSUserDefaults standardUserDefaults] setObject:dataBTObject forKey:@"btObject"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"BTConnected" object:nil];
    }else{
        [_btObjectOBD setConnected:YES];
        [cbpDeviceOBD setDelegate:self];
        [cbpDeviceOBD discoverServices:nil];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverServices:(NSError *)error{
    if(error){
        [self connectError];
        return;
    }
    for(CBService *service in [peripheral services]){
        [peripheral discoverCharacteristics:nil forService:service];
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didDiscoverCharacteristicsForService:(CBService *)service error:(NSError *)error{
    if(error){
        [self connectError];
        return;
    }
    for(CBCharacteristic *characteristic in [service characteristics]){
        [peripheral setNotifyValue:YES forCharacteristic:characteristic];
        if([[characteristic UUID] isEqual:[CBUUID UUIDWithString:NS_READ_UUID]]){
            if(!searchingOBD){
                chrReadHC = characteristic;
            }else{
                chrReadOBD = characteristic;
            }
        }else if([[characteristic UUID] isEqual:[CBUUID UUIDWithString:NS_WRITE_UUID]]){
            if(!searchingOBD){
                chrWriteHC = characteristic;
            }else{
                chrWriteOBD = characteristic;
            }
        }
    }
    UIAlertController *ctrAlert;
    if(!searchingOBD){
        ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_btSettings"] message:[NSString stringWithFormat:@"You are currently connected to %@.", [_btObjectHC name]] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] dismissViewControllerAnimated:YES completion:nil];
            if(chrWriteHC){
                [self checkFirmware];
            }
        }];
        [ctrAlert addAction:actOk];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
    }else{
        ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_btSettings"] message:[NSString stringWithFormat:@"You are currently connected to %@.", [_btObjectOBD address]] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] dismissViewControllerAnimated:YES completion:nil];
            NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_performingRelearn"],  @"hideCancel":@1};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:dic];
            [self pingOBD];
        }];
        [ctrAlert addAction:actOk];
    }
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:ctrAlert animated:YES completion:nil];
}

- (void)checkFirmware{
    fwVersion = @"960";
    checkingFirmware = YES;
    uint8_t bytes[] = {0x8E, 0x00, 0x05, 0x9B, 0x2E};
    NSData *datWrite = [NSData dataWithBytes:bytes length:5];
    [cbpDeviceHC writeValue:datWrite forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
    NSLog(@"TX: %@", datWrite);
}

- (void)checkSensor:(NSArray *)sentCmdWords mhz:(uint8_t)sentMhz chk:(BOOL)sentCheckingFreq{
    copy = NO;
    copySet = NO;
    cmdWordsCount = 0;
    cmdWords = sentCmdWords;
    mhz = sentMhz;
    checkingFreq = sentCheckingFreq;
    _creating = NO;
    currCmdWord = [cmdWords objectAtIndex:0];
    canceled = NO;
    [self doCheck];
}

- (void)createSensor:(NSArray *)sentCmdWords mhz:(uint8_t)sentMhz{
    copy = NO;
    copySet = NO;
    cmdWordsCount = 0;
    cmdWords = sentCmdWords;
    mhz = sentMhz;
    checkingFreq = NO;
    _creating = YES;
    currCmdWord = [cmdWords objectAtIndex:0];
    canceled = NO;
    uint8_t bytes[] = {0x8E, 0x00, 0x06, 0x80, 0x03, 0x17};
    NSData *datWrite = [NSData dataWithBytes:bytes length:6];
    [cbpDeviceHC writeValue:datWrite forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
    NSLog(@"TX: %@", datWrite);
}

- (void)copySensor:(NSArray *)sentCmdWords mhz:(uint8_t)sentMhz{
    copy = YES;
    copySet = NO;
    cmdWordsCount = 0;
    cmdWords = sentCmdWords;
    mhz = sentMhz;
    checkingFreq = NO;
    _creating = NO;
    currCmdWord = [cmdWords objectAtIndex:0];
    canceled = NO;
    [self doCheck];
}

- (void)copySetSensor:(NSArray *)sentCmdWords mhz:(uint8_t)sentMhz{
    copy = NO;
    copySet = YES;
    cmdWordsCount = 0;
    cmdWords = sentCmdWords;
    mhz = sentMhz;
    checkingFreq = NO;
    _creating = NO;
    currCmdWord = [cmdWords objectAtIndex:0];
    canceled = NO;
    [self doCheck];
}

- (void)doCheck{
    uint8_t checkWord = [currCmdWord checkWord];
    uint8_t sum = 0x8E + 0x00 + 0x07 + 0x60 + checkWord + mhz;
    uint8_t bytes[] = {0x8E, 0x00, 0x07, 0x60, checkWord, mhz, sum};
    NSData *datWrite = [NSData dataWithBytes:bytes length:7];
    [cbpDeviceHC writeValue:datWrite forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
    NSLog(@"TX: %@", datWrite);
}

- (void)createFromCopy{
    canceled = NO;
    _creating = YES;
    uint8_t bytes[] = {0x8E, 0x00, 0x06, 0x80, 0x03, 0x17};
    NSData *datWrite = [NSData dataWithBytes:bytes length:6];
    [cbpDeviceHC writeValue:datWrite forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
    NSLog(@"TX: %@", datWrite);
}

- (void)createFromSet:(NSData *)datId{
    const uint8_t *iBytes = (const uint8_t *)[datId bytes];
    iByte0 = iBytes[0];
    iByte1 = iBytes[1];
    iByte2 = iBytes[2];
    iByte3 = iBytes[3];
    canceled = NO;
    _creating = YES;
    uint8_t bytes[] = {0x8E, 0x00, 0x06, 0x80, 0x03, 0x17};
    NSData *datWrite = [NSData dataWithBytes:bytes length:6];
    [cbpDeviceHC writeValue:datWrite forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
    NSLog(@"TX: %@", datWrite);
    
}

- (void)updateFirmware{
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    uint8_t bytes[] = {0x8E, 0x00, 0x05, 0x9B, 0x2E};
    NSData *datWrite = [NSData dataWithBytes:bytes length:5];
    [cbpDeviceHC writeValue:datWrite forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
    NSLog(@"TX: %@", datWrite);
}

- (void)pingOBD{
    uint8_t bytes[] = {0x8E, 0x00, 0x05, 0x50, 0xE3};
    NSData *datWrite = [NSData dataWithBytes:bytes length:5];
    [cbpDeviceOBD writeValue:datWrite forCharacteristic:chrWriteOBD type:CBCharacteristicWriteWithResponse];
    NSLog(@"TX: %@", datWrite);
}

- (void)discoverOBDProtocol{
    uint8_t bytes[] = {0x8E, 0x00, 0x05, 0x1B, 0xAE};
    NSData *datWrite = [NSData dataWithBytes:bytes length:5];
    [cbpDeviceOBD writeValue:datWrite forCharacteristic:chrWriteOBD type:CBCharacteristicWriteWithResponse];
    NSLog(@"TX: %@", datWrite);
}

- (void)doRelearn{
    NSDateFormatter *dFormat = [[NSDateFormatter alloc] init];
    [dFormat setDateFormat:@"yyyyMMddhhmm"];
    NSString *strDate = [dFormat stringFromDate:[NSDate date]];
    uint8_t year0 = [[strDate substringWithRange:NSMakeRange(0, 1)] intValue];
    uint8_t year1 = [[strDate substringWithRange:NSMakeRange(1, 1)] intValue];
    uint8_t year2 = [[strDate substringWithRange:NSMakeRange(2, 1)] intValue];
    uint8_t year3 = [[strDate substringWithRange:NSMakeRange(3, 1)] intValue];
    uint8_t month0 = [[strDate substringWithRange:NSMakeRange(4, 1)] intValue];
    uint8_t month1 = [[strDate substringWithRange:NSMakeRange(5, 1)] intValue];
    uint8_t day0 = [[strDate substringWithRange:NSMakeRange(6, 1)] intValue];
    uint8_t day1 = [[strDate substringWithRange:NSMakeRange(7, 1)] intValue];
    uint8_t hour0 = [[strDate substringWithRange:NSMakeRange(8, 1)] intValue];
    uint8_t hour1 = [[strDate substringWithRange:NSMakeRange(9, 1)] intValue];
    uint8_t minute0 = [[strDate substringWithRange:NSMakeRange(10, 1)] intValue];
    uint8_t minute1 = [[strDate substringWithRange:NSMakeRange(11, 1)] intValue];
    uint8_t sum = 0x8E + 0x00 + 0x11 + 0x10 + year0 + year1 + year2 + year3 + month0 + month1 + day0 + day1 + hour0 + hour1 + minute0 + minute1;
    uint8_t bytes[] = {0x8E, 0x00, 0x011, 0x10, year0, year1, year2, year3, month0, month1, day0, day1, hour0, hour1, minute0, minute1, sum};
    NSData *datWrite = [NSData dataWithBytes:bytes length:17];
    [cbpDeviceOBD writeValue:datWrite forCharacteristic:chrWriteOBD type:CBCharacteristicWriteWithResponse];
    NSLog(@"TX: %@", datWrite);
}

- (void)peripheral:(CBPeripheral *)peripheral didWriteValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    if(error){
        [self dataError];
    }else if(chunking){
        chunkCount++;
        int location = chunkCount * 20;
        int rangeLength = 20;
        int whatsLeft = chunkLength - location;
        if(whatsLeft < rangeLength){
            chunking = NO;
            rangeLength = whatsLeft;
        }
        chunkData = [writeData subdataWithRange:NSMakeRange(location, rangeLength)];
        [cbpDeviceHC writeValue:chunkData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
        //NSLog(@"Chunk TX: %@", chunkData);
    }
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateNotificationStateForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
}

- (void)peripheral:(CBPeripheral *)peripheral didUpdateValueForCharacteristic:(CBCharacteristic *)characteristic error:(NSError *)error{
    if(error){
        [self dataError];
        return;
    }
    if([[characteristic UUID] isEqual:[CBUUID UUIDWithString:NS_READ_UUID]]){
        [readingData appendData:[characteristic value]];
        if([[characteristic value] length] < 20){
            resultData = readingData;
            readingData = [[NSMutableData alloc] init];
            NSLog(@"RX: %@", resultData);
            if(resultData && !canceled){
                [self parseResult:resultData];
            }else if(!canceled){
                [self dataError];
            }
        }
    }
}

- (void)parseResult:(NSData *)passedData{
    const uint8_t *bytes = [passedData bytes];
    if(bytes[0] != 0x8F){
        return;
    }
    int msgLength = bytes[2];
    if(msgLength > 3){
        uint8_t commandByte = bytes[3];
        switch(commandByte){
            //check sensor block
            case 0xA0:{ //ack check sensor
                uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0x61, 0xF4};
                writeData = [NSData dataWithBytes:writeBytes length:5];
                [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
            }
                break;
            case 0xA1:{ //ack progress
                uint8_t typeByte = bytes[4];
                if(typeByte == 0x00){ //still in progress
                    uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0x61, 0xF4};
                    writeData = [NSData dataWithBytes:writeBytes length:5];
                    [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse]; //try again
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateLoadSensor" object:nil];
                }else if(typeByte == 0x01){ //ack success
                    uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0x62, 0xF5};
                    writeData = [NSData dataWithBytes:writeBytes length:5];
                    [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse]; //get sensor info
                }else if(typeByte == 0x02){ //error or timeout
                    if(checkingFreq){
                        [self finishDiscoverFreq];
                    }else{
                        [self finishCheck];
                    }
                }
            }
                break;
            case 0xA2:
                if(checkingFreq){
                    [self finish:5];
                }else{
                    iByte0 = bytes[4];
                    iByte1 = bytes[5];
                    iByte2 = bytes[6];
                    iByte3 = bytes[7];
                    pByte = bytes[8];
                    tByte = bytes[9];
                    bByte = bytes[10];
                    [self finish:1];
                }
                break;
            //create sensor block
            case 0xC0:{
                if(!copy && !copySet){ //create random id
                    CommandWord *cmdWord = [cmdWords objectAtIndex:cmdWordsCount];
                    if([cmdWord hasRange]){
                        if(cmdWord.highByteZero - cmdWord.lowByteZero == 0){
                            iByte0 = cmdWord.lowByteZero;
                        }else{
                            iByte0 = cmdWord.lowByteZero + arc4random_uniform(cmdWord.highByteZero - cmdWord.lowByteZero);
                        }
                        if(cmdWord.highByteOne - cmdWord.lowByteOne == 0){
                            iByte1 = cmdWord.lowByteOne;
                        }else{
                            iByte1 = cmdWord.lowByteOne + arc4random_uniform(cmdWord.highByteOne - cmdWord.lowByteOne);
                        }
                        if(cmdWord.highByteTwo - cmdWord.lowByteTwo == 0){
                            iByte2 = cmdWord.lowByteTwo;
                        }else{
                            iByte2 = cmdWord.lowByteTwo + arc4random_uniform(cmdWord.highByteTwo - cmdWord.lowByteTwo);
                        }
                        if(cmdWord.highByteThree - cmdWord.lowByteThree == 0){
                            iByte3 = cmdWord.lowByteThree;
                        }else{
                            iByte3 = cmdWord.lowByteThree + arc4random_uniform(cmdWord.highByteThree - cmdWord.lowByteThree);
                        }
                    }else{
                        iByte0 = arc4random();
                        iByte1 = arc4random();
                        iByte2 = arc4random();
                        iByte3 = arc4random();
                    }
                }
                int8_t sum = 0x8E + 0x00 + 0x09 + 0x81 + iByte0 + iByte1 + iByte2 + iByte3;
                uint8_t writeBytes[] = {0x8E, 0x00, 0x09, 0x81, iByte0, iByte1, iByte2, iByte3, sum};
                writeData = [NSData dataWithBytes:writeBytes length:9];
                [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
            }
                break;
            case 0xC1:{ //ack ID accepted
                uint8_t sum = 0x8E + 0x00 + 0x0C + 0x90 + 0x09 + mhz + 0x00 + [currCmdWord selectWord] + 0x00 + [currCmdWord softwareVersion] + 0x00;
                uint8_t writeBytes[] = {0x8E, 0x00, 0x0C, 0x90, 0x09, mhz, 0x00, [currCmdWord selectWord], 0x00, [currCmdWord softwareVersion], 0x00, sum};
                writeData = [NSData dataWithBytes:writeBytes length:12];
                [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse]; //setup sensor type
            }
                break;
            case 0xD0:{ //ack setup accepted
                uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0x91, 0x24};
                writeData = [NSData dataWithBytes:writeBytes length:5];
                [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse]; //start wake
            }
                break;
            case 0xD1:{ //ack wake started
                uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0x92, 0x25};
                writeData = [NSData dataWithBytes:writeBytes length:5];
                [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse]; //get wake progress
            }
                break;
            case 0xD2:{ //return wake progress
                uint8_t typeByte = bytes[4];
                if(typeByte == 0x01){ //success
                    uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0x93, 0x26};
                    writeData = [NSData dataWithBytes:writeBytes length:5];
                    [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse]; //get wake results
                }else if(typeByte == 0x02){ //error or timeout
                    [self finishCreate];
                }else{
                    uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0x92, 0x25};
                    writeData = [NSData dataWithBytes:writeBytes length:5];
                    [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse]; //try again
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateLoadSensor" object:nil];
                }
            }
                break;
            case 0xD3:{ //wake results
                uint8_t sum = 0x8E + 0x00 + 0x09 + 0x94 + bytes[5] + bytes[6] + bytes[7] + bytes[8];
                uint8_t writeBytes[] = {0x8E, 0x00, 0x09, 0x94, bytes[5], bytes[6], bytes[7], bytes[8], sum};
                writeData = [NSData dataWithBytes:writeBytes length:9];
                [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse]; //choose sensor to program
            }
                break;
            case 0xD4:{ //ack sensor choice accepted
                writing = YES;
                uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0x98, 0x2B};
                writeData = [NSData dataWithBytes:writeBytes length:5];
                [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse]; //check writing status
            }
                break;
            case 0xD8:{ //return writing or lock results
                uint8_t typeByte = bytes[4];
                if(writing){
                    if(typeByte == 0x00){ //timeout
                        [self finishCreate];
                    }else if(typeByte == 0x01){ //success
                        uint8_t writeBytes[] = {0x8E, 0x00, 0x06, 0x95, 0x00, 0x29};
                        writeData = [NSData dataWithBytes:writeBytes length:6];
                        [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse]; //lock it in
                    }else{
                        uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0x98, 0x2B};
                        writeData = [NSData dataWithBytes:writeBytes length:5];
                        [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse]; //try again
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateLoadSensor" object:nil];
                    }
                }else{
                    if(typeByte == 0x00){ //timeout
                        [self finishCreate];
                    }else if(typeByte == 0x01 || typeByte == 0x04){ //success
                        [self finish:2];
                    }else{
                        uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0x98, 0x2B};
                        writeData = [NSData dataWithBytes:writeBytes length:5];
                        [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse]; //try again
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateLoadSensor" object:nil];
                    }
                }
            }
                break;
            case 0xD5:{ //ack writing completed
                writing = NO;
                uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0x98, 0x2B};
                writeData = [NSData dataWithBytes:writeBytes length:5];
                [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse]; //get lock results
            }
                break;
            //update firmware block
            case 0xDB:{ //ack firmware response
                uint8_t fwBytes[3] = {bytes[14], bytes[15], bytes[16]};
                NSData *fwData = [NSData dataWithBytes:fwBytes length:3];
                NSString *fwVersionHC = [NSString stringWithUTF8String:[fwData bytes]];
                if(checkingFirmware){
                    checkingFirmware = NO;
                    NSString *fwVersionFormatted = [NSString stringWithFormat:@"%@.%@.%@", [fwVersionHC substringToIndex:1], [fwVersionHC substringWithRange:NSMakeRange(1, 1)], [fwVersionHC substringWithRange:NSMakeRange(2, 1)]];
                    [[NSUserDefaults standardUserDefaults] setObject:fwVersionFormatted forKey:@"firmwareVersion"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                }else{
                    if([fwVersion intValue] <= [fwVersionHC intValue]){
                        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_sameFirmware"] preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *actYes = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_yes"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            [self sendStartFileWritingCommand];
                        }];
                        UIAlertAction* actNo = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_no"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] dismissViewControllerAnimated:YES completion:nil];
                        }];
                        [ctrAlert addAction:actYes];
                        [ctrAlert addAction:actNo];
                        [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:ctrAlert animated:YES completion:nil];
                    }else{ // do it
                        [self sendStartFileWritingCommand];
                    }
                }
            }
                break;
            case 0xEA:{ //ack we're writing firmware
                NSString *fileName = [NSString stringWithFormat:@"%@firmware.bin", fwVersion];
                NSString *filePath = [[NSBundle mainBundle] pathForResource:[NSString stringWithFormat:@"%@firmware", fwVersion] ofType:@"bin"];
                fileData = [NSData dataWithContentsOfFile:filePath];
                if(fileData){
                    partCounter = 0;
                    float fPartTotal = ([fileData length]/200.0) + .5;
                    partTotal = (int)fPartTotal;
                    totalParts = malloc(sizeof(UInt8) * 4);
                    totalParts[0] = partTotal >> 8;
                    totalParts[1] = partTotal >> 0;
                    int i = (int)[fileData length];
                    UInt8 *fileLength = malloc(sizeof(UInt8) * 4);
                    fileLength[0] = i >> 24;
                    fileLength[1] = i >> 16;
                    fileLength[2] = i >> 8;
                    fileLength[3] = i >> 0;
                    int length = 12 + (int)[fileName length];
                    uint8_t writeBytes[length];
                    writeBytes[0] = 0x8E;
                    writeBytes[1] = 0x00;
                    writeBytes[2] = length;
                    writeBytes[3] = 0x9E;
                    writeBytes[4] = fileLength[0];
                    writeBytes[5] = fileLength[1];
                    writeBytes[6] = fileLength[2];
                    writeBytes[7] = fileLength[3];
                    writeBytes[8] = 0x00;
                    writeBytes[9] = 0xFF;
                    writeBytes[10] = (uint8_t)[fileName length];
                    uint8_t sum = 0;
                    for(int i = 0; i < length-1; i++){
                        if(i > 10){
                            writeBytes[i] = (uint8_t)[fileName characterAtIndex:(i - 11)];
                        }
                        sum = (uint8_t)(sum + writeBytes[i]);
                    }
                    writeBytes[length-1] = sum;
                    writeData = [NSData dataWithBytes:writeBytes length:length];
                    if(length > 20){
                        chunking = YES;
                        chunkCount = 0;
                        chunkLength = length;
                        chunkData = [writeData subdataWithRange:NSMakeRange(0, 20)];
                        [cbpDeviceHC writeValue:chunkData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
                        //NSLog(@"TX: %@", chunkData);
                    }else{
                        [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
                    }
                }else{ //error
                    [self finish:0];
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
                }
                
            }
                break;
            case 0xDE: //ack we're ready to send file parts
                [self writeFilePart];
                break;
            case 0xE2:{ //file part received
                uint8_t typeByte = bytes[4];
                if(typeByte == 0x00){
                    if(partCounter < partTotal){
                        [self writeFilePart];
                    }else{
                        uint8_t sumFile = 0;
                        for(int i = 0; i < [fileData length]; i++){
                            unsigned char aBuffer[1];
                            [fileData getBytes:aBuffer range:NSMakeRange(i, 1)];
                            sumFile = sumFile + aBuffer[0];
                        }
                        UInt8 *fileSumBytes = malloc(sizeof(UInt8) * 4);
                        fileSumBytes[0] = sumFile >> 8;
                        fileSumBytes[1] = sumFile >> 0;
                        uint8_t sum = 0x8E + 0x07 + 0xA3 + fileSumBytes[0] + fileSumBytes[1];
                        uint8_t writeBytes[] = {0x8E, 0x00, 0x07, 0xA3, fileSumBytes[0], fileSumBytes[1], sum};
                        writeData = [NSData dataWithBytes:writeBytes length:7];
                        [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
                    }
                }else{
                    [self finish:0];
                }
            }
                break;
            case 0xE3:{ //file accepted
                uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0x52, 0xe5}; //do a reset
                writeData = [NSData dataWithBytes:writeBytes length:5];
                [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
                [self finish:3];
            }
                break;
            //obd block
            case 0x90: //obd has been pinged
                [self discoverOBDProtocol];
                break;
            case 0x5B:{ //obd protocol result
                uint8_t typeByte = bytes[4];
                if(typeByte == 0x00){ //ecu
                    activateWithTool = NO;
                    if([_datIds count] > 0){
                        BOOL idFound = NO;
                        for(int i = 0; i < [_datIds count]; i++){
                            if(![[_datIds objectAtIndex:i] isEqual:[NSNull null]]){
                                idFound = YES;
                            }
                        }
                        if(idFound){
                            [self doRelearn];
                        }else{
                            [self finish:7];
                        }
                    }else{
                        [self finish:7];
                    }
                }else if(typeByte == 0x01){ //tool
                    activateWithTool = YES;
                    [self doRelearn];
                }
            }
                break;
            case 0x50:{ //check communication status
                uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0x14, 0xA7};
                writeData = [NSData dataWithBytes:writeBytes length:5];
                [cbpDeviceOBD writeValue:writeData forCharacteristic:chrWriteOBD type:CBCharacteristicWriteWithResponse];
            }
                break;
            case 0x54:{ //ack check communication
                uint8_t typeByte = bytes[4];
                if(typeByte == 0x00){ //ecu is active
                    //do some writing
                    //eventually determine if ids need to be sent or if the tires need to be activated afterward!!!
                }else if(typeByte == 0x01){
                    //broken
                }else{
                    uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0x14, 0xA7};
                    writeData = [NSData dataWithBytes:writeBytes length:5];
                    [cbpDeviceOBD writeValue:writeData forCharacteristic:chrWriteOBD type:CBCharacteristicWriteWithResponse]; //try again?
                }
                uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0x14, 0xA7};
                writeData = [NSData dataWithBytes:writeBytes length:5];
                [cbpDeviceOBD writeValue:writeData forCharacteristic:chrWriteOBD type:CBCharacteristicWriteWithResponse];
            }
                break;
        }
        if([writeData length] > 0){
            NSLog(@"TX: %@", writeData);
        }
    }
}

- (void) sendStartFileWritingCommand {
    NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_updatingFirmware"],  @"hideCancel":@1};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoadSensor" object:dic];
    uint8_t writeBytes[] = {0x8E, 0x00, 0x05, 0xAA, 0x3D};
    writeData = [NSData dataWithBytes:writeBytes length:5];
    [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse]; //prepare for writing firmware
}

- (void)writeFilePart{
    //length of command = 213
    //number of bytes per part = 200
    //part number
    UInt8 *partNumBytes = malloc(sizeof(UInt8) * 4);
    partNumBytes[0] = partCounter >> 8;
    partNumBytes[1] = partCounter >> 0;
    int start = partCounter * 200;
    NSData *sendData;
    int end = (int)[fileData length] - start;
    if(end >= 200){
        sendData = [fileData subdataWithRange:NSMakeRange(start, 200)];
        end = start + 200;
    }else{
        sendData = [fileData subdataWithRange:NSMakeRange(start, end)];
        end = (int)[fileData length];
    }
    int arrayLength = (end - start) + 13;
    uint8_t writeBytes[arrayLength];
    uint8_t sum = 0;
    for(int i = 0; i < (arrayLength - 1); i++){
        if(i < 12){
            switch(i){
                case 0:
                    writeBytes[i] = 0x8E;
                    break;
                case 1:
                    writeBytes[i] = 0x00;
                    break;
                case 2:
                    writeBytes[i] = (uint8_t)arrayLength;
                    break;
                case 3:
                    writeBytes[i] = 0xA2;
                    break;
                case 4:
                    writeBytes[i] = 0x00;
                    break;
                case 5:
                    writeBytes[i] = 0x00;
                    break;
                case 6:
                    writeBytes[i] = 0x00;
                    break;
                case 7:
                    writeBytes[i] = (arrayLength - 13);
                    break;
                case 8:
                    writeBytes[i] = partNumBytes[0];
                    break;
                case 9:
                    writeBytes[i] = partNumBytes[1];
                    break;
                case 10:
                    writeBytes[i] = totalParts[0];
                    break;
                case 11:
                    writeBytes[i] = totalParts[1];
                    break;
            }
        }else{
            unsigned char aBuffer[1];
            [sendData getBytes:aBuffer range:NSMakeRange(i-12, 1)];
            writeBytes[i] = aBuffer[0];
        }
        sum = sum + writeBytes[i];
    }
    writeBytes[arrayLength-1] = sum;
    writeData = [NSData dataWithBytes:writeBytes length:arrayLength];
    if([writeData length] > 20){
        chunking = YES;
        chunkCount = 0;
        chunkLength = (int)[writeData length];
        chunkData = [writeData subdataWithRange:NSMakeRange(0, 20)];
        [cbpDeviceHC writeValue:chunkData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
    }else{
        [cbpDeviceHC writeValue:writeData forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
    }
    partCounter++;
    float value = (float)partCounter/(float)partTotal;
    NSDictionary *dic = @{@"value":[NSNumber numberWithFloat:value]};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateLoadFirmware" object:dic];
}

- (void)finishCheck{
    if(cmdWordsCount < [cmdWords count]){
        cmdWordsCount++;
        for(int i = cmdWordsCount; i < [cmdWords count]; i++){
            CommandWord *cmdWord = [cmdWords objectAtIndex:cmdWordsCount];
            if([currCmdWord checkWord] != [cmdWord checkWord]){
                currCmdWord = cmdWord;
                uint8_t checkWord = [currCmdWord checkWord];
                uint8_t sum = 0x8E + 0x00 + 0x07 + 0x60 + checkWord + mhz;
                uint8_t bytes[] = {0x8E, 0x00, 0x07, 0x60, checkWord, mhz, sum};
                NSData *datWrite = [NSData dataWithBytes:bytes length:7];
                [cbpDeviceHC writeValue:datWrite forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
                return;
            }
        }
        [self finish:0];
    }else{
        [self finish:0];
    }
}

- (void)finishDiscoverFreq{
    if(mhz == 0x01){
        mhz = 0x02;
        [self checkSensor:cmdWords mhz:mhz chk:YES];
    }else{
        [self finish:6];
    }
}

- (void)finishCreate{
    if(cmdWordsCount < [cmdWords count]-1){
        cmdWordsCount++;
        currCmdWord = [cmdWords objectAtIndex:cmdWordsCount];
        [self createSensor:cmdWords mhz:mhz];
    }else{
        [self finish:0];
    }
}

- (void)finish:(int)result{
    switch(result){
        case 0:
            [self dataError];
            break;
        case 1:{
            NSDictionary *dic = @{@"resultData":resultData, @"frequency":@(mhz)};
            if(copy){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CopyResults" object:dic];
            }else if(copySet){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CopySetResults" object:dic];
            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CheckResults" object:dic];
            }
        }
            break;
        case 2:{
            uint8_t resultBytes[] = {iByte0, iByte1, iByte2, iByte3};
            resultData = [NSData dataWithBytes:resultBytes length:4];
            NSDictionary *dic = @{@"resultData":resultData, @"frequency":@(mhz)};
            if(copy){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CopyResults" object:nil];
            }else if(copySet){
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CreateFromSetResults" object:dic];
            }else{
                [[NSNotificationCenter defaultCenter] postNotificationName:@"CreateResults" object:dic];
            }
        }
            break;
        case 3:
            fwVersion = [NSString stringWithFormat:@"%@.%@.%@", [fwVersion substringToIndex:1], [fwVersion substringWithRange:NSMakeRange(1, 1)], [fwVersion substringWithRange:NSMakeRange(2, 1)]];
            [[NSUserDefaults standardUserDefaults] setObject:fwVersion forKey:@"firmwareVersion"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FirmwareUpdated" object:nil];
            [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
            break;
        case 5:{
            NSDictionary *dic = @{@"frequency":@(mhz)};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FreqDiscovered" object:dic];
        }
            break;
        case 6:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"FreqUndiscovered" object:nil];
            break;
        case 7:{
            UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_sensorBeforeOBD"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [[[[[UIApplication sharedApplication] delegate] window] rootViewController] dismissViewControllerAnimated:YES completion:nil];
            }];
            [ctrAlert addAction:actOk];
            [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:ctrAlert animated:YES completion:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoadSensor" object:nil];
        }
            break;
    }
}

- (void)connectError{
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_deviceNotConnected"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [[[[[UIApplication sharedApplication] delegate] window] rootViewController] dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actOk];
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:ctrAlert animated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoadSensor" object:nil];
}

- (void)dataError{
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_errorTimeout"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [[[[[UIApplication sharedApplication] delegate] window] rootViewController] dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actOk];
    [[[[[UIApplication sharedApplication] delegate] window] rootViewController] presentViewController:ctrAlert animated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoadSensor" object:nil];
}

- (void)centralManager:(CBCentralManager *)central didDisconnectPeripheral:(CBPeripheral *)peripheral error:(NSError *)error{
    if([peripheral isEqual:cbpDeviceHC]){
        _btObjectHC = nil;
        [[NSNotificationCenter defaultCenter] postNotificationName:@"BTDisconnected" object:nil];
    }else if([peripheral isEqual:cbpDeviceOBD]){
        _btObjectOBD = nil;
    }
}

- (void)cancel{
    canceled = YES;
    uint8_t bytes[] = {0x8E, 0x00, 0x05, 0x51, 0xE4};
    NSData *datWrite = [NSData dataWithBytes:bytes length:5];
    [cbpDeviceHC writeValue:datWrite forCharacteristic:chrWriteHC type:CBCharacteristicWriteWithResponse];
    NSLog(@"TX: %@", datWrite);
}

@end
