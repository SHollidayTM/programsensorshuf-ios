//
//  CommandWord.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/4/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CommandWord : NSObject

@property (assign) int selectWord;
@property (assign) int checkWord;
@property (assign) int softwareVersion;
@property (assign) BOOL hasRange;
@property (assign) int lowByteZero;
@property (assign) int lowByteOne;
@property (assign) int lowByteTwo;
@property (assign) int lowByteThree;
@property (assign) int highByteZero;
@property (assign) int highByteOne;
@property (assign) int highByteTwo;
@property (assign) int highByteThree;

@end
