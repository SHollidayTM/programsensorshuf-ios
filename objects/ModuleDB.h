//
//  DBModule.h
//  alligator-iphone
//
//  Created by Scott Holliday on 10/26/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <sqlite3.h>
#import "Make.h"
#import "Model.h"
#import "SensorTPMS.h"
#import "ServiceKit.h"
#import "Attachment.h"
#import "Vehicle.h"
#import "CommandWord.h"
#import "SensorNameFreq.h"
#import "VehicleConfig.h"
#import "History.h"
#import "Action.h"

@interface ModuleDB : NSObject{
    sqlite3 *db;
}

@property (assign) BOOL inService;

+ (id)sharedInstance;
- (NSArray *)getAllYears;
- (NSArray *)getMakesByYear:(NSString *)year;
- (NSArray *)getModelsByYearAndMake:(NSString *)year mke:(Make *)make;
- (Make *)getMakeByMakeName:(NSString *)makeName;
- (Model *)getModelByModelName:(NSString *)modelName;
- (NSArray *)getQualifiersByYearMakeAndModel:(NSString *)year mke:(Make *)make mod:(Model *)model;
- (Vehicle *)getVehicleByConfigID:(NSString *)configID;
- (NSArray *)getVehiclesByConfigIDs:(NSString *)configIDs;
- (NSArray *)getAttachmentsForVehicle:(Vehicle *)vehicle;
- (void)addAttachments:(NSArray *)attachments;
- (void)addVehicles:(NSArray *)vehicles;
- (void)addHistory:(NSNumber *)configID act:(NSNumber *)action;
- (NSArray *)getHistory:(NSNumber *)action;
- (NSArray *)getActions;
- (void)deleteHistory;
- (NSString *)getPartNumByUPC:(NSString *)upc;

@end
