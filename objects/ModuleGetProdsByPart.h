//
//  ModuleGetProdsByPart.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 3/30/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "ModuleGetKey.h"
#import "ModuleParseAndReturnProds.h"

@interface ModuleGetProdsByPart : NSObject{
    NSString *partNum;
    BOOL attemptedOnce;
}

+ (id)sharedInstance;
- (void)go:(NSString *)sentPartNum;

@end
