//
//  ModuleRegister.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 5/19/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "ModuleRegister.h"

@implementation ModuleRegister

+ (id)sharedInstance{
    static ModuleRegister *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)go{
    attemptedOnce = NO;
    [self tryToConnect];
}

- (void)tryToConnect{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@User/Register", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"apiUrl"]]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *securityKey = [[NSUserDefaults standardUserDefaults] stringForKey:@"securityKey"];
    NSString *deviceKey = @"";
    NSData *dataBTObject = [[NSUserDefaults standardUserDefaults] objectForKey:@"btObject"];
    if(dataBTObject){
        BTObject *btObject = [NSKeyedUnarchiver unarchiveObjectWithData:dataBTObject];
        deviceKey = [btObject serialNum];
    }
    NSString *email = [[NSUserDefaults standardUserDefaults] stringForKey:@"email"];
    NSString *name = [[NSUserDefaults standardUserDefaults] stringForKey:@"name"];
    NSString *companyName = [[NSUserDefaults standardUserDefaults] stringForKey:@"companyName"];
    NSString *addressOne = [[NSUserDefaults standardUserDefaults] stringForKey:@"addressOne"];
    NSString *addressTwo = [[NSUserDefaults standardUserDefaults] stringForKey:@"addressTwo"];
    NSString *city = [[NSUserDefaults standardUserDefaults] stringForKey:@"city"];
    NSString *state = [[NSUserDefaults standardUserDefaults] stringForKey:@"state"];
    NSString *postal = [[NSUserDefaults standardUserDefaults] stringForKey:@"postal"];
    NSString *phone = [[NSUserDefaults standardUserDefaults] stringForKey:@"phone"];
    NSString *body = [NSString stringWithFormat:@"{\"securityKey\":\"%@\",\"regionCode\":\"NorthAmerica\",\"deviceKey\":\"%@\",\"email\":\"%@\",\"name\":\"%@\",\"companyName\":\"%@\",\"companyAddress1\":\"%@\",\"companyAddress2\":\"%@\",\"city\":\"%@\",\"state\":\"%@\",\"postal\":\"%@\",\"companyPhone\":\"%@\"}", securityKey, deviceKey, email, name, companyName, addressOne, addressTwo, city, state, postal, phone];
    [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        if(error){
            [self tryAgain];
            return;
        }
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse *)response;
        if(httpResp.statusCode != 200){
            [self tryAgain];
            return;
        }else{
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnRegistration" object:json];
        }
    }];
    [task resume];
}

- (void)tryAgain{
    if(!attemptedOnce){
        attemptedOnce = YES;
        [[ModuleGetKey sharedInstance] getSecurityKey:^(BOOL success){
            if(success){
                [self tryToConnect];
            }else{
                NSDictionary *dic = @{@"text":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_errorTryAgain"]};
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnError" object:dic];
            }
        }];
    }else{
        NSDictionary *dic = @{@"text":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_errorTryAgain"]};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnError" object:dic];
    }
}

@end
