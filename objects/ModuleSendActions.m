//
//  ModuleSendActions.m
//  progsensors-ipad
//
//  Created by Scott Holliday on 6/1/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "ModuleSendActions.h"

@implementation ModuleSendActions

+ (id)sharedInstance{
    static ModuleSendActions *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)go:(NSArray *)sentActions{
    attemptedOnce = NO;
    actions = sentActions;
    [self tryToConnect];
}

- (void)tryToConnect{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@Log/Add", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"apiUrl"]]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *securityKey = [[NSUserDefaults standardUserDefaults] stringForKey:@"securityKey"];
    NSData *dataBTObject = [[NSUserDefaults standardUserDefaults] objectForKey:@"btObject"];
    BTObject *btObject = [NSKeyedUnarchiver unarchiveObjectWithData:dataBTObject];
    NSString *deviceKey = [btObject serialNum];
    NSString *strActions = @"\"details\":[";
    for(Action *action in actions){
        strActions = [strActions stringByAppendingString:[NSString stringWithFormat:@"{\"eventCode\":\"%d\",\"eventDetail\":\"%d\"},", [action actionID], [action configID]]];
    }
    strActions = [strActions substringToIndex:[strActions length]-1];
    strActions = [strActions stringByAppendingString:@"]"];
    NSString *body = [NSString stringWithFormat:@"{\"securityKey\":\"%@\",\"regionCode\":\"NorthAmerica\",\"deviceKey\":\"%@\",%@}", securityKey, deviceKey, strActions];
    [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        if(error){
            [self tryAgain];
            return;
        }
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse *)response;
        if(httpResp.statusCode != 200 && httpResp.statusCode != 204){
            [self tryAgain];
            return;
        }else{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnSentActions" object:nil];
        }
    }];
    [task resume];
}

- (void)tryAgain{
    if(!attemptedOnce){
        attemptedOnce = YES;
        [[ModuleGetKey sharedInstance] getSecurityKey:^(BOOL success){
            if(success){
                [self tryToConnect];
            }else{
                NSDictionary *dic = @{@"text":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_errorTryAgain"]};
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnError" object:dic];
            }
        }];
    }else{
        NSDictionary *dic = @{@"text":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_errorTryAgain"]};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnError" object:dic];
    }
}

@end
