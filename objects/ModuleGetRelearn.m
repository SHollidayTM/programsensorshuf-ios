//
//  ModuleGetRelearns.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/7/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "ModuleGetRelearn.h"

@implementation ModuleGetRelearn

+ (id)sharedInstance{
    static ModuleGetRelearn *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)go:(NSString *)sentConfigID{
    attemptedOnce = NO;
    alreadySent = NO;
    configID = sentConfigID;
    [self tryToConnect];
}

- (void)tryToConnect{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@Vehicle/Relearn", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"apiUrl"]]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *securityKey = [[NSUserDefaults standardUserDefaults] stringForKey:@"securityKey"];
    NSString *body = [NSString stringWithFormat:@"{\"securityKey\":\"%@\",\"configID\":\"%@\"}", securityKey, configID];
    [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        if(error){
            [self tryAgain];
            return;
        }
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse *)response;
        if(httpResp.statusCode != 200){
            [self tryAgain];
            return;
        }else{
            NSDictionary *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            Relearn *relearn = [[Relearn alloc] init];
            if(![[json objectForKey:@"addAirRequired"] isKindOfClass:[NSNull class]]){
                [relearn setAddAir:[[json objectForKey:@"addAirRequired"] uppercaseString]];
            }else{
                [relearn setAddAir:@""];
            }
            if(![[json objectForKey:@"rotateTireRequired"] isKindOfClass:[NSNull class]]){
                [relearn setRotateTires:[[json objectForKey:@"rotateTireRequired"] uppercaseString]];
            }else{
                [relearn setRotateTires:@""];
            }
            if(![[json objectForKey:@"replaceSensorRequired"] isKindOfClass:[NSNull class]]){
                [relearn setReplaceSensor:[[json objectForKey:@"replaceSensorRequired"] uppercaseString]];
            }else{
                [relearn setReplaceSensor:@""];
            }
            if(![[json objectForKey:@"Base64Relearn"] isKindOfClass:[NSNull class]]){
                if([[json objectForKey:@"Base64Relearn"] length] > 0){
                    NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:[json objectForKey:@"Base64Relearn"] options:0];
                    NSString *instructions = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
                    [relearn setInstructions:instructions];
                }
            }
            [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnRelearn" object:relearn];
        }
    }];
    [task resume];
}

- (void)tryAgain{
    if(!attemptedOnce){
        attemptedOnce = YES;
        [[ModuleGetKey sharedInstance] getSecurityKey:^(BOOL success){
            if(success){
                [self tryToConnect];
            }else{
                if(!alreadySent){
                    alreadySent = YES;
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnRelearn" object:nil];
                }
            }
        }];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnRelearn" object:nil];
    }
}

@end
