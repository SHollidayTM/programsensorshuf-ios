//
//  ModuleGetProdsByPart.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 3/30/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "ModuleGetProdsByPart.h"

@implementation ModuleGetProdsByPart

+ (id)sharedInstance{
    static ModuleGetProdsByPart *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)go:(NSString *)sentPartNum{
    attemptedOnce = NO;
    partNum = sentPartNum;
    [self tryToConnect];
}

- (void)tryToConnect{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@Product/ByPartNumber", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"apiUrl"]]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *securityKey = [[NSUserDefaults standardUserDefaults] stringForKey:@"securityKey"];
    NSString *body = [NSString stringWithFormat:@"{\"securityKey\":\"%@\",\"partNumSpec\":\"%@\"}", securityKey, partNum];
    [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        if(error){
            [self tryAgain];
            return;
        }
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse *)response;
        if(httpResp.statusCode != 200){
            [self tryAgain];
            return;
        }else{
            NSJSONSerialization *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            [[ModuleParseAndReturnProds sharedInstance] parseAndReturn:json];
        }
    }];
    [task resume];
}

- (void)tryAgain{
    if(!attemptedOnce){
        attemptedOnce = YES;
        [[ModuleGetKey sharedInstance] getSecurityKey:^(BOOL success){
            if(success){
                [self tryToConnect];
            }else{
                NSDictionary *dic = @{@"text":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_errorTryAgain"]};
                [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnError" object:dic];
            }
        }];
    }else{
        NSDictionary *dic = @{ @"text":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_errorTryAgain"]};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnError" object:dic];
    }
}

@end
