//
//  Relearn.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/7/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Relearn : NSObject

@property (nonatomic, strong) NSString *addAir;
@property (nonatomic, strong) NSString *rotateTires;
@property (nonatomic, strong) NSString *replaceSensor;
@property (nonatomic, strong) NSString *instructions;

@end
