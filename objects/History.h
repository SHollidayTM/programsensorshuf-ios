//
//  History.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 5/3/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface History : NSObject

@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSNumber *year;
@property (nonatomic, strong) NSString *make;
@property (nonatomic, strong) NSString *model;
@property (nonatomic, strong) NSString *action;

@end
