//
//  VehicleConfig.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 3/29/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface VehicleConfig : NSObject

@property (nonatomic, strong) NSString *configID;
@property (nonatomic, strong) NSString *qualifier;
@property (assign) BOOL indirect;

@end
