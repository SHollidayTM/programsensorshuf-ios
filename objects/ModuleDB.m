//
//  DBModule.m
//  alligator-iphone
//
//  Created by Scott Holliday on 10/26/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "ModuleDB.h"

@implementation ModuleDB

+ (id)sharedInstance{
    static ModuleDB *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (id)init{
    if(self = [super init]){
        NSFileManager *mgrFile = [NSFileManager defaultManager];
        NSString *strTemp = [[NSBundle mainBundle] pathForResource:@"vehicles" ofType:@"sqlite"];
        NSString *strDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *strDBPath = [strDir stringByAppendingPathComponent:@"vehicles.sqlite"];
        NSLog(@"DB Path: %@", strDBPath);
        if(![mgrFile fileExistsAtPath:strDBPath]){
            [mgrFile copyItemAtPath:strTemp toPath:strDBPath error:nil];
        }else{
            sqlite3_open([strDBPath UTF8String], &db);
            NSString *strSQL = @"SELECT name FROM sqlite_master WHERE type = 'table' AND name = 'version'";
            const char *chrStmt = [strSQL UTF8String];
            sqlite3_stmt *cmpStmt;
            if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
                if(sqlite3_step(cmpStmt) != SQLITE_ROW){
                    sqlite3_close(db);
                    [mgrFile removeItemAtPath:strDBPath error:nil];
                    [mgrFile copyItemAtPath:strTemp toPath:strDBPath error:nil];
                }else{
                    NSString *strSQL2 = @"SELECT ID FROM version";
                    const char *chrStmt2 = [strSQL2 UTF8String];
                    sqlite3_stmt *cmpStmt2;
                    if(sqlite3_prepare_v2(db, chrStmt2, -1, &cmpStmt2, NULL) == SQLITE_OK){
                        if(sqlite3_step(cmpStmt2) == SQLITE_ROW){
                            NSString *strId = [NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt2, 0)];
                            if(![strId isEqualToString:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]]){
                                //here we'll eventually have to transfer data, but for now, just replace database
                                sqlite3_close(db);
                                [mgrFile removeItemAtPath:strDBPath error:nil];
                                [mgrFile copyItemAtPath:strTemp toPath:strDBPath error:nil];
                            }
                        }
                    }
                    sqlite3_finalize(cmpStmt2);
                }
            }
            sqlite3_finalize(cmpStmt);
        }
        sqlite3_open([strDBPath UTF8String], &db);
    }
    return self;
}

- (NSArray *)getAllYears{
    NSMutableArray *years = [[NSMutableArray alloc] init];
    [years addObject:@"0"];
    NSString *strSQL = @"SELECT DISTINCT YearID FROM ymm ORDER BY YearID DESC";
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        while(sqlite3_step(cmpStmt) == SQLITE_ROW){
            NSString *year = [NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 0)];
            [years addObject:year];
        }
    }
    sqlite3_finalize(cmpStmt);
    return years;
}

- (NSArray *)getMakesByYear:(NSString *)year{
    NSMutableArray *makes = [[NSMutableArray alloc] init];
    NSString *strSQL = [NSString stringWithFormat:@"SELECT DISTINCT m.ID, m.Make FROM makes m JOIN ymm y ON m.ID = y.MakeID WHERE y.YearID = %@ ORDER BY m.Make ASC", year];
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        while(sqlite3_step(cmpStmt) == SQLITE_ROW){
            Make *make = [[Make alloc] init];
            [make setIdentifier:sqlite3_column_int(cmpStmt, 0)];
            [make setName:[[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 1)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            [makes addObject:make];
        }
    }
    sqlite3_finalize(cmpStmt);
    return makes;
}

- (NSArray *)getModelsByYearAndMake:(NSString *)year mke:(Make *)make{
    NSMutableArray *models = [[NSMutableArray alloc] init];
    NSString *strSQL = [NSString stringWithFormat:@"SELECT DISTINCT m.ID, m.Model FROM models m JOIN ymm y ON m.ID = y.ModelID WHERE y.YearID = %@ AND y.MakeID = %d ORDER BY m.Model ASC", year, [make identifier]];
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        while(sqlite3_step(cmpStmt) == SQLITE_ROW){
            Model *model = [[Model alloc] init];
            [model setIdentifier:sqlite3_column_int(cmpStmt, 0)];
            [model setName:[[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 1)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            [models addObject:model];
        }
    }
    sqlite3_finalize(cmpStmt);
    return models;
}

- (Make *)getMakeByMakeName:(NSString *)makeName{
    NSString *strSQL = [NSString stringWithFormat:@"SELECT ID, Make FROM makes WHERE Make = '%@'", makeName];
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    Make *make = [[Make alloc] init];
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        if(sqlite3_step(cmpStmt) == SQLITE_ROW){
            [make setIdentifier:sqlite3_column_int(cmpStmt, 0)];
            [make setName:[[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 1)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        }
    }
    sqlite3_finalize(cmpStmt);
    return make;
}

- (Model *)getModelByModelName:(NSString *)modelName{
    NSString *strSQL = [NSString stringWithFormat:@"SELECT ID, Model FROM models WHERE Model = '%@'", modelName];
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    Model *model = [[Model alloc] init];
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        if(sqlite3_step(cmpStmt) == SQLITE_ROW){
            [model setIdentifier:sqlite3_column_int(cmpStmt, 0)];
            [model setName:[[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 1)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
        }
    }
    sqlite3_finalize(cmpStmt);
    return model;
}

- (NSArray *)getQualifiersByYearMakeAndModel:(NSString *)year mke:(Make *)make mod:(Model *)model{
    NSMutableArray *vehicleConfigs = [[NSMutableArray alloc] init];
    NSString *strSQL = [NSString stringWithFormat:@"SELECT ConfigID, Qualifier, Indirect FROM ymm WHERE YearID = %@ AND MakeID = %d AND ModelID = %d GROUP BY Qualifier", year, [make identifier], [model identifier]];
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        while(sqlite3_step(cmpStmt) == SQLITE_ROW){
            VehicleConfig *vehicleConfig = [[VehicleConfig alloc] init];
            [vehicleConfig setConfigID:[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 0)]];
            [vehicleConfig setQualifier:[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 1)]];
            [vehicleConfig setIndirect:sqlite3_column_int(cmpStmt, 2)];
            [vehicleConfigs addObject:vehicleConfig];
        }
    }
    sqlite3_finalize(cmpStmt);
    return vehicleConfigs;
}

- (Vehicle *)getVehicleByConfigID:(NSString *)configID{
    Vehicle *vehicle = [[Vehicle alloc] init];
    NSString *strSQL = [NSString stringWithFormat:@"SELECT y.ConfigID, y.YearID, ma.ID, ma.Make, mo.ID, mo.Model FROM ymm y JOIN makes ma ON y.MakeID = ma.ID JOIN models mo ON y.ModelID = mo.ID WHERE y.ConfigID = %@", configID];
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        while(sqlite3_step(cmpStmt) == SQLITE_ROW){
            [vehicle setConfigID:[NSNumber numberWithInt:sqlite3_column_int(cmpStmt, 0)]];
            [vehicle setYear:[NSNumber numberWithInt:sqlite3_column_int(cmpStmt, 1)]];
            [vehicle setMakeID:[NSNumber numberWithInt:sqlite3_column_int(cmpStmt, 2)]];
            [vehicle setMakeName:[[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 3)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            [vehicle setModelID:[NSNumber numberWithInt:sqlite3_column_int(cmpStmt, 4)]];
            [vehicle setModelName:[[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 5)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            vehicle.commandWords = [[NSMutableArray alloc] init];
            NSString *strSQL2 = [NSString stringWithFormat:@"SELECT c.CheckWord, c.SelectWord, c.SoftwareVersion, sw.LowByteZero, sw.LowByteOne, sw.LowByteTwo, sw.LowByteThree, sw.HighByteZero, sw.HighByteOne, sw.HighByteTwo, sw.HighByteThree FROM commands c JOIN ymmcommands y ON c.ID = y.CommandID LEFT JOIN selectwordidrange sw ON c.SelectWord = sw.SelectWord WHERE y.ConfigID = %@", [vehicle configID]];
            const char *chrStmt2 = [strSQL2 UTF8String];
            sqlite3_stmt *cmpStmt2;
            if(sqlite3_prepare_v2(db, chrStmt2, -1, &cmpStmt2, NULL) == SQLITE_OK){
                while(sqlite3_step(cmpStmt2) == SQLITE_ROW){
                    CommandWord *cmdWord = [[CommandWord alloc] init];
                    [cmdWord setCheckWord:sqlite3_column_int(cmpStmt2, 0)];
                    [cmdWord setSelectWord:sqlite3_column_int(cmpStmt2, 1)];
                    [cmdWord setSoftwareVersion:sqlite3_column_int(cmpStmt2, 2)];
                    const char *lowByteText = (const char *)sqlite3_column_text(cmpStmt2, 3);
                    if(lowByteText != NULL){
                        [cmdWord setHasRange:YES];
                        [cmdWord setLowByteZero:sqlite3_column_int(cmpStmt2, 3)];
                        [cmdWord setLowByteOne:sqlite3_column_int(cmpStmt2, 4)];
                        [cmdWord setLowByteTwo:sqlite3_column_int(cmpStmt2, 5)];
                        [cmdWord setLowByteThree:sqlite3_column_int(cmpStmt2, 6)];
                        [cmdWord setHighByteZero:sqlite3_column_int(cmpStmt2, 7)];
                        [cmdWord setHighByteOne:sqlite3_column_int(cmpStmt2, 8)];
                        [cmdWord setHighByteTwo:sqlite3_column_int(cmpStmt2, 9)];
                        [cmdWord setHighByteThree:sqlite3_column_int(cmpStmt2, 10)];
                    }
                    [vehicle.commandWords addObject:cmdWord];
                }
            }
            sqlite3_finalize(cmpStmt2);
            vehicle.sensorNameFreqs = [[NSMutableArray alloc] init];
            strSQL2 = [NSString stringWithFormat:@"SELECT s.Name, s.Frequency, s.Material FROM sensors s JOIN ymmsensors y ON s.ID = y.SensorID WHERE y.ConfigID = %@", [vehicle configID]];
            chrStmt2 = [strSQL2 UTF8String];
            if(sqlite3_prepare_v2(db, chrStmt2, -1, &cmpStmt2, NULL) == SQLITE_OK){
                while(sqlite3_step(cmpStmt2) == SQLITE_ROW){
                    SensorNameFreq *sensorNameFreq = [[SensorNameFreq alloc] init];
                    [sensorNameFreq setName:[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt2, 0)]];
                    [sensorNameFreq setFrequency:[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt2, 1)]];
                    const char *valveMaterial = (const char *)sqlite3_column_text(cmpStmt2, 2);
                    if(valveMaterial != NULL){
                        [sensorNameFreq setValveMaterial:[NSString stringWithUTF8String:valveMaterial]];
                    }
                    [vehicle.sensorNameFreqs addObject:sensorNameFreq];
                }
            }
            sqlite3_finalize(cmpStmt2);
        }
    }
    sqlite3_finalize(cmpStmt);
    return vehicle;
}

- (NSArray *)getVehiclesByConfigIDs:(NSString *)configIDs{
    NSMutableArray *vehicles = [[NSMutableArray alloc] init];
    NSString *strSQL = [NSString stringWithFormat:@"SELECT y.YearID, ma.ID, ma.Make, mo.ID, mo.Model FROM ymm y JOIN makes ma ON y.MakeID = ma.ID JOIN models mo ON y.ModelID = mo.ID WHERE y.ConfigID IN %@", configIDs];
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        while(sqlite3_step(cmpStmt) == SQLITE_ROW){
            Vehicle *vehicle = [[Vehicle alloc] init];
            [vehicle setYear:[NSNumber numberWithInt:sqlite3_column_int(cmpStmt, 0)]];
            [vehicle setMakeID:[NSNumber numberWithInt:sqlite3_column_int(cmpStmt, 2)]];
            [vehicle setMakeName:[[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 3)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            [vehicle setModelID:[NSNumber numberWithInt:sqlite3_column_int(cmpStmt, 4)]];
            [vehicle setModelName:[[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 5)] stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]]];
            [vehicles addObject:vehicle];
        }
    }
    sqlite3_finalize(cmpStmt);
    return vehicles;
}

- (NSArray *)getAttachmentsForVehicle:(Vehicle *)vehicle{
    NSMutableArray *attachments = [[NSMutableArray alloc] init];
    NSString *strSQL = [NSString stringWithFormat:@"SELECT a.Level, a.Name, a.Text, a.Url FROM attachments a JOIN ymmattachments ya ON a.ID = ya.AttachmentID WHERE ya.ConfigID = %@", [vehicle configID]];
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        while(sqlite3_step(cmpStmt) == SQLITE_ROW){
            Attachment *attachment = [[Attachment alloc] init];
            [attachment setLevel:[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 0)]];
            [attachment setName:[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 1)]];
            [attachment setText:[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 2)]];
            [attachment setUrl:[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 3)]];
            [attachments addObject:attachment];
        }
    }
    sqlite3_finalize(cmpStmt);
    return attachments;
}

- (void)addAttachments:(NSArray *)attachments{
    for(Attachment *attachment in attachments){
        NSString *strSQL = [NSString stringWithFormat:@"INSERT INTO attachments (ID, Level, Name, Text, Url) VALUES (%d, '%@', '%@', '%@', '%@')", [attachment attachmentID], [attachment level], [attachment name], [attachment text], [attachment url]];
        const char *chrStmt = [strSQL UTF8String];
        sqlite3_stmt *cmpStmt;
        if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
            sqlite3_step(cmpStmt);
        }
        sqlite3_finalize(cmpStmt);
    }
}

- (void)addVehicles:(NSArray *)vehicles{
    for(Vehicle *vehicle in vehicles){
        NSString *strSQL = [NSString stringWithFormat:@"SELECT ConfigID FROM ymm WHERE ConfigID = %@", [vehicle configID]];
        const char *chrStmt = [strSQL UTF8String];
        sqlite3_stmt *cmpStmt;
        if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
            if(sqlite3_step(cmpStmt) == SQLITE_ROW){
                strSQL = [NSString stringWithFormat:@"UPDATE ymm SET YearID = %@, MakeID = %@, ModelID = %@, QualifierID = '%@', Qualifier = '%@', Indirect = %d WHERE ConfigID = %@", [vehicle year], [vehicle makeID], [vehicle modelID], [vehicle qualifierID], [vehicle qualifier], [vehicle indirect], [vehicle configID]];
            }else{
                strSQL = [NSString stringWithFormat:@"INSERT INTO ymm (ConfigID, YearID, MakeID, ModelID, QualifierID, Qualifier, Indirect) VALUES (%@, %@, %@, %@, '%@', '%@', %d)", [vehicle configID], [vehicle year], [vehicle makeID], [vehicle modelID], [vehicle qualifierID], [vehicle qualifier], [vehicle indirect]];
            }
            chrStmt = [strSQL UTF8String];
            if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
                sqlite3_step(cmpStmt);
            }
            
        }
        strSQL = [NSString stringWithFormat:@"INSERT INTO makes (ID, Make) VALUES (%@, '%@')", [vehicle makeID], [vehicle makeName]];
        chrStmt = [strSQL UTF8String];
        if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
            sqlite3_step(cmpStmt);
        }
        strSQL = [NSString stringWithFormat:@"INSERT INTO models (ID, Model) VALUES (%@, '%@')", [vehicle modelID], [vehicle modelName]];
        chrStmt = [strSQL UTF8String];
        if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
            sqlite3_step(cmpStmt);
        }
        sqlite3_finalize(cmpStmt);
        for(CommandWord *cmdWord in vehicle.commandWords){
            int cmdID;
            strSQL = [NSString stringWithFormat:@"SELECT ID FROM commands WHERE CheckWord = %d AND SelectWord = %d AND SoftwareVersion = %d", [cmdWord checkWord], [cmdWord selectWord], [cmdWord softwareVersion]];
            chrStmt = [strSQL UTF8String];
            if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
                if(sqlite3_step(cmpStmt) == SQLITE_ROW){
                    cmdID = sqlite3_column_int(cmpStmt, 0);
                }else{
                    strSQL = [NSString stringWithFormat:@"INSERT INTO commands (CheckWord, SelectWord, SoftwareVersion) VALUES (%d, %d, %d)", [cmdWord checkWord], [cmdWord selectWord], [cmdWord softwareVersion]];
                    chrStmt = [strSQL UTF8String];
                    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
                        sqlite3_step(cmpStmt);
                        cmdID = (int)sqlite3_last_insert_rowid(db);
                    }
                }
            }
            strSQL = [NSString stringWithFormat:@"SELECT ConfigID, CommandID FROM ymmcommands WHERE ConfigID = %@ AND CommandID = %d", [vehicle configID], cmdID];
            chrStmt = [strSQL UTF8String];
            if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
                if(sqlite3_step(cmpStmt) != SQLITE_ROW){
                    strSQL = [NSString stringWithFormat:@"INSERT INTO ymmcommands (ConfigID, CommandID) VALUES (%@, %d)", [vehicle configID], cmdID];
                    chrStmt = [strSQL UTF8String];
                    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
                        sqlite3_step(cmpStmt);
                    }
                }
            }
            sqlite3_finalize(cmpStmt);
        }
        for(NSNumber *attID in vehicle.attachmentIDs){
            strSQL = [NSString stringWithFormat:@"SELECT ConfigID, AttachmentID FROM ymmattachments WHERE ConfigID = %@ AND AttachmentID = %d", [vehicle configID], [attID intValue]];
            chrStmt = [strSQL UTF8String];
            if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
                if(sqlite3_step(cmpStmt) != SQLITE_ROW){
                    strSQL = [NSString stringWithFormat:@"INSERT INTO ymmattachments (ConfigID, AttachmentID) VALUES (%d, %d)", [[vehicle configID] intValue], [attID intValue]];
                    chrStmt = [strSQL UTF8String];
                    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
                        sqlite3_step(cmpStmt);
                    }
                }
            }
            sqlite3_finalize(cmpStmt);
        }
        for(SensorNameFreq *sensorNameFreq in vehicle.sensorNameFreqs){
            int snfID;
            strSQL = [NSString stringWithFormat:@"SELECT ID FROM sensors WHERE Name = '%@' AND Frequency = '%@'", [sensorNameFreq name], [sensorNameFreq frequency]];
            chrStmt = [strSQL UTF8String];
            if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
                if(sqlite3_step(cmpStmt) == SQLITE_ROW){
                    snfID = sqlite3_column_int(cmpStmt, 0);
                }else{
                    strSQL = [NSString stringWithFormat:@"INSERT INTO sensors (Name, Frequency) VALUES ('%@', '%@')", [sensorNameFreq name], [sensorNameFreq frequency]];
                    chrStmt = [strSQL UTF8String];
                    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
                        sqlite3_step(cmpStmt);
                        snfID = (int)sqlite3_last_insert_rowid(db);
                    }
                }
            }
            strSQL = [NSString stringWithFormat:@"SELECT ConfigID, SensorID FROM ymmsensors WHERE ConfigID = %@ AND SensorID = %d", [vehicle configID], snfID];
            chrStmt = [strSQL UTF8String];
            if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
                if(sqlite3_step(cmpStmt) != SQLITE_ROW){
                    strSQL = [NSString stringWithFormat:@"INSERT INTO ymmsensors (ConfigID, SensorID) VALUES (%@, %d)", [vehicle configID], snfID];
                    chrStmt = [strSQL UTF8String];
                    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
                        sqlite3_step(cmpStmt);
                    }
                }
            }
            sqlite3_finalize(cmpStmt);
        }
    }
    [[UIApplication sharedApplication] setIdleTimerDisabled:NO];
}

- (void)addHistory:(NSNumber *)configID act:(NSNumber *)action{
    NSDateFormatter *dFormat = [[NSDateFormatter alloc] init];
    [dFormat setDateFormat:@"MM/dd/yyyy hh:mm:ss"];
    NSString *fDate = [dFormat stringFromDate:[NSDate date]];
    NSString *strSQL = [NSString stringWithFormat:@"INSERT INTO history (ConfigID, Date, ActionID) VALUES (%@, '%@', %@)", configID, fDate, action];
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        sqlite3_step(cmpStmt);
    }
    sqlite3_finalize(cmpStmt);
    [self getHistory:action];
}

- (NSArray *)getHistory:(NSNumber *)action{
    NSString *strSQL = @"DELETE FROM history WHERE Date < CURRENT_DATE - 30";
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        sqlite3_step(cmpStmt);
    }
    sqlite3_finalize(cmpStmt);
    NSMutableArray *histories = [[NSMutableArray alloc] init];
    strSQL = [NSString stringWithFormat:@"SELECT h.Date, y.YearID, ma.Make, mo.Model, a.Action FROM history h JOIN ymm y ON h.ConfigID = y.ConfigID JOIN makes ma ON y.MakeID = ma.ID JOIN models mo ON y.ModelID = mo.ID JOIN actions a ON h.ActionID = a.ID WHERE h.ActionID = %@", action];
    chrStmt = [strSQL UTF8String];
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        while(sqlite3_step(cmpStmt) == SQLITE_ROW){
            History *history = [[History alloc] init];
            NSDateFormatter *dFormat = [[NSDateFormatter alloc] init];
            [dFormat setDateFormat:@"MM/dd/yyyy hh:mm:ss"];
            [history setDate:[dFormat dateFromString:[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 0)]]];
            [history setYear:[NSNumber numberWithInt:sqlite3_column_int(cmpStmt, 1)]];
            [history setMake:[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 2)]];
            [history setModel:[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 3)]];
            [history setAction:[NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 4)]];
            [histories addObject:history];
        }
    }
    sqlite3_finalize(cmpStmt);
    return histories;
}

- (NSArray *)getActions{
    NSMutableArray *actions = [[NSMutableArray alloc] init];
    NSString *strSQL = @"SELECT ConfigID, ActionID FROM history";
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        while(sqlite3_step(cmpStmt) == SQLITE_ROW){
            Action *action = [[Action alloc] init];
            [action setConfigID:sqlite3_column_int(cmpStmt, 0)];
            [action setActionID:sqlite3_column_int(cmpStmt, 1)];
            [actions addObject:action];
        }
    }
    sqlite3_finalize(cmpStmt);
    return actions;
}

- (void)deleteHistory{
    NSString *strSQL = @"DELETE FROM history";
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        sqlite3_step(cmpStmt);
    }
    sqlite3_finalize(cmpStmt);
}

- (NSString *)getPartNumByUPC:(NSString *)upc{
    NSString *strSQL = [NSString stringWithFormat:@"SELECT PartNum FROM upcpartnums WHERE Upc = %@", upc];
    const char *chrStmt = [strSQL UTF8String];
    sqlite3_stmt *cmpStmt;
    if(sqlite3_prepare_v2(db, chrStmt, -1, &cmpStmt, NULL) == SQLITE_OK){
        if(sqlite3_step(cmpStmt) == SQLITE_ROW){
            return [NSString stringWithUTF8String:(char *)sqlite3_column_text(cmpStmt, 0)];
        }
    }
    sqlite3_finalize(cmpStmt);
    return @"";
}

@end
