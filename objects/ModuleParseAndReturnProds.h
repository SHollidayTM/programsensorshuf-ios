//
//  ModuleParseAndReturnProds.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 3/30/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SensorTPMS.h"
#import "ServiceKit.h"

@interface ModuleParseAndReturnProds : NSObject{
    NSMutableArray *programmables;
    NSMutableArray *nonprogrammables;
    NSMutableArray *serviceKits;
}

+ (id)sharedInstance;
- (void)parseAndReturn:(NSJSONSerialization *)json;

@end
