//
//  Vehicle.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/25/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Vehicle : NSObject

@property (nonatomic, strong) NSNumber *configID;
@property (nonatomic, strong) NSNumber *year;
@property (nonatomic, strong) NSNumber *makeID;
@property (nonatomic, strong) NSString *makeName;
@property (nonatomic, strong) NSNumber *modelID;
@property (nonatomic, strong) NSString *modelName;
@property (nonatomic, strong) NSString *qualifierID;
@property (nonatomic, strong) NSString *qualifier;
@property (assign) BOOL indirect;
@property (nonatomic, strong) NSMutableArray *commandWords;
@property (nonatomic, strong) NSMutableArray *attachmentIDs;
@property (nonatomic, strong) NSMutableArray *sensorNameFreqs;


@end
