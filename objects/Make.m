//
//  Make.m
//  alligator-iphone
//
//  Created by Scott Holliday on 10/26/15.
//  Copyright © 2015 TireMetrix. All rights reserved.
//

#import "Make.h"

@implementation Make

- (void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeInteger:_identifier forKey:@"identifier"];
    [encoder encodeObject:_name forKey:@"name"];
}

- (id)initWithCoder:(NSCoder *)decoder{
    self = [super init];
    if( self != nil){
        _identifier = [decoder decodeIntForKey:@"identifier"];
        _name = [decoder decodeObjectForKey:@"name"];
    }
    return self;
}

@end
