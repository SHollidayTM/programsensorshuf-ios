//
//  ModuleRegister.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 5/19/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModuleLanguage.h"
#import "ModuleGetKey.h"
#import "BTObject.h"

@interface ModuleRegister : NSObject{
    BOOL attemptedOnce;
}

+ (id)sharedInstance;
- (void)go;

@end
