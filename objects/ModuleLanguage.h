//
//  ModuleLanguage.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 5/6/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ModuleLanguage : NSObject <NSXMLParserDelegate>{
    NSString *currAtttribute;
}

@property(nonatomic, strong) NSMutableDictionary *phrases;

+ (id)sharedInstance;

@end
