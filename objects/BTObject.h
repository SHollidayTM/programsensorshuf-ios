//
//  BTObject.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 5/13/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreBluetooth/CoreBluetooth.h>

@interface BTObject : NSObject

@property (nonatomic, strong) CBPeripheral *peripheral;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *serialNum;
@property (nonatomic, strong) NSString *firmwareVersion;
@property (nonatomic, strong) NSString *hardwareVersion;
@property (nonatomic, strong) NSString *databaseVersion;
@property (assign) BOOL obdDevice;
@property (assign) BOOL connected;

@end
