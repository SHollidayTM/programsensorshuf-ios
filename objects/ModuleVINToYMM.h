//
//  VINToYMMModule.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 12/7/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModuleLanguage.h"

@interface ModuleVINToYMM : NSObject <NSXMLParserDelegate>{
    NSString *year;
    NSString *make;
    NSString *model;
}

+ (id)sharedInstance;
- (void)returnYMM:(NSString *)sentVIN;

@end
