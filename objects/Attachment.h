//
//  TipAndWarning.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/7/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Attachment : NSObject

@property (assign) int attachmentID;
@property (nonatomic, strong) NSString *level;
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *text;
@property (nonatomic, strong) NSString *url;

@end
