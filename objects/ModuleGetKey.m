//
//  ModuleGetKey.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 3/29/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "ModuleGetKey.h"

@implementation ModuleGetKey

+ (id)sharedInstance{
    static ModuleGetKey *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)getSecurityKey:(void (^)(BOOL success))callback{
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@User/Login", [[NSBundle mainBundle] objectForInfoDictionaryKey:@"apiUrl"]]]];
    [request setHTTPMethod:@"POST"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    NSString *body = @"{userName:\"HufUser1\", password:\"Doesn'tMatter'\", toolID:\"FAKETOOLID\"}";
    [request setHTTPBody:[body dataUsingEncoding:NSUTF8StringEncoding]];
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *task = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error){
        if(error){
            callback(NO);
        }
        NSHTTPURLResponse *httpResp = (NSHTTPURLResponse *)response;
        if(httpResp.statusCode != 200){
            callback(NO);
        }else{
            NSJSONSerialization *json = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
            [[NSUserDefaults standardUserDefaults] setObject:[json valueForKey:@"SecurityKey"] forKey:@"securityKey"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            callback(YES);
        }
    }];
    [task resume];
}


@end
