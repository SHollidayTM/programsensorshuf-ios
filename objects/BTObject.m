//
//  BTObject.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 5/13/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "BTObject.h"

@implementation BTObject

- (void)encodeWithCoder:(NSCoder *)encoder{
    [encoder encodeObject:_name forKey:@"name"];
    [encoder encodeObject:_address forKey:@"address"];
    [encoder encodeObject:_name forKey:@"serialNum"];
    [encoder encodeObject:_address forKey:@"firmwareVersion"];
    [encoder encodeObject:_name forKey:@"hardwardVersion"];
    [encoder encodeObject:_address forKey:@"databaseVersion"];
    [encoder encodeBool:_connected forKey:@"connected"];
}

- (id)initWithCoder:(NSCoder *)decoder{
    self = [super init];
    if(self != nil){
        _name = [decoder decodeObjectForKey:@"name"];
        _address = [decoder decodeObjectForKey:@"address"];
        _serialNum = [decoder decodeObjectForKey:@"serialNum"];
        _firmwareVersion = [decoder decodeObjectForKey:@"firmwareVersion"];
        _hardwareVersion = [decoder decodeObjectForKey:@"hardwareVersion"];
        _databaseVersion = [decoder decodeObjectForKey:@"databaseVersion"];
        _connected = [decoder decodeObjectForKey:@"connected"];
    }
    return self;
}

@end
