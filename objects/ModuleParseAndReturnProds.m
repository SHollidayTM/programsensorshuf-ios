//
//  ModuleParseAndReturnProds.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 3/30/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "ModuleParseAndReturnProds.h"

@implementation ModuleParseAndReturnProds

+ (id)sharedInstance{
    static ModuleParseAndReturnProds *sharedInstance = nil;
    @synchronized(self){
        if(sharedInstance == nil){
            sharedInstance = [[self alloc] init];
        }
    }
    return sharedInstance;
}

- (void)parseAndReturn:(NSJSONSerialization *)json{
    programmables = [[NSMutableArray alloc] init];
    nonprogrammables = [[NSMutableArray alloc] init];
    serviceKits = [[NSMutableArray alloc] init];
    NSArray *arrJson = [json copy];
    for(NSDictionary *object in arrJson){
        NSString *catalogID = [object objectForKey:@"CatalogItemID"];
        NSString *partNumber = [object objectForKey:@"PartNumber"];
        NSString *name = [object objectForKey:@"Name"];
        NSString *category = [object objectForKey:@"Category"];
        NSString *sameAsNumber = [object objectForKey:@"SameAsNumber"];
        NSString *alternateNumber = [object objectForKey:@"AlternatePartNumber"];
        NSString *description = [object objectForKey:@"Description"];
        NSString *relatedPartNumber = [object objectForKey:@"RelatedPartNumber"];
        NSString *manufacturer = [object objectForKey:@"Manufacturer"];
        NSString *imageUrl = [object objectForKey:@"ImageUrl"];
        NSString *fileName = [NSString stringWithFormat:@"%@.jpg", partNumber];
        NSFileManager *mgrFile = [NSFileManager defaultManager];
        NSString *strDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
        NSString *strPath = [strDir stringByAppendingPathComponent:fileName];
        if(![mgrFile fileExistsAtPath:strPath]){
            UIImage *img = [UIImage imageNamed:fileName];
            if(img){
                [UIImagePNGRepresentation(img) writeToFile:strPath atomically:NO];
            }else{
                if(![imageUrl isKindOfClass:[NSNull class]]){
                    NSData *imgData = [NSData dataWithContentsOfURL:[NSURL URLWithString:imageUrl]];
                    [imgData writeToFile:strPath atomically:NO];
                }
            }
        }
        NSArray *arrAttributes = [object objectForKey:@"Attributes"];
        if([category isEqualToString:@"Sensors"]){
            SensorTPMS *sensor = [[SensorTPMS alloc] init];
            [sensor setCatalogID:catalogID];
            [sensor setPartNumber:partNumber];
            [sensor setName:name];
            [sensor setCategory:category];
            [sensor setSameAsNumber:sameAsNumber];
            [sensor setAlternateNumber:alternateNumber];
            [sensor setDesc:description];
            [sensor setRelatedPartNumber:relatedPartNumber];
            [sensor setManufacturer:manufacturer];
            for(NSDictionary *attribute in arrAttributes){
                NSString *attName = [attribute objectForKey:@"Name"];
                NSString *attValue = [attribute objectForKey:@"Value"];
                if([attName isEqualToString:@"Frequency"]){
                    NSString *freq = [attValue stringByReplacingOccurrencesOfString:@" MHz" withString:@""];
                    [sensor setFrequency:freq];
                }else if([attName isEqualToString:@"Torque Nut"]){
                    [sensor setTorqueNut:attValue];
                }else if([attName isEqualToString:@"Torque Screw"]){
                    [sensor setTorqueScrew:attValue];
                }else if([attName isEqualToString:@"Torque Core"]){
                    [sensor setTorqueCore:attValue];
                }else if([attName isEqualToString:@"Housing Color"]){
                    [sensor setHousingColor:attValue];
                }else if([attName isEqualToString:@"Sensor Type"]){
                    [sensor setType:attValue];
                }else if([attName isEqualToString:@"Frequency"]){
                    [sensor setFrequency:attValue];
                }else if([attName isEqualToString:@"Part Design"]){
                    [sensor setPartDesign:attValue];
                }else if([attName isEqualToString:@"Valve Material"]){
                    [sensor setValveMaterial:attValue];
                }else if([attName isEqualToString:@"Shape"]){
                    [sensor setShape:attValue];
                }else if([attName isEqualToString:@"Category"]){
                    [sensor setAttCategory:attValue];
                    [sensor setProgrammable:NO];
                    if([[[sensor attCategory] uppercaseString] containsString:@"PROGRAMMABLE"]){
                        [sensor setProgrammable:YES];
                    }
                }else if([attName isEqualToString:@"Sensor ID Length"]){
                    [sensor setIdLength:attValue];
                }else if([attName isEqualToString:@"Sensor ID Type"]){
                    [sensor setIdType:attValue];
                }else if([attName isEqualToString:@"Who sensor was made for"]){
                    [sensor setMadeFor:attValue];
                }
            }
            if([sensor attCategory]){
                if(![[sensor attCategory] isEqualToString:@"OEM"]){
                    if([sensor programmable]){
                        [programmables addObject:sensor];
                    }else{
                        if([[partNumber substringWithRange:NSMakeRange(0, 4)] isEqualToString:@"RDE0"]){
                            NSString *version = [partNumber substringWithRange:NSMakeRange([partNumber length]-3, 3)];
                            if([version isEqualToString:@"V21"] || [version isEqualToString:@"V41"]){
                                [nonprogrammables addObject:sensor];
                            }
                        }else{
                            [nonprogrammables addObject:sensor];
                        }
                    }
                }
            }
        }else if([category isEqualToString:@"Service Kits"]){
            if([[partNumber substringWithRange:NSMakeRange(0, 4)] isEqualToString:@"RDV0"]){
                NSArray *arrPartNum = [partNumber componentsSeparatedByString:@"0"];
                NSString *svNum = arrPartNum[[arrPartNum count]-1];
                if([svNum length] > 0){
                    if(([svNum intValue] > 20) && ([svNum intValue] < 28)){
                        ServiceKit *serviceKit = [[ServiceKit alloc] init];
                        [serviceKit setCatalogID:catalogID];
                        [serviceKit setPartNumber:partNumber];
                        [serviceKit setName:name];
                        [serviceKit setCategory:category];
                        [serviceKit setSameAsNumber:sameAsNumber];
                        [serviceKit setAlternateNumber:alternateNumber];
                        [serviceKit setDesc:description];
                        [serviceKit setRelatedPartNumber:relatedPartNumber];
                        [serviceKit setManufacturer:manufacturer];
                        for(NSDictionary *attribute in arrAttributes){
                            NSString *attName = [attribute objectForKey:@"Name"];
                            NSString *attValue = [attribute objectForKey:@"Value"];
                            if([attName isEqualToString:@"Finish"]){
                                [serviceKit setFinish:attValue];
                            }else if([attName isEqualToString:@"Cap"]){
                                [serviceKit setCap:attValue];
                            }else if([attName isEqualToString:@"Hex Nut"]){
                                [serviceKit setHexNut:attValue];
                            }else if([attName isEqualToString:@"Metal Washer"]){
                                [serviceKit setMetalWasher:attValue];
                            }else if([attName isEqualToString:@"Metal Valve Included"]){
                                [serviceKit setMetalValveIncluded:NO];
                                if([attName isEqualToString:@"Yes"]){
                                    [serviceKit setMetalValveIncluded:YES];
                                }
                            }else if([attName isEqualToString:@"Complete Valve Assembly"]){
                                [serviceKit setCompleteValveAssembly:NO];
                                if([attName isEqualToString:@"Yes"]){
                                    [serviceKit setCompleteValveAssembly:YES];
                                }
                            }else if([attName isEqualToString:@"Core"]){
                                [serviceKit setCore:attValue];
                            }else if([attName isEqualToString:@"Torque Core"]){
                                [serviceKit setTorqueCore:attValue];
                            }else if([attName isEqualToString:@"Torque Nut"]){
                                [serviceKit setTorqueNut:attValue];
                                [serviceKit setTorqueScrew:attValue]; //get this fixed
                            }else if([attName isEqualToString:@"Grommet"]){
                                [serviceKit setGrommet:attValue];
                            }else if([attName isEqualToString:@"Valve Length"]){
                                [serviceKit setValveLength:attValue];
                            }
                        }
                        [serviceKits addObject:serviceKit];
                    }
                }
            }
        }
    }
    NSDictionary *dic = @{@"programmables":programmables, @"nonprogrammables":nonprogrammables, @"serviceKits":serviceKits};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"ReturnProducts" object:dic];
}

@end
