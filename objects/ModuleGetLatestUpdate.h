//
//  ModuleGetLatestUpdate.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 5/26/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "ModuleGetKey.h"
#import "ModuleGetNewVehicles.h"

@interface ModuleGetLatestUpdate : NSObject{
    BOOL attemptedOnce;
}

+ (id)sharedInstance;
- (void)go;

@end
