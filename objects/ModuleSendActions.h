//
//  ModuleSendActions.h
//  progsensors-ipad
//
//  Created by Scott Holliday on 6/1/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ModuleLanguage.h"
#import "ModuleGetKey.h"
#import "Action.h"
#import "BTObject.h"

@interface ModuleSendActions : NSObject{
    BOOL attemptedOnce;
    NSArray *actions;
}

+ (id)sharedInstance;
- (void)go:(NSArray *)sentActions;

@end
