//
//  ModuleGetNewVehicles.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/5/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "ModuleGetKey.h"
#import "Attachment.h"
#import "Vehicle.h"
#import "CommandWord.h"
#import "SensorNameFreq.h"

@interface ModuleGetNewVehicles : NSObject{
    BOOL attemptedOnce;
    NSMutableArray *attachments;
    NSMutableArray *vehicles;
}

+ (id)sharedInstance;
- (void)go;

@end
