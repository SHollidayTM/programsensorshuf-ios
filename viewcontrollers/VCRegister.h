//
//  VCRegister.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/24/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "ModuleRegister.h"

@interface VCRegister : UIViewController{
    IBOutlet UITextField *txtName;
    IBOutlet UITextField *txtCompanyName;
    IBOutlet UITextField *txtAddressOne;
    IBOutlet UITextField *txtAddressTwo;
    IBOutlet UITextField *txtCity;
    IBOutlet UITextField *txtState;
    IBOutlet UITextField *txtZip;
    IBOutlet UITextField *txtPhone;
    IBOutlet UITextField *txtEmail;
    IBOutlet UIButton *btnGo;
}

- (IBAction)close;

@end
