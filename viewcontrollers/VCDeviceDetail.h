//
//  VCDeviceDetail.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/25/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "ModuleLanguage.h"
#import "SensorTPMS.h"
#import "ServiceKit.h"
#import "ModuleBT.h"

@interface VCDeviceDetail : UIViewController{
    SensorTPMS *sensor;
    ServiceKit *serviceKit;
    NSString *name;
    UIImage *imgDevice;
    IBOutlet UIScrollView *scrContent;
    IBOutlet UIView *vContent;
    IBOutlet UIImageView *imvDevice;
    IBOutlet UIButton *btnDevice;
    Reachability *reach;
    UIView *vEnlarged;
    int heightOne;
    int heightTwo;
    int heightThree;
    int heightFour;
    int heightFive;
    int heightSix;
}

- (void)setupSensor:(SensorTPMS *)sentSensor ymm:(BOOL)inYMM;
- (void)setupDirectFit:(SensorTPMS *)sentSensor;
- (void)setupServiceKit:(ServiceKit *)sentServiceKit;
- (IBAction)enlargeImage;

@end
