//
//  VCHolder.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 3/30/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "VCHolder.h"

@interface VCHolder ()

@end

@implementation VCHolder

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(byYMMFromVIN:) name:@"ByYMMFromVIN" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(errorReturned:) name:@"ReturnError" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(productsReturned:) name:@"ReturnProducts" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openDeviceDetail:) name:@"OpenYMMDetail" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openService:) name:@"OpenService" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openCheck) name:@"OpenCheck" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openProgram) name:@"OpenProgram" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openSelect) name:@"OpenSelect" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sensorNameFreqSelected:) name:@"SensorNameFreqSelected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openCreate) name:@"OpenCreate" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openCopy) name:@"OpenCopy" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openCopySet) name:@"OpenCopySet" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkAttachments) name:@"CheckAttachments" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openTechDocs) name:@"OpenTechDocs" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkSensor:) name:@"CheckSensor" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createSensor:) name:@"CreateSensor" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(copySensor:) name:@"CopySensor" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createFromCopy) name:@"CreateFromCopy" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(copySetSensor:) name:@"CopySetSensor" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createFromSet:) name:@"CreateFromSet" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(freqDiscovered:) name:@"FreqDiscovered" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(freqUndiscovered) name:@"FreqUndiscovered" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openOBD) name:@"OpenOBD" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(popBack) name:@"PopBack" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(relearnReturned:) name:@"ReturnRelearn" object:nil];
    UIBarButtonItem *bbiBack = [[UIBarButtonItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    itmFirst = [[barNav items] objectAtIndex:0];
    [itmFirst setBackBarButtonItem:bbiBack];
    reach = [Reachability reachabilityWithHostname:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"apiUrl"]];
}

- (void)openYMM{
    inYMM = YES;
    [itmFirst setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_selectYMM"]];
    vcByYMM = [[VCByYMM alloc] initWithNibName:@"ByYMM" bundle:nil];
    [vcByYMM setInService:_inService];
    [vcByYMM.view setFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
    [self.view addSubview:vcByYMM.view];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [self.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [UIView commitAnimations];
}

- (void)openVIN{
    inYMM = YES;
    [itmFirst setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_searchByVIN"]];
    vcByVIN = [[VCByVIN alloc] initWithNibName:@"ByVIN" bundle:nil];
    [vcByVIN.view setFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
    [self.view addSubview:vcByVIN.view];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [self.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [UIView commitAnimations];
}

- (void)openProd{
    inYMM = NO;
    [itmFirst setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_prodSearch"]];
    vcByProduct = [[VCByProduct alloc] initWithNibName:@"ByProduct" bundle:nil];
    [vcByProduct.view setFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
    [self.view addSubview:vcByProduct.view];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [self.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [UIView commitAnimations];
}

- (void)errorReturned:(NSNotification *)notification{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *dic = [notification object];
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:[dic objectForKey:@"title"] message:[dic objectForKey:@"text"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
    });
}

- (void)productsReturned:(NSNotification *)notification{
    if(_stopFetching){
        return;
    }
    NSDictionary *dic = [notification object];
    programmables = [dic objectForKey:@"programmables"];
    nonprogrammables = [dic objectForKey:@"nonprogrammables"];
    serviceKits = [dic objectForKey:@"serviceKits"];
    dispatch_async(dispatch_get_main_queue(), ^{
        if(([programmables count] > 0) || ([nonprogrammables count] > 0) || ([serviceKits count] > 0)){
            if(inYMM){
                itmDeviceResults = [[UINavigationItem alloc] initWithTitle:[NSString stringWithFormat:@"%@ %@ %@", [vcByYMM selectedYear], [[vcByYMM selectedMake] name], [[vcByYMM selectedModel] name]]];
                [self showDeviceResults];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
            }else{
                itmDeviceResults = [[UINavigationItem alloc] initWithTitle:[NSString stringWithFormat:@"%@ %@", [vcByProduct partNum], [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_results"]]];
                [self showDeviceResults];
                [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
            }
        }else{
            UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_noResults"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [ctrAlert addAction:actOk];
            [self presentViewController:ctrAlert animated:YES completion:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
        }
    });
}

- (void)showDeviceResults{
    if(_stopFetching){
        return;
    }
    UIBarButtonItem *bbiBack = [[UIBarButtonItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    [itmDeviceResults setBackBarButtonItem:bbiBack];
    [barNav pushNavigationItem:itmDeviceResults animated:YES];
    vcDeviceResults = [[VCDeviceResults alloc] initWithNibName:@"DeviceResults" bundle:nil];
    [vcDeviceResults.view setFrame:CGRectMake(self.view.frame.size.width, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
    [self.view addSubview:vcDeviceResults.view];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcDeviceResults.view setTransform:CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)];
    [UIView commitAnimations];
    [vcDeviceResults setup:programmables non:nonprogrammables kit:serviceKits typ:0];
}

- (void)openService:(NSNotification *)notification{
    if([notification object] != nil){
        selectedSensor = [notification object];
    }
    NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_loadingResults"],  @"hideCancel":@1};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:dic];
    [[ModuleGetRelearn sharedInstance] go:[[vcByYMM selectedVehicleConfig] configID]];
}

- (void)relearnReturned:(NSNotification *)notification{
    dispatch_async(dispatch_get_main_queue(), ^{
        sensorNameFreq = nil;
        freqChosen = NO;
        itmService = [[UINavigationItem alloc] initWithTitle:[NSString stringWithFormat:@"%@ %@ %@", [vcByYMM selectedYear], [[vcByYMM selectedMake] name], [[vcByYMM selectedModel] name]]];
        UIBarButtonItem *bbiBack = [[UIBarButtonItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_back"] style:UIBarButtonItemStylePlain target:nil action:nil];
        [itmService setBackBarButtonItem:bbiBack];
        [barNav pushNavigationItem:itmService animated:YES];
        _vcService = [[VCService alloc] initWithNibName:@"Service" bundle:nil];
        [_vcService.view setFrame:CGRectMake(self.view.frame.size.width, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
        [self.view addSubview:_vcService.view];
        BOOL indirect = NO;
        indirect = [[vcByYMM selectedVehicleConfig] indirect];
        vehicle = [[ModuleDB sharedInstance] getVehicleByConfigID:[[vcByYMM selectedVehicleConfig] configID]];
        if(notification != nil){
            relearn = [notification object];
        }
        [_vcService setup:indirect veh:vehicle rel:relearn];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.25];
        [_vcService.view setTransform:CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)];
        [UIView commitAnimations];
    });
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
}

- (void)openDeviceDetail:(NSNotification *)notification{
    vcDeviceDetail = [[VCDeviceDetail alloc] initWithNibName:@"DeviceDetail" bundle:nil];
    [vcDeviceDetail.view setFrame:CGRectMake(self.view.frame.size.width, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
    [self.view addSubview:vcDeviceDetail.view];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcDeviceDetail.view setTransform:CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)];
    [UIView commitAnimations];
    NSDictionary *dic = [notification object];
    NSNumber *type = [dic objectForKey:@"type"];
    NSString *name;
    switch([type intValue]){
        case 0:{
            SensorTPMS *sensor = [dic objectForKey:@"sensor"];
            name = [sensor partNumber];
            [vcDeviceDetail setupSensor:sensor ymm:inYMM];
        }
            break;
        case 1:{
            SensorTPMS *sensor = [dic objectForKey:@"sensor"];
            name = [sensor partNumber];
            [vcDeviceDetail setupDirectFit:sensor];
        }
            break;
        case 2:{
            ServiceKit *serviceKit = [dic objectForKey:@"serviceKit"];
            name = [serviceKit partNumber];
            [vcDeviceDetail setupServiceKit:serviceKit];
        }
            break;
    }
    itmDeviceDetail = [[UINavigationItem alloc] initWithTitle:name];
    UIBarButtonItem *bbiBack = [[UIBarButtonItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    [itmDeviceDetail setBackBarButtonItem:bbiBack];
    [barNav pushNavigationItem:itmDeviceDetail animated:NO];
}

- (void)openCheck{
    BOOL valid = NO;
    if([vehicle.commandWords count] > 0){
        for(CommandWord *cmdWord in [vehicle commandWords]){
            if([cmdWord checkWord] != 0){
                valid = YES;
                break;
            }
        }
    }
    if(!valid){
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_noProtocol"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ClosePdfViewer" object:nil];
        itmCheck = [[UINavigationItem alloc] initWithTitle:[NSString stringWithFormat:@"%@ %@ %@ %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_check"], [vcByYMM selectedYear], [[vcByYMM selectedMake] name], [[vcByYMM selectedModel] name]]];
        UIBarButtonItem *bbiBack = [[UIBarButtonItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_back"] style:UIBarButtonItemStylePlain target:nil action:nil];
        [itmCheck setBackBarButtonItem:bbiBack];
        [barNav pushNavigationItem:itmCheck animated:YES];
        vcCheck = [[VCCheck alloc] initWithNibName:@"Check" bundle:nil];
        [vcCheck.view setFrame:CGRectMake(self.view.frame.size.width, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
        [self.view addSubview:vcCheck.view];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.25];
        [vcCheck.view setTransform:CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)];
        [UIView commitAnimations];
    }
}

- (void)openProgram{
    if(sensorNameFreq || [vehicle.sensorNameFreqs count] > 0){
        itmProgram = [[UINavigationItem alloc] initWithTitle:[NSString stringWithFormat:@"%@ %@ %@ %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_program"], [vcByYMM selectedYear], [[vcByYMM selectedMake] name], [[vcByYMM selectedModel] name]]];
        UIBarButtonItem *bbiBack = [[UIBarButtonItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_back"] style:UIBarButtonItemStylePlain target:nil action:nil];
        [itmProgram setBackBarButtonItem:bbiBack];
        [barNav pushNavigationItem:itmProgram animated:YES];
        vcProgram = [[VCProgram alloc] initWithNibName:@"Program" bundle:nil];
        [vcProgram.view setFrame:CGRectMake(self.view.frame.size.width, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
        [self.view addSubview:vcProgram.view];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.25];
        [vcProgram.view setTransform:CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)];
        [UIView commitAnimations];
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_noProtocol"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

- (void)openSelect{
    itmSelect = [[UINavigationItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_chooseSensor"]];
    UIBarButtonItem *bbiBack = [[UIBarButtonItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    [itmSelect setBackBarButtonItem:bbiBack];
    [barNav pushNavigationItem:itmSelect animated:YES];
    vcSelect = [[VCSelect alloc] initWithNibName:@"Select" bundle:nil];
    [vcSelect.view setFrame:CGRectMake(self.view.frame.size.width, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
    [self.view addSubview:vcSelect.view];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcSelect.view setTransform:CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)];
    [UIView commitAnimations];
    [vcSelect setup:[vehicle sensorNameFreqs]];
}

- (void)sensorNameFreqSelected:(NSNotification *)notification{
    sensorNameFreq = [notification object];
}

- (void)openCreate{
    itmCreate = [[UINavigationItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_createSensor"]];
    UIBarButtonItem *bbiBack = [[UIBarButtonItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] style:UIBarButtonItemStylePlain target:nil action:nil];
    [itmCreate setBackBarButtonItem:bbiBack];
    [barNav pushNavigationItem:itmCreate animated:YES];
    vcCreate = [[VCCreate alloc] initWithNibName:@"Create" bundle:nil];
    [vcCreate.view setFrame:CGRectMake(self.view.frame.size.width, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
    [self.view addSubview:vcCreate.view];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcCreate.view setTransform:CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)];
    [UIView commitAnimations];
    [self checkSensorNameFreq];
    if(sensorNameFreq){
        [vcCreate setup:@[sensorNameFreq] rel:relearn];
    }else{
        [vcCreate setup:[vehicle sensorNameFreqs] rel:relearn];
    }
}

- (void)openCopy{
    itmCopy = [[UINavigationItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_copySensor"]];
    UIBarButtonItem *bbiBack = [[UIBarButtonItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] style:UIBarButtonItemStylePlain target:nil action:nil];
    [itmCopy setBackBarButtonItem:bbiBack];
    [barNav pushNavigationItem:itmCopy animated:YES];
    vcCopy = [[VCCopy alloc] initWithNibName:@"Copy" bundle:nil];
    [vcCopy.view setFrame:CGRectMake(self.view.frame.size.width, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
    [self.view addSubview:vcCopy.view];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcCopy.view setTransform:CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)];
    [UIView commitAnimations];
    [self checkSensorNameFreq];
    if(sensorNameFreq){
        [vcCopy setup:@[sensorNameFreq] rel:relearn];
    }else{
        [vcCopy setup:[vehicle sensorNameFreqs] rel:relearn];
    }
}

- (void)openCopySet{
    itmCopySet = [[UINavigationItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_copySensorSet"]];
    UIBarButtonItem *bbiBack = [[UIBarButtonItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    [itmCopySet setBackBarButtonItem:bbiBack];
    [barNav pushNavigationItem:itmCopySet animated:YES];
    vcCopySet = [[VCCopySet alloc] initWithNibName:@"CopySet" bundle:nil];
    [vcCopySet.view setFrame:CGRectMake(self.view.frame.size.width, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
    [self.view addSubview:vcCopySet.view];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcCopySet.view setTransform:CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)];
    [UIView commitAnimations];
    [self checkSensorNameFreq];
    if(sensorNameFreq){
        [vcCopySet setup:@[sensorNameFreq] rel:relearn];
    }else{
        [vcCopySet setup:[vehicle sensorNameFreqs] rel:relearn];
    }
}

- (void)openTechDocs{
    itmTechDocs = [[UINavigationItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_techDocs"]];
    UIBarButtonItem *bbiBack = [[UIBarButtonItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] style:UIBarButtonItemStylePlain target:nil action:nil];
    [itmRelearn setBackBarButtonItem:bbiBack];
    [barNav pushNavigationItem:itmTechDocs animated:YES];
    vcTechDocs = [[VCTechDocs alloc] initWithNibName:@"TechDocs" bundle:nil];
    [vcTechDocs.view setFrame:CGRectMake(self.view.frame.size.width, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
    [self.view addSubview:vcTechDocs.view];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcTechDocs.view setTransform:CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)];
    [UIView commitAnimations];
}

- (void)checkSensorNameFreq{
    if(selectedSensor){
        for(SensorNameFreq *sNameFreq in [vehicle sensorNameFreqs]){
            if([[sNameFreq name] isEqualToString:[selectedSensor name]]){
                sensorNameFreq = sNameFreq;
                break;
            }
        }
    }else{
        sensorNameFreq = nil;
    }
}

- (void)checkAttachments{
    NSArray *attachments = [[ModuleDB sharedInstance] getAttachmentsForVehicle:vehicle];
    if([attachments count] > 0){
        NSString *message = @"";
        for(Attachment *attachment in attachments){
            if([attachment text]){
                NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:[attachment text] options:0];
                NSString *text = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
                message = [message stringByAppendingString:[NSString stringWithFormat:@"%@</br>%@</br>", [attachment name], text]];
            }
            NSString *key = [[[NSUserDefaults standardUserDefaults] stringForKey:@"securityKey"] stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLHostAllowedCharacterSet]];
            key = [key stringByReplacingOccurrencesOfString:@"+" withString:@"%2B"];
            NSString *url = [[attachment url] stringByReplacingOccurrencesOfString:@"{SecurityKey}" withString:key];
            message = [message stringByAppendingString:[NSString stringWithFormat:@"</br><a href=\"%@\">Click here to view</a></br></br>", url]];
        }
        if([message length] > 0){
            NSDictionary *dic = @{@"html":message};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenTroubleshooting" object:dic];
        }else{
            [self showNoAttachments];
        }
    }else{
        [self showNoAttachments];
    }
}

- (void)showNoAttachments{
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_noTroubleshooting"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
}

- (BOOL)navigationBar:(UINavigationBar *)navigationBar shouldPopItem:(UINavigationItem *)item{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    if([itmDeviceResults isEqual:item]){
        [vcDeviceResults.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    }else if([itmService isEqual:item]){
        [_vcService.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    }else if([itmDeviceDetail isEqual:item]){
        selectedSensor = nil;
        sensorNameFreq = nil;
        [vcDeviceDetail.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    }else if([itmCheck isEqual:item]){
        [vcCheck.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    }else if([itmProgram isEqual:item]){
        [vcProgram.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    }else if([itmCreate isEqual:item]){
        sensorNameFreq = nil;
        switch([vcCreate state]){
            case 0:
                [vcCreate.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
                break;
            case 1:
                if(selectedSensor){
                    [vcCreate.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
                }else{
                    [vcCreate openSensorSelect];
                    [UIView commitAnimations];
                    return NO;
                }
                break;
        }
    }else if([itmCopy isEqual:item]){
        sensorNameFreq = nil;
        switch([vcCopy state]){
            case 0:
                [vcCopy.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
                break;
            case 1:
                if(selectedSensor){
                    [vcCopy.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
                }else{
                    [vcCopy openSensorSelect];
                    [UIView commitAnimations];
                    return NO;
                }
                break;
        }
    }else if([itmCopySet isEqual:item]){
        sensorNameFreq = nil;
        [vcCopySet.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    }else if([itmRelearn isEqual:item]){
        [vcRelearn.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    }else if([itmTechDocs isEqual:item]){
        [vcTechDocs.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    }
    [UIView commitAnimations];
    return YES;
}

- (void)popBack{
    [barNav popNavigationItemAnimated:YES];
}

- (void)byYMMFromVIN:(NSNotification *)notification{
    if(_stopFetching){
        return;
    }
    dispatch_async(dispatch_get_main_queue(), ^{
        NSDictionary *dic = [notification object];
        if([dic objectForKey:@"error"]){
            UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[dic objectForKey:@"error"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [ctrAlert addAction:actOk];
            [self presentViewController:ctrAlert animated:YES completion:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
        }else{
            vcByYMM = [[VCByYMM alloc] initWithNibName:@"ByYMM" bundle:nil];
            [vcByYMM setInService:_inService];
            [vcByYMM setSelectedYear:[dic objectForKey:@"year"]];
            [vcByYMM setSelectedMake:[[ModuleDB sharedInstance] getMakeByMakeName:[dic objectForKey:@"make"]]];
            [vcByYMM setSelectedModel:[[ModuleDB sharedInstance] getModelByModelName:[dic objectForKey:@"model"]]];
            [vcByYMM.view setFrame:CGRectMake(0, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
            [vcByYMM.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
            [self.view addSubview:vcByYMM.view];
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:.25];
            [vcByYMM.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
            [UIView commitAnimations];
            [vcByYMM go];
        }
    });
}

- (void)checkSensor:(NSNotification *)notification{
    if([[ModuleBT sharedInstance] btObjectHC]){
        behavior = 1;
        NSDictionary *dic = [notification object];
        [[ModuleDB sharedInstance] addHistory:[vehicle configID] act:[dic objectForKey:@"action"]];
        if(sensorNameFreq){
            mhz = 0x01;
            if([[sensorNameFreq frequency] isEqualToString:@"433"]){
                mhz = 0x02;
            }
            [self finishFreq:0];
        }else{
            if(!freqChosen){
                [self getVehicleFreq:0];
            }else{
                [self finishFreq:0];
            }
        }
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowNoDevice" object:nil];
    }
}

- (void)createSensor:(NSNotification *)notification{
    if([[ModuleBT sharedInstance] btObjectHC]){
        behavior = 0;
        NSDictionary *dic = [notification object];
        [[ModuleDB sharedInstance] addHistory:[vehicle configID] act:[dic objectForKey:@"action"]];
        if(sensorNameFreq){
            mhz = 0x01;
            if([[sensorNameFreq frequency] isEqualToString:@"433"]){
                mhz = 0x02;
            }
            [self finishFreq:1];
        }else{
            if(!freqChosen){
                [self getVehicleFreq:1];
            }else{
                [self finishFreq:1];
            }
        }
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowNoDevice" object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoadSensor" object:nil];
    }
}

- (void)copySensor:(NSNotification *)notification{
    if([[ModuleBT sharedInstance] btObjectHC]){
        behavior = 2;
        NSDictionary *dic = [notification object];
        [[ModuleDB sharedInstance] addHistory:[vehicle configID] act:[dic objectForKey:@"action"]];
        if(sensorNameFreq){
            mhz = 0x01;
            if([[sensorNameFreq frequency] isEqualToString:@"433"]){
                mhz = 0x02;
            }
            [self finishFreq:0];
        }else{
            if(!freqChosen){
                [self getVehicleFreq:0];
            }else{
                [self finishFreq:0];
            }
        }
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowNoDevice" object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoadSensor" object:nil];
    }
}

- (void)copySetSensor:(NSNotification *)notification{
    if([[ModuleBT sharedInstance] btObjectHC]){
        behavior = 3;
        NSDictionary *dic = [notification object];
        [[ModuleDB sharedInstance] addHistory:[vehicle configID] act:[dic objectForKey:@"action"]];
        if(sensorNameFreq){
            mhz = 0x01;
            if([[sensorNameFreq frequency] isEqualToString:@"433"]){
                mhz = 0x02;
            }
            [self finishFreq:0];
        }else{
            if(!freqChosen){
                [self getVehicleFreq:0];
            }else{
                [self finishFreq:0];
            }
        }
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowNoDevice" object:nil];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoadSensor" object:nil];
    }
}

- (void)createFromCopy{
    [[ModuleBT sharedInstance] createFromCopy];
}

- (void)createFromSet:(NSNotification *)notification{
    NSDictionary *dic = [notification object];
    [[ModuleBT sharedInstance] createFromSet:[dic objectForKey:@"id"]];
}

- (void)getVehicleFreq:(int)checkOrCreate{
    if([[vehicle sensorNameFreqs] count] > 0){
        if([[vehicle sensorNameFreqs] count] > 1){
            NSMutableArray *frequencies = [[NSMutableArray alloc] init];
            NSString *frequency = @"";
            for(SensorNameFreq *sNFreq in [vehicle sensorNameFreqs]){
                if(([frequency length] < 1) || !([frequency isEqualToString:[sNFreq frequency]])){
                    frequency = [sNFreq frequency];
                    [frequencies addObject:frequency];
                }
            }
            if([frequencies count] > 1){
                UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:nil preferredStyle:UIAlertControllerStyleAlert];
                for(NSString *freq in frequencies){
                    UIAlertAction *act = [UIAlertAction actionWithTitle:[NSString stringWithFormat:@"%@ %@", freq, [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_mhz"]] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        [self dismissViewControllerAnimated:YES completion:nil];
                        mhz = 0x01;
                        if([freq isEqualToString:@"433"]){
                            mhz = 0x02;
                        }
                        freqChosen = YES;
                        [self finishFreq:checkOrCreate];
                    }];
                    [ctrAlert addAction:act];
                }
                UIAlertAction *actAuto = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_discoverAutomatically"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [self helpWithFrequency];
                }];
                [ctrAlert addAction:actAuto];
                [self presentViewController:ctrAlert animated:YES completion:nil];
            }else{
                sensorNameFreq = [[vehicle sensorNameFreqs] objectAtIndex:0];
                mhz = 0x01;
                if([[sensorNameFreq frequency] isEqualToString:@"433"]){
                    mhz = 0x02;
                }
                [self finishFreq:checkOrCreate];
            }
        }else{
            sensorNameFreq = [[vehicle sensorNameFreqs] objectAtIndex:0];
            mhz = 0x01;
            if([[sensorNameFreq frequency] isEqualToString:@"433"]){
                mhz = 0x02;
            }
            [self finishFreq:checkOrCreate];
        }
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_noSensors"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

- (void)helpWithFrequency{
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_helpFindFreq"] message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_placeIntellisens"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
        [self doDiscoverFreq];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
}

- (void)doDiscoverFreq{
    NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_discoverFreq"],  @"hideCancel":@1};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoadSensor" object:dic];
    [[ModuleBT sharedInstance] checkSensor:[vehicle commandWords] mhz:0x01 chk:YES];
}

- (void)finishFreq:(int)checkOrCreate{
    switch(checkOrCreate){
        case 0:
            [self writeCCCS];
            break;
        case 1:
            [self writeCreate];
            break;
    }
}

- (void)openOBD{
    BOOL valid = NO;
    if([vehicle.commandWords count] > 0){
        for(CommandWord *cmdWord in [vehicle commandWords]){
            if([cmdWord checkWord] != 0){
                valid = YES;
                break;
            }
        }
    }
    if(!valid){
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_noProtocol"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }else{
        itmOBD = [[UINavigationItem alloc] initWithTitle:[NSString stringWithFormat:@"%@ %@ %@ %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_obd"], [vcByYMM selectedYear], [[vcByYMM selectedMake] name], [[vcByYMM selectedModel] name]]];
        UIBarButtonItem *bbiBack = [[UIBarButtonItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_back"] style:UIBarButtonItemStylePlain target:nil action:nil];
        [itmOBD setBackBarButtonItem:bbiBack];
        [barNav pushNavigationItem:itmOBD animated:YES];
        vcOBD = [[VCOBD alloc] initWithNibName:@"OBD" bundle:nil];
        [vcOBD.view setFrame:CGRectMake(self.view.frame.size.width, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
        [self.view addSubview:vcOBD.view];
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.25];
        [vcOBD.view setTransform:CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)];
        [UIView commitAnimations];
    }
}

- (void)writeCCCS{
    switch(behavior){
        case 1:{
            NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_checkingSensor"],  @"hideCancel":@0};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoadSensor" object:dic];
            [[ModuleBT sharedInstance] checkSensor:[vehicle commandWords] mhz:mhz chk:NO];
        }
            break;
        case 2:{
            NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_readingSensor"],  @"hideCancel":@0};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoadSensor" object:dic];
            [[ModuleBT sharedInstance] copySensor:[vehicle commandWords] mhz:mhz];
        }
            break;
        case 3:{
            NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_readingSensor"],  @"hideCancel":@0};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoadSensor" object:dic];
            [[ModuleBT sharedInstance] copySetSensor:[vehicle commandWords] mhz:mhz];
        }
            break;
    }
    
}

- (void)writeCreate{
    [[ModuleBT sharedInstance] createSensor:[vehicle commandWords] mhz:mhz];
}

- (void)freqDiscovered:(NSNotification *)notification{
    freqChosen = YES;
    NSDictionary *dic = [notification object];
    mhz = (uint8_t)[[dic objectForKey:@"frequency"] unsignedCharValue];
    NSString *freq = @"315";
    if(mhz == 0x02){
        freq = @"433";
    }
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_freqDiscovered"] message:[NSString stringWithFormat:@"%@ %@ %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_frequencyIs"], freq, [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_mhz"]] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
}

- (void)freqUndiscovered{
    freqChosen = NO;
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_freqNotDiscovered"] message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_freqUnable"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
}

- (IBAction)close{
    [[self.view subviews] makeObjectsPerformSelector:@selector(endEditing:)withObject:@NO];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CloseHolder" object:nil];
}

@end
