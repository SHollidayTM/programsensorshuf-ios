//
//  VCService.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/27/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "Make.h"
#import "Model.h"
#import "ModuleDB.h"
#import "VCPdfViewer.h"
#import "Relearn.h"

@interface VCService : UIViewController{
    IBOutlet UIImageView *imvVehicle;
    IBOutlet UILabel *lblRelearnInst;
    IBOutlet UITableView *tblRelearn;
    IBOutlet UIView *vButtons;
    IBOutlet UIButton *btnInstructions;
    IBOutlet UIButton *btnActivate;
    IBOutlet UILabel *lblObdComing;
    IBOutlet UIButton *btnCheck;
    IBOutlet UIButton *btnProgram;
    IBOutlet UILabel *lblIndirect;
    IBOutlet UIButton *btnRelearn;
    BOOL indirect;
    Vehicle *vehicle;
    SensorNameFreq *sensorNameFreq;
    Relearn *relearn;
}

- (void)setup:(BOOL)sentIndirect veh:(Vehicle *)sentVehicle rel:(Relearn *)sentRelearn;
- (IBAction)check;
- (IBAction)program;
- (IBAction)relearn;

@end
