//
//  VCByVIN.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/25/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCByVIN.h"

@interface VCByVIN ()

@end

@implementation VCByVIN

- (void)viewDidLoad{
    [super viewDidLoad];
    [[btnScan layer] setCornerRadius:5];
    [btnScan setClipsToBounds:YES];
    [[btnGo layer] setCornerRadius:5];
    [btnGo setClipsToBounds:YES];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    [gradient setFrame:btnGo.bounds];
    [gradient setColors:@[(id)[[UIColor colorWithRed:250.0/255.0 green:0 blue:0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:200.0/255.0 green:0 blue:0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:150.0/255.0 green:0 blue:0 alpha:1.0] CGColor]]];
    [btnGo.layer insertSublayer:gradient atIndex:0];
    [txtVIN setPlaceholder:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_inputVINNum"]];
    [lblOr setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_or"]];
    [btnScan setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_scanVIN"] forState:UIControlStateNormal];
    [btnGo setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_go"] forState:UIControlStateNormal];
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    [self go];
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

- (IBAction)scan{
    [txtVIN resignFirstResponder];
    AVCaptureDevice *captureDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error;
    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:captureDevice error:&error];
    if(!input){
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_noScanning"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }else{
        [vVidBlock setHidden:NO];
        session = [[AVCaptureSession alloc] init];
        [session addInput:input];
        AVCaptureMetadataOutput *captureMetadataOutput = [[AVCaptureMetadataOutput alloc] init];
        [session addOutput:captureMetadataOutput];
        [captureMetadataOutput setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
        [captureMetadataOutput setMetadataObjectTypes:[captureMetadataOutput availableMetadataObjectTypes]];
        videoLayer = [[AVCaptureVideoPreviewLayer alloc] initWithSession:session];
        [videoLayer setVideoGravity:AVLayerVideoGravityResizeAspectFill];
        [videoLayer setFrame:[[UIScreen mainScreen] bounds]];
        vOptions = [[UIView alloc] initWithFrame:CGRectMake(0, [UIScreen mainScreen].bounds.size.height-40, [UIScreen mainScreen].bounds.size.width, 40)];
        [vOptions setBackgroundColor:[UIColor blackColor]];
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
            stkSwitch = [[UIStackView alloc] initWithFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width/2, 40)];
            [stkSwitch setAxis:UILayoutConstraintAxisHorizontal];
            [stkSwitch setDistribution:UIStackViewDistributionFillProportionally];
            [stkSwitch setAlignment:UIStackViewAlignmentCenter];
            [stkSwitch setBackgroundColor:[UIColor blackColor]];
            UILabel *lblOff = [[UILabel alloc] init];
            [lblOff setTextAlignment:NSTextAlignmentCenter];
            [lblOff setTextColor:[UIColor whiteColor]];
            [lblOff setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_flashOff"]];
            swtFlashOnOff = [[UISwitch alloc] init];
            [swtFlashOnOff setOn:NO];
            [swtFlashOnOff addTarget:self action:@selector(switchFlash) forControlEvents:UIControlEventValueChanged];
            UILabel *lblOn = [[UILabel alloc] init];
            [lblOn setTextAlignment:NSTextAlignmentCenter];
            [lblOn setTextColor:[UIColor whiteColor]];
            [lblOn setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_flashOn"]];
            [stkSwitch addArrangedSubview:lblOff];
            [stkSwitch addArrangedSubview:swtFlashOnOff];
            [stkSwitch addArrangedSubview:lblOn];
            [vOptions addSubview:stkSwitch];
        }
        btnCancelScan = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnCancelScan setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] forState:UIControlStateNormal];
        [btnCancelScan setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [btnCancelScan addTarget:self action:@selector(cancelScan) forControlEvents:UIControlEventTouchUpInside];
        if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
            [btnCancelScan setFrame:CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 40)];
        }else{
            [btnCancelScan setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2, 0, [UIScreen mainScreen].bounds.size.width/2, 40)];
        }
        [vOptions addSubview:btnCancelScan];
        [[[[UIApplication sharedApplication] delegate] window] addSubview:vOptions];
        [[[[UIApplication sharedApplication] delegate] window].layer insertSublayer:videoLayer below:[vOptions layer]];
        [session startRunning];
    }
}

- (void)switchFlash{
    AVCaptureDevice *device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if([device hasTorch]){
        [device lockForConfiguration:nil];
        if([swtFlashOnOff isOn]){
            [device setTorchMode:AVCaptureTorchModeOn];
        }else{
            [device setTorchMode:AVCaptureTorchModeOff];
        }
        [device unlockForConfiguration];
    }
}

-(void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection{
    if(metadataObjects != nil && [metadataObjects count] > 0){
        [vVidBlock setHidden:YES];
        AVMetadataMachineReadableCodeObject *metadataObj = [metadataObjects objectAtIndex:0];
        if(![metadataObj isKindOfClass:[AVMetadataFaceObject class]]){
            [txtVIN setText:[metadataObj stringValue]];
            [self cancelScan];
            [self go];
        }
    }
}

- (void)cancelScan{
    [session stopRunning];
    [videoLayer removeFromSuperlayer];
    [vOptions removeFromSuperview];
    [vVidBlock setHidden:YES];
}

- (IBAction)go{
    if([[txtVIN text] length] > 0){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [[ModuleVINToYMM sharedInstance] returnYMM:[txtVIN text]];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_loadingResults"],  @"hideCancel":@0};
                [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:dic];
            });
        });
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_invalidVIN"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

@end
