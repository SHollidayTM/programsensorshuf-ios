//
//  VCCopy.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 12/3/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "SensorNameFreq.h"
#import "Relearn.h"

@interface VCCopy : UIViewController{
    IBOutlet UILabel *lblInst;
    IBOutlet UIButton *btnReadSensor;
    IBOutlet UILabel *lblId;
    IBOutlet UILabel *lblMhz;
    IBOutlet UILabel *lblId2;
    IBOutlet UILabel *lblMhz2;
    IBOutlet UIButton *btnCopyTo;
    IBOutlet UIButton *btnRelearnInst;
    NSMutableArray *metal;
    NSMutableArray *rubber;
    IBOutlet UIView *vChooseSensor;
    IBOutlet UILabel *lblMetal;
    IBOutlet UILabel *lblRubber;
    IBOutlet UITableView *tblMetal;
    IBOutlet UITableView *tblRubber;
    Relearn *relearn;
    SensorNameFreq *sensorNameFreq;
}

@property (assign) int state;

- (void)setup:(NSArray *)sentSensorNameFreqs rel:(Relearn *)sentRelearn;
- (IBAction)copy;
- (IBAction)createFromCopy;
- (IBAction)relearnInst;
- (IBAction)closeSensorSelect;
- (void)openSensorSelect;

@end
