//
//  VCByProduct.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/27/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ModuleLanguage.h"
#import "ModuleGetProdsByPart.h"
#import "VCDeviceResults.h"
#import "VCDeviceDetail.h"
#import "ModuleDB.h"

@interface VCByProduct : UIViewController <AVCaptureMetadataOutputObjectsDelegate, UITextFieldDelegate>{
    IBOutlet UITextField *txtProdNum;
    IBOutlet UILabel *lblWildcard;
    IBOutlet UILabel *lblOr;
    IBOutlet UIButton *btnScan;
    IBOutlet UIButton *btnGo;
    IBOutlet UIView *vVidBlock;
    UIView *vOptions;
    UIStackView *stkSwitch;
    UISwitch *swtFlashOnOff;
    UIButton *btnCancelScan;
    AVCaptureSession *session;
    AVCaptureVideoPreviewLayer *videoLayer;
}

@property (nonatomic, strong) NSString *partNum;

- (IBAction)scan;
- (IBAction)go;

@end
