//
//  VCMore.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 12/4/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCMore.h"

@interface VCMore ()

@end

@implementation VCMore

- (void)viewDidLoad{
    [super viewDidLoad];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 28)];
    [lbl setBackgroundColor:[UIColor colorWithRed:(247.0f/255.0f) green:(247.0f/255.0f) blue:(247.0f/255.0f) alpha:1]];
    switch(section){
        case 0:
            [lbl setText:[NSString stringWithFormat:@" %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_more"]]];
            break;
        case 1:
            [lbl setText:[NSString stringWithFormat:@" %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_hufCorporate"]]];
            break;
    }
    return lbl;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch(section){
        case 0:
            return 4;
        case 1:
            return 6;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
        [cell setBackgroundColor:[UIColor clearColor]];
        [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    }
    switch([indexPath section]){
        case 0:{
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(10, 2, tableView.frame.size.width-20, 40)];
            [lbl setBackgroundColor:[UIColor clearColor]];
            [lbl setFont:[UIFont boldSystemFontOfSize:16]];
            switch([indexPath row]){
                case 0:
                    [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_learningCenter"]];
                    break;
                case 1:
                    [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_history"]];
                    break;
                case 2:
                    [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_aboutHuf"]];
                    break;
                case 3:
                    [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_whereToBuy"]];
                    break;
            }
            [cell.contentView addSubview:lbl];
        }
            break;
        case 1:{
            UIView *vCell = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 40)];
            UIImageView *imvIcon = [[UIImageView alloc] initWithFrame:CGRectMake(10, 10, 20, 20)];
            [imvIcon setContentMode:UIViewContentModeScaleAspectFit];
            [vCell addSubview:imvIcon];
            UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(50, 2, tableView.frame.size.width-60, 40)];
            [lbl setBackgroundColor:[UIColor clearColor]];
            [lbl setFont:[UIFont systemFontOfSize:14]];
            switch([indexPath row]){
                case 0:
                    [imvIcon setImage:[UIImage imageNamed:@"call_us.png"]];
                    [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_callUs"]];
                    [vCell addSubview:lbl];
                    [cell.contentView addSubview:vCell];
                    break;
                case 1:
                    [imvIcon setImage:[UIImage imageNamed:@"email_us.png"]];
                    [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_emailUs"]];
                    [vCell addSubview:lbl];
                    [cell.contentView addSubview:vCell];
                    break;
                case 2:
                    [imvIcon setImage:[UIImage imageNamed:@"website.png"]];
                    [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_fullSite"]];
                    [vCell addSubview:lbl];
                    [cell.contentView addSubview:vCell];
                    break;
                case 3:
                    [imvIcon setImage:[UIImage imageNamed:@"website.png"]];
                    [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_privacyPolicy"]];
                    [vCell addSubview:lbl];
                    [cell.contentView addSubview:vCell];
                    break;
                case 4:
                    [imvIcon setImage:[UIImage imageNamed:@"feedback.png"]];
                    [lbl setText:@"Provide Feedback"];
                    [vCell addSubview:lbl];
                    [cell.contentView addSubview:vCell];
                    break;
                case 5:
                    [lbl setFrame:CGRectMake(10, 2, tableView.frame.size.width-20, 40)];
                    [lbl setFont:[UIFont systemFontOfSize:10]];
                    NSString *string = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_copyright"];
                    string = [string stringByReplacingOccurrencesOfString:@"\\u00A9" withString:@"©"];
                    string = [string stringByReplacingOccurrencesOfString:@"\\u0026" withString:@"&"];
                    [lbl setText:string];
                    [cell.contentView addSubview:lbl];
                    break;
            }
        }
            break;
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch([indexPath section]){
        case 0:
            switch([indexPath row]){
                case 0:
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenLearningCenter" object:nil];
                    break;
                case 1:
                    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenHistory" object:nil];
                    break;
                case 2:{
                    NSString *copyright = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_copyright"];
                    copyright = [copyright stringByReplacingOccurrencesOfString:@"\\u00A9" withString:@"©"];
                    copyright = [copyright stringByReplacingOccurrencesOfString:@"\\u0026" withString:@"&"];
                    NSString *string = [NSString stringWithFormat:@"%@ \n\n %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_hufDesc"], copyright];
                    string = [string stringByReplacingOccurrencesOfString:@"\\'" withString:@"'"];
                    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_aboutHuf"] message:string preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [ctrAlert addAction:actOk];
                    [self presentViewController:ctrAlert animated:YES completion:nil];
                }
                    break;
                case 3:
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.intellisens.com/distributor-locator/"]];
                    break;
            }
            break;
        case 1:
            switch([indexPath row]){
                case 0:{
                    UIAlertController *ctrAlert;
                    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
                        ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"%@?", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_callHuf"]] preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *actCall = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_call"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            [self dismissViewControllerAnimated:YES completion:nil];
                            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:414-365-8175"]];
                        }];
                        [ctrAlert addAction:actCall];
                        UIAlertAction *actCancel = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        [ctrAlert addAction:actCancel];
                    }else{
                        ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"%@.", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_callHuf"]] preferredStyle:UIAlertControllerStyleAlert];
                        UIAlertAction *actCancel = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                            [self dismissViewControllerAnimated:YES completion:nil];
                        }];
                        [ctrAlert addAction:actCancel];
                    }
                    [self presentViewController:ctrAlert animated:YES completion:nil];
                }
                    break;
                case 1:{
                    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[NSString stringWithFormat:@"%@ info_us@intellisens.com?", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_emailUs"]] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *actCancel = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        [self dismissViewControllerAnimated:YES completion:nil];
                    }];
                    [ctrAlert addAction:actCancel];
                    UIAlertAction *actEmail = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_email"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:info_us@intellisens.com"]];
                    }];
                    [ctrAlert addAction:actEmail];
                    [self presentViewController:ctrAlert animated:YES completion:nil];
                }
                    break;
                case 2:
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.intellisens.com"]];
                    break;
                case 3:
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.intellisens.com/10/privacy-policy/"]];
                    break;
                case 4:
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://goo.gl/forms/NDNtS772Tl"]];
                    break;
            }
            break;
    }
}

@end
