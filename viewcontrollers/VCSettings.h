//
//  VCSettings.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 12/4/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "ModuleLanguage.h"
#import "ModuleGetNewVehicles.h"
#import "ModuleBT.h"
#import "BTObject.h"

@interface VCSettings : UIViewController{
    IBOutlet UIScrollView *scrContent;
    IBOutlet UIView *vContent;
    IBOutlet UINavigationBar *navBar;
    IBOutlet UILabel *lblAppVersion;
    IBOutlet UISegmentedControl *segPsiBar;
    IBOutlet UISegmentedControl *segHexDec;
    IBOutlet UISegmentedControl *segFC;
    IBOutlet UISegmentedControl *segLanguage;
    IBOutlet UIButton *btnBluetooth;
    IBOutlet UIButton *btnUpdateData;
    IBOutlet UIButton *btnUpdateFirmware;
    IBOutlet UIButton *btnRegister;
    Reachability *reach;
}

- (void)setVersions;
- (IBAction)psiBarChange:(UISegmentedControl *)seg;
- (IBAction)hexDecChange:(UISegmentedControl *)seg;
- (IBAction)fcChange:(UISegmentedControl *)seg;
- (IBAction)languageChange:(UISegmentedControl *)seg;
- (IBAction)bluetooth;
- (IBAction)updateData;
- (IBAction)updateFirmware;
- (IBAction)doRegister;

@end
