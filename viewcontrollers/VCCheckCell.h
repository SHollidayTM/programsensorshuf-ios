//
//  VCCheckCell.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/30/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"

@interface VCCheckCell : UIViewController{
    IBOutlet UILabel *lblTire;
    IBOutlet UILabel *lblId;
    IBOutlet UILabel *lblPsi;
    IBOutlet UILabel *lblFreq;
    IBOutlet UILabel *lblTemp;
    IBOutlet UILabel *lblBat;
    IBOutlet UIProgressView *prg;
    int type;
}

@property (nonatomic, strong) IBOutlet UIButton *btn;
@property (assign) BOOL hasBeenRead;

- (void)setup:(int)loc typ:(int)sentType;
- (IBAction)go;
- (void)showResult:(NSData *)resultData freq:(NSString *)strFreq dupe:(BOOL)duplicate;
- (void)makeGreen;

@end
