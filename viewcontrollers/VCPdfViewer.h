//
//  VCPdfViewer.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/25/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"

@interface VCPdfViewer : UIViewController <UIPrintInteractionControllerDelegate>{
    IBOutlet UIWebView *webPdf;
    IBOutlet UIButton *btnClose;
    IBOutlet UIButton *btnPrint;
    NSString *html;
    NSString *name;
    BOOL isPdf;
}

- (void)loadHtml:(NSString *)sentHtml links:(BOOL)addLinks;
- (void)loadPdf:(NSString *)sentName;
- (IBAction)close;
- (IBAction)print;

@end
