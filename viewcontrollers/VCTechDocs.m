//
//  VCTechDocs.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 1/28/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "VCTechDocs.h"

@interface VCTechDocs ()

@end

@implementation VCTechDocs

- (void)viewDidLoad{
    [super viewDidLoad];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    switch([indexPath row]){
        case 0:
            [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_tipsTechs"]];
            break;
        case 1:
            [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_initialReport"]];
            break;
        case 2:
            [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_finalReport"]];
            break;
    }
    [cell.contentView addSubview:lbl];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *name;
    switch([indexPath row]){
        case 0:
            name = @"tips_techniques";
            break;
        case 1:
            name = @"initial_customer";
            break;
        case 2:
            name = @"final_customer";
            break;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenPdfViewer" object:name];
}

@end
