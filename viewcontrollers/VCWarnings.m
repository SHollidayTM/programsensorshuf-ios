//
//  VCWarnings.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/22/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "VCWarnings.h"

@interface VCWarnings ()

@end

@implementation VCWarnings

- (void)viewDidLoad {
    [super viewDidLoad];
    [[webWarnings layer] setCornerRadius:5];
    [webWarnings setClipsToBounds:YES];
    [btnClose setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_close"] forState:UIControlStateNormal];
}

- (void)loadWeb:(NSString *)sentHtml{
    [webWarnings loadHTMLString:sentHtml baseURL:nil];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if(navigationType == UIWebViewNavigationTypeLinkClicked){
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}

- (IBAction)close{
    [self.view removeFromSuperview];
}

@end
