//
//  VCRelearn.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 12/4/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCRelearn.h"

@interface VCRelearn ()

@end

@implementation VCRelearn

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(relearnReturned:) name:@"ReturnRelearn" object:nil];
    [[btnInstructions layer] setCornerRadius:5];
    [btnInstructions setClipsToBounds:YES];
    [[btnTroubleshooting layer] setCornerRadius:5];
    [btnTroubleshooting setClipsToBounds:YES];
    [lblInst setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_relearnInst"]];
    [btnInstructions setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_instructions"] forState:UIControlStateNormal];
    [btnTroubleshooting setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_troubleshooting"] forState:UIControlStateNormal];
}

- (void)relearnReturned:(NSNotification *)notification{
    dispatch_async(dispatch_get_main_queue(), ^{
        relearn = [notification object];
        [tbl reloadData];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
    });
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    UIView *vBackground = [[UIView alloc] initWithFrame:CGRectMake(1, 1, tableView.frame.size.width-2, 42)];
    [vBackground setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:.025]];
    [cell.contentView addSubview:vBackground];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, tableView.frame.size.width-40, 40)];
    UIImageView *imvStatus = [[UIImageView alloc] initWithFrame:CGRectMake(tableView.frame.size.width-35, 7, 30, 30)];
    switch([indexPath row]){
        case 0:
            [lbl setText:@"Add Air"];
            if([relearn addAir]){
                [imvStatus setImage:[UIImage imageNamed:@"checkmark.png"]];
            }else{
                [imvStatus setImage:[UIImage imageNamed:@"xmark.png"]];
            }
            break;
        case 1:
            [lbl setText:@"Rotate Tires"];
            if([relearn rotateTires]){
                [imvStatus setImage:[UIImage imageNamed:@"checkmark.png"]];
            }else{
                [imvStatus setImage:[UIImage imageNamed:@"xmark.png"]];
            }
            break;
        case 2:
            [lbl setText:@"Replace Sensor"];
            if([relearn replaceSensor]){
                [imvStatus setImage:[UIImage imageNamed:@"checkmark.png"]];
            }else{
                [imvStatus setImage:[UIImage imageNamed:@"xmark.png"]];
            }
            break;
    }
    [cell.contentView addSubview:lbl];
    [cell.contentView addSubview:imvStatus];
    return cell;
}

- (IBAction)instructions{
    if([relearn instructions]){
        NSDictionary *dic = @{@"html":[relearn instructions], @"links":@YES};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenPdfViewer" object:dic];
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Sorry" message:@"No relearn instructions available." preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

- (IBAction)obd{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenOBD" object:nil];
}

- (IBAction)troubleshooting{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CheckAttachments" object:nil];
}

@end
