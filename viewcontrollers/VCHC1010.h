//
//  VCHC1010.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/6/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"

@interface VCHC1010 : UIViewController{
    IBOutlet UITextView *txvAbout;
    IBOutlet UIButton *btnInteractive;
    IBOutlet UIButton *btnWhereBuy;
}

- (IBAction)whereToBuy;

@end
