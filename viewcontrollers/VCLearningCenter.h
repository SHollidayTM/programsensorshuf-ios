//
//  VCLearningCenter.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/6/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "VCTechDocs.h"
#import "VCTraining.h"
#import "VCFreqQues.h"
#import "VCHC1010.h"

@interface VCLearningCenter : UIViewController{
    IBOutlet UINavigationBar *barNav;
    UINavigationItem *itmFirst;
    UINavigationItem *itmTechDocs;
    VCTechDocs *vcTechDocs;
    UINavigationItem *itmTraining;
    VCTraining *vcTraining;
    UINavigationItem *itmFreqQues;
    VCFreqQues *vcFreqQues;
    UINavigationItem *itmHC1010;
    VCHC1010 *vcHC1010;
}

- (IBAction)close;

@end
