//
//  VCSettings.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 12/4/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCSettings.h"

@interface VCSettings ()

@end

@implementation VCSettings

- (void)viewDidLoad{
    [super viewDidLoad];
    [[btnBluetooth layer] setCornerRadius:5];
    [btnBluetooth setClipsToBounds:YES];
    [[btnUpdateData layer] setCornerRadius:5];
    [btnUpdateData setClipsToBounds:YES];
    [[btnUpdateFirmware layer] setCornerRadius:5];
    [btnUpdateFirmware setClipsToBounds:YES];
    [[btnRegister layer] setCornerRadius:5];
    [btnRegister setClipsToBounds:YES];
    [[[navBar items] objectAtIndex:0] setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_settings"]];
    [segPsiBar setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_poundsSquareInch"] forSegmentAtIndex:0];
    [segPsiBar setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_bar"] forSegmentAtIndex:1];
    [segHexDec setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_hex"] forSegmentAtIndex:0];
    [segHexDec setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_dec"] forSegmentAtIndex:1];
    [segFC setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_fahrenheit"] forSegmentAtIndex:0];
    [segFC setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_celsius"] forSegmentAtIndex:1];
    [segLanguage setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_default"] forSegmentAtIndex:0];
    [segLanguage setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_english"] forSegmentAtIndex:1];
    [segLanguage setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_spanish"] forSegmentAtIndex:2];
    [segLanguage setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_french"] forSegmentAtIndex:3];
    [btnBluetooth setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_btSettings"] forState:UIControlStateNormal];
    [btnUpdateData setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_updateData"] forState:UIControlStateNormal];
    [btnUpdateFirmware setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_updateFirmware"] forState:UIControlStateNormal];
    [btnRegister setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_register"] forState:UIControlStateNormal];
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [segPsiBar setSelectedSegmentIndex:[defaults boolForKey:@"psiBar"]];
    [segHexDec setSelectedSegmentIndex:[defaults boolForKey:@"hexDec"]];
    [segFC setSelectedSegmentIndex:[defaults boolForKey:@"fahCel"]];
    [segLanguage setSelectedSegmentIndex:[defaults integerForKey:@"language"]];
    reach = [Reachability reachabilityWithHostname:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"apiUrl"]];
}

- (void)setVersions{
    NSData *dataBTObject = [[NSUserDefaults standardUserDefaults] objectForKey:@"btObject"];
    if(dataBTObject){
        BTObject *btObject = [NSKeyedUnarchiver unarchiveObjectWithData:dataBTObject];
        [lblAppVersion setText:[NSString stringWithFormat:@"%@ %@\r\n%@ %@ %@ %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_appVersion"], [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"], [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_device"], [btObject serialNum], [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_firmwareVersion"], [[NSUserDefaults standardUserDefaults] objectForKey:@"firmwareVersion"]]];
    }else{
        [lblAppVersion setText:[NSString stringWithFormat:@"%@ %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_appVersion"], [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleShortVersionString"]]];
    }
}

- (IBAction)psiBarChange:(UISegmentedControl *)seg{
    [[NSUserDefaults standardUserDefaults] setBool:[seg selectedSegmentIndex] forKey:@"psiBar"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)hexDecChange:(UISegmentedControl *)seg{
    [[NSUserDefaults standardUserDefaults] setBool:[seg selectedSegmentIndex] forKey:@"hexDec"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)fcChange:(UISegmentedControl *)seg{
    [[NSUserDefaults standardUserDefaults] setBool:[seg selectedSegmentIndex] forKey:@"fahCel"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

- (IBAction)languageChange:(UISegmentedControl *)seg{
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_langSettings"] message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_langAreYouSure"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actNo = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_no"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [segLanguage setSelectedSegmentIndex:[[NSUserDefaults standardUserDefaults] integerForKey:@"language"]];
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actNo];
    UIAlertAction *actYes = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_yes"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
        [[NSUserDefaults standardUserDefaults] setInteger:[seg selectedSegmentIndex] forKey:@"language"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        exit(0);
    }];
    [ctrAlert addAction:actYes];
    [self presentViewController:ctrAlert animated:YES completion:nil];
}

- (IBAction)bluetooth{
    UIAlertController *ctrAlert;
    BTObject *btObject = [[ModuleBT sharedInstance] btObjectHC];
    if(btObject){
        ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_btSettings"] message:[NSString stringWithFormat:@"You are currently connected to %@. If you would like to connect to a different device, turn off %@, turn on the new device, and tap the OK button below.", [btObject name], [btObject name]] preferredStyle:UIAlertControllerStyleAlert];
    }else{
        ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_btSettings"] message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_notConnectDevice"] preferredStyle:UIAlertControllerStyleAlert];
    }
    UIAlertAction *actCancel = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actCancel];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
        NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_connectToDevice"], @"hideCancel":@1};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:dic];
        [[ModuleBT sharedInstance] searchForDevices:nil obd:NO];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
}

- (IBAction)updateData{
    NetworkStatus status = [reach currentReachabilityStatus];
    if(status == NotReachable){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowNoNetwork" object:nil];
    }else{
        [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
        NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_loadingResults"],  @"hideCancel":@0};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:dic];
        [[ModuleGetNewVehicles sharedInstance] go];
    }
}

- (IBAction)updateFirmware{
    BTObject *btObject = [[ModuleBT sharedInstance] btObjectHC];
    if(!btObject){
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_btSettings"] message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_notConnectDevice"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actCancel = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actCancel];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_connectToDevice"], @"hideCancel":@1};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:dic];
            [[ModuleBT sharedInstance] searchForDevices:nil obd:NO];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_updateFirmware"] message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_plugDevice"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actCancel = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actCancel];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"UpdateFirmware" object:nil];
            
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

- (IBAction)doRegister{
    NetworkStatus status = [reach currentReachabilityStatus];
    if(status == NotReachable){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowNoNetwork" object:nil];
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenRegister" object:nil];
    }
}

@end
