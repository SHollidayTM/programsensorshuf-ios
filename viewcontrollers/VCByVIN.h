//
//  VCByVIN.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/25/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import "ModuleLanguage.h"
#import "ModuleVINToYMM.h"
#import "ModuleDB.h"
#import "Make.h"
#import "Model.h"

@interface VCByVIN : UIViewController <AVCaptureMetadataOutputObjectsDelegate, UITextFieldDelegate>{
    IBOutlet UITextField *txtVIN;
    IBOutlet UILabel *lblOr;
    IBOutlet UIButton *btnScan;
    IBOutlet UIButton *btnGo;
    IBOutlet UIView *vVidBlock;
    UIView *vOptions;
    UIStackView *stkSwitch;
    UISwitch *swtFlashOnOff;
    UIButton *btnCancelScan;
    AVCaptureSession *session;
    AVCaptureVideoPreviewLayer *videoLayer;
}

- (IBAction)scan;
- (IBAction)go;

@end
