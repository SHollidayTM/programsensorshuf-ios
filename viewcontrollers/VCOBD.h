//
//  VCOBD.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/30/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "VCCheckCell.h"
#import "ModuleBT.h"

@interface VCOBD : UIViewController{
    IBOutlet UILabel *lblLoc;
    IBOutlet UILabel *lblId;
    IBOutlet UILabel *lblPSI;
    IBOutlet UILabel *lblMhz;
    IBOutlet UILabel *lblTemp;
    IBOutlet UILabel *lblBat;
    IBOutlet UIStackView *stkCheckCells;
    IBOutlet UIButton *btnRelearn;
    NSMutableArray *checkCells;
    int checkSensor;
    int checkCount;
    NSMutableArray *datIds;
}

- (IBAction)relearn;

@end
