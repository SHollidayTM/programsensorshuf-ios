//
//  VCCheck.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/30/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCCheck.h"

@interface VCCheck ()

@end

@implementation VCCheck

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkResults:) name:@"CheckResults" object:nil];
    datIds = [[NSMutableArray alloc] initWithObjects:[NSNull null], [NSNull null], [NSNull null], [NSNull null], [NSNull null], nil];
}

- (IBAction)check:(UIButton *)sender{
    if([datIds objectAtIndex:[sender tag]] == [NSNull null]){
        checkSensor = (int)[sender tag];
        [self setupCheck];
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_checkSensorAgain"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actCancel = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actCancel];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            checkSensor = (int)[sender tag];
            [datIds replaceObjectAtIndex:checkSensor withObject:[NSNull null]];
            [self setupCheck];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

- (void)setupCheck{
    checkCount = 0;
    [self doCheck];
}

- (void)doCheck{
    NSDictionary *dic = @{@"action":@0};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CheckSensor" object:dic];
}

- (void)checkResults:(NSNotification *)notification{
    NSDictionary *dic = [notification object];
    NSData *resultData = [dic objectForKey:@"resultData"];
    NSData *currId = [resultData subdataWithRange:NSMakeRange(4, 4)];
    BOOL duplicate = NO;
    for(NSData *prevID in datIds){
        if([currId isEqual:prevID]){
            if(checkCount < 2){
                checkCount++;
                [self doCheck];
                return;
            }
            duplicate = YES;
            break;
        }
    }
    if(!duplicate){
        [datIds replaceObjectAtIndex:checkSensor withObject:currId];
        switch(checkSensor){
            case 0:
                strTitle = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_lfCheckSuccess"];
                [btnLeftFront setSelected:YES];
                break;
            case 1:
                strTitle = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_rfCheckSuccess"];
                [btnRightFront setSelected:YES];
                break;
            case 2:
                strTitle = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_rrCheckSuccess"];
                [btnRightRear setSelected:YES];
                break;
            case 3:
                strTitle = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_lrCheckSuccess"];
                [btnLeftRear setSelected:YES];
                break;
            case 4:
                strTitle = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_spCheckSuccess"];
                [btnSpare setSelected:YES];
                break;
        }
        strMsg = [NSString stringWithFormat:@"%@: ", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_id"]];
        const uint8_t *bytes = [resultData bytes];
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"hexDec"]){
            int value = CFSwapInt32BigToHost(*(int*)([[resultData subdataWithRange:NSMakeRange(4, 4)] bytes]));
            strMsg = [strMsg stringByAppendingString:[NSString stringWithFormat:@"%d", value]];
        }else{
            strMsg = [strMsg stringByAppendingString:[[NSString stringWithFormat:@"%02x%02x%02x%02x", bytes[4], bytes[5], bytes[6], bytes[7]] uppercaseString]];
        }
        uint8_t mhz = (uint8_t)[[dic objectForKey:@"frequency"] unsignedCharValue];
        NSString *strFreq = @"315";
        if(mhz == 0x02){
            strFreq = @"433";
        }
        strMsg = [strMsg stringByAppendingString:[NSString stringWithFormat:@"\r\n%@: %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_mhz"], strFreq]];
        float fPressure;
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"psiBar"]){
            fPressure = (float)(bytes[8] * .001);
            strMsg = [strMsg stringByAppendingString:[NSString stringWithFormat:@"\r\n%@: %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_bar"], [NSString stringWithFormat:@"%.2f", fPressure]]];
        }else{
            fPressure = (float)(bytes[8] * 1.45038);
            strMsg = [strMsg stringByAppendingString:[NSString stringWithFormat:@"\r\n%@: %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_poundsSquareInch"], [NSString stringWithFormat:@"%.2f", fPressure]]];
        }
        NSString *strTemp = [NSString stringWithFormat:@"%d", (int)bytes[9]];
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"fahCel"]){
            float fTemp = (float)([strTemp intValue] - 32) * .555;
            int cTemp = (int)fTemp;
            strTemp = [NSString stringWithFormat:@"%d", cTemp];
        }
        strMsg = [strMsg stringByAppendingString:[NSString stringWithFormat:@"\r\n%@: %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_temp"], strTemp]];
        NSString *strBat = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_low"];
        if(bytes[10] > 0){
            strBat = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"];
        }else{
            strBat = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_low"];
        }
        strMsg = [strMsg stringByAppendingString:[ NSString stringWithFormat:@"\r\n%@: %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_battery"], strBat]];
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:strTitle message:strMsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:.25];
            switch(checkSensor){
                case 0:
                    [txvLeftFront setText:strMsg];
                    [txvLeftFront setAlpha:1];
                    break;
                case 1:
                    [txvRightFront setText:strMsg];
                    [txvRightFront setAlpha:1];
                    break;
                case 2:
                    [txvRightRear setText:strMsg];
                    [txvRightRear setAlpha:1];
                    break;
                case 3:
                    [txvLeftRear setText:strMsg];
                    [txvLeftRear setAlpha:1];
                    break;
                case 4:
                    [txvSpare setText:strMsg];
                    [txvSpare setAlpha:1];
                    break;
            }
            [UIView commitAnimations];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_duplicateSensor"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoadSensor" object:nil];
}

@end
