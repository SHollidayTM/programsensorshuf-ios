//
//  VCTraining.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/6/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "VCTraining.h"

@interface VCTraining ()

@end

@implementation VCTraining

- (void)viewDidLoad{
    [super viewDidLoad];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    switch([indexPath row]){
        case 0:
            [lbl setText:[NSString stringWithFormat:@"%@ 1", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_hufTrainingVideo"]]];
            break;
        case 1:
            [lbl setText:[NSString stringWithFormat:@"%@ 2", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_hufTrainingVideo"]]];
            break;
        case 2:
            [lbl setText:[NSString stringWithFormat:@"%@ 3", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_hufTrainingVideo"]]];
            break;
    }
    [cell.contentView addSubview:lbl];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch([indexPath row]){
        case 0:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.youtube.com/watch?v=kC31FvaPAD4"]];
            break;
        case 1:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.youtube.com/watch?v=KOOUb_NCya0"]];
            break;
        case 2:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"https://www.youtube.com/watch?v=FnKofX-krmc"]];
            break;
    }
}

@end
