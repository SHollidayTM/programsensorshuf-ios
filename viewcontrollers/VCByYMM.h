//
//  VCByYMM.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/24/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "ModuleDB.h"
#import "ModuleGetProdsByConfig.h"
#import "Make.h"
#import "Model.h"

@interface VCByYMM : UIViewController{
    IBOutlet UIPickerView *pck;
    IBOutlet UIButton *btnGo;
    NSArray *years;
    NSArray *makes;
    NSArray *models;
    SensorTPMS *selectedSensor;
    IBOutlet UIButton *btnRam;
}

@property (assign) BOOL inService;
@property (nonatomic, strong) NSString *selectedYear;
@property (nonatomic, strong) Make *selectedMake;
@property (nonatomic, strong) Model *selectedModel;
@property (nonatomic, strong) VehicleConfig *selectedVehicleConfig;

- (void)preload;
- (IBAction)go;
- (IBAction)goToRam;

@end
