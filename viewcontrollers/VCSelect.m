//
//  VCSelect.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/30/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCSelect.h"

@interface VCSelect ()

@end

@implementation VCSelect

- (void)viewDidLoad{
    [super viewDidLoad];
    [[btnGo layer] setCornerRadius:5];
    [btnGo setClipsToBounds:YES];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    [gradient setFrame:btnGo.bounds];
    [gradient setColors:@[(id)[[UIColor colorWithRed:250.0/255.0 green:0 blue:0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:200.0/255.0 green:0 blue:0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:150.0/255.0 green:0 blue:0 alpha:1.0] CGColor]]];
    [btnGo.layer insertSublayer:gradient atIndex:0];
    [btnGo setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_go"] forState:UIControlStateNormal];
}

- (void)setup:(NSArray *)sentSensorNameFreqs{
    sensorNameFreqs = sentSensorNameFreqs;
    [pck reloadAllComponents];
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    return [sensorNameFreqs count];
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    SensorNameFreq *sensorNameFreq = [sensorNameFreqs objectAtIndex:row];
    UILabel *lbl = [[UILabel alloc] init];
    [lbl setFont:[UIFont systemFontOfSize:14]];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    [lbl setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:.5]];
    [lbl setText:[sensorNameFreq name]];
    return lbl;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    chosenSensorNameFreq = [sensorNameFreqs objectAtIndex:row];
}

- (IBAction)go{
    if(!chosenSensorNameFreq){
        chosenSensorNameFreq = [sensorNameFreqs objectAtIndex:0];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SensorNameFreqSelected" object:chosenSensorNameFreq];
}

@end
