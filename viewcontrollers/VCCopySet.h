//
//  VCCopySet.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 12/3/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "Relearn.h"
#import "SensorNameFreq.h"

@interface VCCopySet : UIViewController{
    IBOutlet UIButton *btnLeftFront;
    IBOutlet UIButton *btnRightFront;
    IBOutlet UIButton *btnRightRear;
    IBOutlet UIButton *btnLeftRear;
    IBOutlet UIButton *btnSpare;
    IBOutlet UIButton *btnLeftFrontCopy;
    IBOutlet UIButton *btnRightFrontCopy;
    IBOutlet UIButton *btnRightRearCopy;
    IBOutlet UIButton *btnLeftRearCopy;
    IBOutlet UIButton *btnSpareCopy;
    IBOutlet UITextView *txvLeftFront;
    IBOutlet UITextView *txvRightFront;
    IBOutlet UITextView *txvRightRear;
    IBOutlet UITextView *txvLeftRear;
    IBOutlet UITextView *txvSpare;
    NSMutableArray *metal;
    NSMutableArray *rubber;
    IBOutlet UIView *vChooseSensor;
    IBOutlet UILabel *lblMetal;
    IBOutlet UILabel *lblRubber;
    IBOutlet UITableView *tblMetal;
    IBOutlet UITableView *tblRubber;
    Relearn *relearn;
    SensorNameFreq *sensorNameFreq;
    int copySensor;
    int checkCount;
    NSMutableArray *datIds;
    NSString *strTitle;
    NSString *strMsg;
}

- (void)setup:(NSArray *)sentSensorNameFreqs rel:(Relearn *)sentRelearn;
- (IBAction)copy:(UIButton *)sender;
- (IBAction)createFromButton:(UIButton *)sender;

@end
