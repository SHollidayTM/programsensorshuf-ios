//
//  VCDeviceResults.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/24/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCDeviceResults.h"

@interface VCDeviceResults ()

@end

@implementation VCDeviceResults

- (void)viewDidLoad{
    [super viewDidLoad];
    [tbl registerNib:[UINib nibWithNibName:@"DeviceCell" bundle:nil] forCellReuseIdentifier:@"cell"];
    [segSort setTitle:[NSString stringWithFormat:@"315 %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_mhz"]] forSegmentAtIndex:0];
    [segSort setTitle:[NSString stringWithFormat:@"433 %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_mhz"]] forSegmentAtIndex:1];
}

- (void)setup:(NSArray *)sentProgrammables non:(NSArray *)sentNonprogrammables kit:(NSArray *)sentServiceKits typ:(int)sentType{
    heldProgrammables = sentProgrammables;
    programmables = [heldProgrammables mutableCopy];
    heldNonprogrammables = sentNonprogrammables;
    nonprogrammables = [heldNonprogrammables mutableCopy];
    frequency = @"";
    BOOL multiple = NO;
    for(int i = 0; i < [programmables count]; i++){
        SensorTPMS *sensor = [programmables objectAtIndex:i];
        if([frequency length] < 1){
            frequency = [sensor frequency];
        }else if(![frequency isEqualToString:[sensor frequency]]){
            multiple = YES;
        }
    }
    if(!multiple){
        for(int i = 0; i < [nonprogrammables count]; i++){
            SensorTPMS *sensor = [nonprogrammables objectAtIndex:i];
            if([frequency length] < 1){
                frequency = [sensor frequency];
            }else if(![frequency isEqualToString:[sensor frequency]]){
                multiple = YES;
            }
        }
    }
    serviceKits = sentServiceKits;
    type = sentType;
    if(!multiple){
        [segSort setHidden:YES];
        [tblTopConstraint setConstant:0];
        frequency = @"";
    }else{
        frequency = @"315";
        [self stripFrequencies];
    }
    [tbl reloadData];
}

- (void)stripFrequencies{
    NSMutableArray *discardedSensors = [[NSMutableArray alloc] init];
    for(SensorTPMS *sensor in programmables){
        if(![[sensor frequency] isEqualToString:frequency]){
            [discardedSensors addObject:sensor];
        }
    }
    [programmables removeObjectsInArray:discardedSensors];
    discardedSensors = [[NSMutableArray alloc] init];
    for(SensorTPMS *sensor in nonprogrammables){
        if(![[sensor frequency] isEqualToString:frequency]){
            [discardedSensors addObject:sensor];
        }
    }
    [nonprogrammables removeObjectsInArray:discardedSensors];
}

- (IBAction)filterFreq:(UISegmentedControl *)seg{
    programmables = [heldProgrammables mutableCopy];
    nonprogrammables = [heldNonprogrammables mutableCopy];
    switch([seg selectedSegmentIndex]){
        case 0:
            frequency = @"315";
            break;
        case 1:
            frequency = @"433";
            break;
    }
    [self stripFrequencies];
    [tbl reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 28)];
    [lbl setBackgroundColor:[UIColor blackColor]];
    [lbl setTextColor:[UIColor whiteColor]];
    switch(section){
        case 0:
            [lbl setText:[NSString stringWithFormat:@" %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_intellisensConfigurable"]]];
            break;
        case 1:
            [lbl setText:[NSString stringWithFormat:@" Huf: %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_directFit"]]];
            break;
        case 2:
            [lbl setText:[NSString stringWithFormat:@" Huf: %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_serviceKits"]]];
            break;
    }
    return lbl;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    switch(section){
        case 0:
            return [programmables count];
        case 1:
            return [nonprogrammables count];
        case 2:
            return [serviceKits count];
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    TCDevice *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cell == nil){
        cell = [[TCDevice alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 60)];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    switch([indexPath section]){
        case 0:{
            SensorTPMS *sensor = [programmables objectAtIndex:indexPath.row];
            [cell.imvDevice setImage:[self getImage:[sensor partNumber]]];
            [cell.lblName setText:[sensor partNumber]];
            [cell.lblDesc setText:[NSString stringWithFormat:@"%@ %@", [sensor frequency], [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_mhz"]]];
        }
            break;
        case 1:{
            SensorTPMS *sensor = [nonprogrammables objectAtIndex:indexPath.row];
            [cell.imvDevice setImage:[self getImage:[sensor partNumber]]];
            [cell.lblName setText:[sensor partNumber]];
            [cell.lblDesc setText:[NSString stringWithFormat:@"%@ %@", [sensor frequency], [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_mhz"]]];
        }
            break;
        case 2:{
            ServiceKit *serviceKit = [serviceKits objectAtIndex:indexPath.row];
            [cell.imvDevice setImage:[self getImage:[serviceKit partNumber]]];
            [cell.lblName setText:[serviceKit partNumber]];
            if([[serviceKit desc] containsString:@"Gen 1"]){
                [cell.lblDesc setText:[NSString stringWithFormat:@"%@ 1", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_gen"]]];
            }else{
                [cell.lblDesc setText:[NSString stringWithFormat:@"%@ 2", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_gen"]]];
            }
        }
            break;
    }
    return cell;
}

- (UIImage *)getImage:(NSString *)partName{
    NSString *strDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg", partName]];
    if(!img){
        NSString *strPath = [strDir stringByAppendingPathComponent:[partName stringByAppendingString:@".jpg"]];
        img = [UIImage imageWithContentsOfFile:strPath];
        if(!img){
            img = [UIImage imageNamed:@"noimage.jpg"];
        }
    }
    return img;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSDictionary *dic;
    switch([indexPath section]){
        case 0:{
            SensorTPMS *sensor = [programmables objectAtIndex:indexPath.row];
            dic = @{@"type":[NSNumber numberWithLong:indexPath.section], @"sensor":sensor};
        }
            break;
        case 1:{
            SensorTPMS *sensor = [nonprogrammables objectAtIndex:indexPath.row];
            dic = @{@"type":[NSNumber numberWithLong:indexPath.section], @"sensor":sensor};
        }
            break;
        case 2:{
            ServiceKit *serviceKit = [serviceKits objectAtIndex:indexPath.row];
            dic = @{@"type":[NSNumber numberWithLong:indexPath.section], @"serviceKit":serviceKit};
        }
            break;
    }
    switch(type){
        case 0:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenYMMDetail" object:dic];
            break;
        case 1:
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenVINDetail" object:dic];
            break;
    }
}

@end
