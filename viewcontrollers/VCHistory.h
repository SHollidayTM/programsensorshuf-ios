//
//  VCHistory.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 5/3/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "ModuleDB.h"
#import "History.h"

@interface VCHistory : UIViewController{
    IBOutlet UISegmentedControl *segSort;
    IBOutlet UITableView *tbl;
    IBOutlet UIButton *btnClose;
    NSArray *checkHistories;
    NSArray *copyHistories;
    NSArray *createHistories;
    NSArray *shownHistories;
}

- (IBAction)sortHistories:(UISegmentedControl *)seg;
- (IBAction)close;

@end
