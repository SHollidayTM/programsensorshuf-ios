//
//  VCService.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/27/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCService.h"

@interface VCService ()

@end

@implementation VCService

- (void)viewDidLoad{
    [super viewDidLoad];
    [lblRelearnInst setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_relearnInst"]];
    [btnCheck setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_check"] forState:UIControlStateNormal];
    [btnProgram setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_prog"] forState:UIControlStateNormal];
    [lblIndirect setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_hasIndirect"]];
    [btnRelearn setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_relearns"] forState:UIControlStateNormal];
    [[btnInstructions layer] setCornerRadius:5];
    [btnInstructions setClipsToBounds:YES];
    [btnInstructions setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_instructions"] forState:UIControlStateNormal];
    [[btnActivate layer] setCornerRadius:5];
    [btnActivate setClipsToBounds:YES];
    [btnActivate setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_activateSensors"] forState:UIControlStateNormal];
}

- (void)setup:(BOOL)sentIndirect veh:(Vehicle *)sentVehicle rel:(Relearn *)sentRelearn{
    indirect = sentIndirect;
    vehicle = sentVehicle;
    NSData *datImg = [NSData dataWithContentsOfURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://d2ef5twh46wdyk.cloudfront.net/us/chromeveh/%@/_%@_%@.jpg", [vehicle year], [vehicle makeID], [vehicle modelID]]]];
    UIImage *img = [UIImage imageWithData:datImg];
    if(img){
        [imvVehicle setImage:img];
    }
    if(sentRelearn){
        relearn = sentRelearn;
        [tblRelearn reloadData];
    }else{
        [lblRelearnInst setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_noNetRelearn"]];
        [tblRelearn setHidden:YES];
        [btnInstructions setHidden:YES];
    }
    if(indirect){
        [vButtons setHidden:YES];
    }else{
        [vButtons setHidden:NO];
    }
    NSArray *attachments = [[ModuleDB sharedInstance] getAttachmentsForVehicle:vehicle];
    if([attachments count] > 0){
        NSString *message = @"";
        for(Attachment *attachment in attachments){
            if([[attachment level] isEqualToString:@"Critical Alert"]){
                NSData *decodedData = [[NSData alloc] initWithBase64EncodedString:[attachment text] options:0];
                NSString *text = [[NSString alloc] initWithData:decodedData encoding:NSUTF8StringEncoding];
                message = [message stringByAppendingString:[NSString stringWithFormat:@"%@</br>%@</br></br>", [attachment name], text]];
            }
        }
        if([message length] > 0){
            NSDictionary *dic = @{@"html":message};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenWarnings" object:dic];
        }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 3;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    UIView *vBackground = [[UIView alloc] initWithFrame:CGRectMake(1, 1, tableView.frame.size.width-2, 42)];
    [vBackground setBackgroundColor:[UIColor colorWithRed:0 green:0 blue:0 alpha:.025]];
    [cell.contentView addSubview:vBackground];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, tableView.frame.size.width-70, 40)];
    UILabel *lblStatus = [[UILabel alloc] initWithFrame:CGRectMake(tableView.frame.size.width-65, 2, 60, 40)];
    switch([indexPath row]){
        case 0:
            [lbl setText:@"Add Air"];
            [lblStatus setText:[relearn addAir]];
            break;
        case 1:
            [lbl setText:@"Rotate Tires"];
            [lblStatus setText:[relearn rotateTires]];
            break;
        case 2:
            [lbl setText:@"Replace Sensor"];
            [lblStatus setText:[relearn replaceSensor]];
            break;
    }
    [cell.contentView addSubview:lbl];
    [cell.contentView addSubview:lblStatus];
    return cell;
}

- (IBAction)check{
    if(!indirect){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenCheck" object:nil];
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_hasIndirect"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

- (IBAction)program{
    if(!indirect){
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenProgram" object:sensorNameFreq];
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_hasIndirect"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

- (IBAction)relearn{
    if(relearn){
        if([relearn instructions]){
            NSDictionary *dic = @{@"html":[relearn instructions], @"links":@YES};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenPdfViewer" object:dic];
        }else{
            [self noRelearn];
        }
    }else{
        [self noRelearn];
    }
}

- (void)noRelearn{
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_noRelearnInst"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
}

@end
