//
//  VCProgram.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/30/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCProgram.h"

@interface VCProgram ()

@end

@implementation VCProgram

- (void)viewDidLoad{
    [super viewDidLoad];
    [btnCreate setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_createSensor"] forState:UIControlStateNormal];
    [btnCopy setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_copySensor"] forState:UIControlStateNormal];
    [btnCopySet setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_copySensorSet"] forState:UIControlStateNormal];
}

- (IBAction)create{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenCreate" object:nil];
}

- (IBAction)copy{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenCopy" object:nil];
}

- (IBAction)copySet{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenCopySet" object:nil];
}

@end
