//
//  VCMain.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/23/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "ModuleLanguage.h"
#import "ModuleDB.h"
#import "VCRegister.h"
#import "VCHolder.h"
#import "VCPdfViewer.h"
#import "VCWarnings.h"
#import "VCSettings.h"
#import "VCMore.h"
#import "VCLearningCenter.h"
#import "VCHistory.h"
#import "ModuleBT.h"
#import "ModuleGetLatestUpdate.h"
#import "ModuleSendActions.h"

@interface VCMain : UIViewController{
    IBOutlet UIWebView *webAds;
    IBOutlet UIView *vWebLoading;
    IBOutlet UIButton *btnOpenProd;
    IBOutlet UIButton *btnOpenTPMS;
    IBOutlet UIView *vUnderButtons;
    IBOutlet UIButton *btnByYMM;
    IBOutlet UIButton *btnByVIN;
    IBOutlet UIButton *btnByProd;
    IBOutlet UIImageView *imvByProd;
    IBOutlet UITabBar *barMain;
    IBOutlet UIView *vButtons;
    IBOutlet UIView *vLoading;
    IBOutlet UILabel *lblLoading;
    IBOutlet UIButton *btnCancel;
    IBOutlet UIView *vLoadSensor;
    IBOutlet UILabel *lblLoadSensor;
    IBOutlet UIProgressView *prgSensor;
    IBOutlet UIButton *btnCancelSensor;
    Reachability *reach;
    int currTab;
    int prevTab;
    BOOL buttonsMoved;
    BOOL stopFetching;
    VCRegister *vcRegister;
    VCHolder *vcHolder;
    VCPdfViewer *vcPdfViewer;
    VCPdfViewer *vcTroubleshooting;
    VCWarnings *vcWarnings;
    VCSettings *vcSettings;
    VCMore *vcMore;
    VCLearningCenter *vcLearningCenter;
    VCHistory *vcHistory;
    IBOutlet UIImageView *imvBluetooth;
    BOOL fromDetail;
    BOOL holderIsOpenAndInService;
}

- (IBAction)clickBanner;
- (IBAction)openProdSearch;
- (IBAction)openTPMSService;
- (IBAction)byYMM;
- (IBAction)byVIN;
- (IBAction)cancelLoading;
- (IBAction)cancelLoadSensor;

@end
