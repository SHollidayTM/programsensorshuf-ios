//
//  VCHistory.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 5/3/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "VCHistory.h"

@interface VCHistory ()

@end

@implementation VCHistory

- (void)viewDidLoad{
    [super viewDidLoad];
    [[btnClose layer] setCornerRadius:5];
    [btnClose setClipsToBounds:YES];
    [segSort setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_checkedSensors"] forSegmentAtIndex:0];
    [segSort setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_createdSensors"] forSegmentAtIndex:1];
    [segSort setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_copiedSensors"] forSegmentAtIndex:2];
    [btnClose setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_close"] forState:UIControlStateNormal];
    [self loadHistories];
}

- (void)loadHistories{
    checkHistories = [[ModuleDB sharedInstance] getHistory:@0];
    createHistories = [[ModuleDB sharedInstance] getHistory:@1];
    copyHistories = [[ModuleDB sharedInstance] getHistory:@2];
    [self sortHistories:segSort];
    
}

- (IBAction)sortHistories:(UISegmentedControl *)seg{
    switch([seg selectedSegmentIndex]){
        case 0:
            shownHistories = checkHistories;
            break;
        case 1:
            shownHistories = createHistories;
            break;
        case 2:
            shownHistories = copyHistories;
            break;
            
    }
    shownHistories = [shownHistories sortedArrayUsingComparator:^NSComparisonResult(id a, id b){
        NSDate *first = [(History *)a date];
        NSDate *second = [(History *)b date];
        return [second compare:first];
    }];
    [tbl reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return [shownHistories count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    History *history = [shownHistories objectAtIndex:indexPath.row];
    UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 30)];
    UIStackView *stkLabels = [[UIStackView alloc] initWithFrame:CGRectMake(2, 2, tableView.frame.size.width-4, 26)];
    [stkLabels setAxis:UILayoutConstraintAxisHorizontal];
    [stkLabels setDistribution:UIStackViewDistributionFillEqually];
    [stkLabels setBackgroundColor:[UIColor lightGrayColor]];
    UILabel *lblDate = [[UILabel alloc] init];
    [lblDate setFont:[UIFont systemFontOfSize:14]];
    NSDateFormatter *dFormat = [[NSDateFormatter alloc] init];
    [dFormat setDateFormat:@"MM/dd/yyyy"];
    NSString *date = [dFormat stringFromDate:[history date]];
    [lblDate setText:[NSString stringWithFormat:@"%@", date]];
    UILabel *lblYear = [[UILabel alloc] init];
    [lblYear setFont:[UIFont systemFontOfSize:14]];
    [lblYear setTextAlignment:NSTextAlignmentCenter];
    [lblYear setText:[NSString stringWithFormat:@"%@", [history year]]];
    UILabel *lblMake = [[UILabel alloc] init];
    [lblMake setFont:[UIFont systemFontOfSize:14]];
    [lblMake setTextAlignment:NSTextAlignmentCenter];
    [lblMake setText:[history make]];
    UILabel *lblModel = [[UILabel alloc] init];
    [lblModel setFont:[UIFont systemFontOfSize:14]];
    [lblModel setTextAlignment:NSTextAlignmentRight];
    [lblModel setText:[history model]];
    [stkLabels addArrangedSubview:lblDate];
    [stkLabels addArrangedSubview:lblYear];
    [stkLabels addArrangedSubview:lblMake];
    [stkLabels addArrangedSubview:lblModel];
    [cell.contentView addSubview:stkLabels];
    return cell;
}

- (IBAction)close{
    [self.view removeFromSuperview];
}

@end
