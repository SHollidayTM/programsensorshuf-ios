//
//  VCByYMM.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/24/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCByYMM.h"

@interface VCByYMM ()

@end

@implementation VCByYMM

- (void)viewDidLoad{
    [super viewDidLoad];
    [[btnGo layer] setCornerRadius:5];
    [btnGo setClipsToBounds:YES];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    [gradient setFrame:btnGo.bounds];
    [gradient setColors:@[(id)[[UIColor colorWithRed:250.0/255.0 green:0 blue:0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:200.0/255.0 green:0 blue:0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:150.0/255.0 green:0 blue:0 alpha:1.0] CGColor]]];
    [btnGo.layer insertSublayer:gradient atIndex:0];
    [btnGo setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_go"] forState:UIControlStateNormal];
}

- (void)viewDidAppear:(BOOL)animated{
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    _selectedYear = [defaults objectForKey:@"selectedYear"];
    NSData *dataMake = [defaults objectForKey:@"selectedMake"];
    _selectedMake = [NSKeyedUnarchiver unarchiveObjectWithData:dataMake];
    NSData *dataModel = [defaults objectForKey:@"selectedModel"];
    _selectedModel = [NSKeyedUnarchiver unarchiveObjectWithData:dataModel];
    if(_selectedYear && _selectedMake && _selectedModel){
        [self preload];
    }
}

- (void)preload{
    [pck reloadComponent:0];
    for(int i = 0; i < [years count]; i++){
        NSString *year = [NSString stringWithFormat:@"%d", [[years objectAtIndex:i] intValue]];
        if([_selectedYear isEqualToString:year]){
            [pck selectRow:i inComponent:0 animated:YES];
            break;
        }
    }
    [pck reloadComponent:1];
    for(int i = 0; i < [makes count]; i++){
        Make *make = [makes objectAtIndex:i];
        if([[_selectedMake name] isEqualToString:[make name]]){
            [pck selectRow:i inComponent:1 animated:YES];
            break;
        }
    }
    [pck reloadComponent:2];
    for(int i = 0; i < [models count]; i++){
        Model *model = [models objectAtIndex:i];
        if([[_selectedModel name] isEqualToString:[model name]]){
            [pck selectRow:i inComponent:2 animated:YES];
            break;
        }
    }
}

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView{
    return 3;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component{
    int count = 0;
    switch(component){
        case 0:
            years = [[ModuleDB sharedInstance] getAllYears];
            count = (int)[years count];
            break;
        case 1:
            if(![_selectedYear isEqualToString:@"0"]){
                makes = [[ModuleDB sharedInstance] getMakesByYear:_selectedYear];
                count = (int)[makes count];
            }
            break;
        case 2:
            if(_selectedMake){
                models = [[ModuleDB sharedInstance] getModelsByYearAndMake:_selectedYear mke:_selectedMake];
                count = (int)[models count];
            }
            break;
    }
    return count;
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component{
    return 50;
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view{
    NSString *title;
    switch(component){
        case 0:
            if([[years objectAtIndex:row] isEqualToString:@"0"]){
                title = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_selectYear"];
            }else{
                title = [years objectAtIndex:row];
            }
            break;
        case 1:{
            Make *make = [makes objectAtIndex:row];
            title = [make name];
        }
            break;
        case 2:{
            Model *model = [models objectAtIndex:row];
            title = [model name];
        }
            break;
    }
    UILabel *lbl = [[UILabel alloc] init];
    [lbl setFont:[UIFont systemFontOfSize:14]];
    [lbl setNumberOfLines:2];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    [lbl setBackgroundColor:[UIColor colorWithRed:1 green:1 blue:1 alpha:.5]];
    [lbl setText:title];
    [[[pickerView subviews] objectAtIndex:1] setBackgroundColor:[UIColor colorWithRed:217.0/255.0 green:0 blue:4.0/255.0 alpha:1.0]];
    [[[pickerView subviews] objectAtIndex:2] setBackgroundColor:[UIColor colorWithRed:217.0/255.0 green:0 blue:4.0/255.0 alpha:1.0]];
    return lbl;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component{
    switch(component){
        case 0:
            _selectedYear = [years objectAtIndex:row];
            makes = [[ModuleDB sharedInstance] getMakesByYear:_selectedYear];
            if([pickerView selectedRowInComponent:1] < [makes count]){
                _selectedMake = [makes objectAtIndex:[pickerView selectedRowInComponent:1]];
            }else{
                _selectedMake = [makes lastObject];
            }
            models = [[ModuleDB sharedInstance] getModelsByYearAndMake:_selectedYear mke:_selectedMake];
            if([pickerView selectedRowInComponent:2] < [models count]){
                _selectedModel = [models objectAtIndex:[pickerView selectedRowInComponent:2]];
            }else{
                _selectedModel = [models lastObject];
            }
            [pck reloadAllComponents];
            break;
        case 1:
            if([makes count] > 0){
                _selectedMake = [makes objectAtIndex:row];
                models = [[ModuleDB sharedInstance] getModelsByYearAndMake:_selectedYear mke:_selectedMake];
                if([pickerView selectedRowInComponent:2] < [models count]){
                    _selectedModel = [models objectAtIndex:[pickerView selectedRowInComponent:2]];
                }else{
                    _selectedModel = [models lastObject];
                }
                [pck reloadAllComponents];
            }
            break;
        case 2:
            if([models count] > 0){
                _selectedModel = [models objectAtIndex:row];
            }
            break;
    }
    if(([_selectedYear intValue] >= 2012) && [_selectedMake identifier] == 40){
        [btnRam setHidden:NO];
    }else{
        [btnRam setHidden:YES];
    }
}

- (IBAction)go{
    if(_selectedYear && _selectedMake && _selectedModel){
        [[NSUserDefaults standardUserDefaults] setObject:_selectedYear forKey:@"selectedYear"];
        NSData *dataMake = [NSKeyedArchiver archivedDataWithRootObject:_selectedMake];
        [[NSUserDefaults standardUserDefaults] setObject:dataMake forKey:@"selectedMake"];
        NSData *dataModel = [NSKeyedArchiver archivedDataWithRootObject:_selectedModel];
        [[NSUserDefaults standardUserDefaults] setObject:dataModel forKey:@"selectedModel"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSArray *vehicleConfigs = [[ModuleDB sharedInstance] getQualifiersByYearMakeAndModel:_selectedYear mke:_selectedMake mod:_selectedModel];
        if([vehicleConfigs count] > 1){
            [self displayQualifiers:vehicleConfigs];
        }else{
            _selectedVehicleConfig = [vehicleConfigs objectAtIndex:0];
            [self completeGo];
        }
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_invalidYMM"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

- (void)displayQualifiers:(NSArray *)vehicleConfigs{
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:@"Qualifier" message:nil preferredStyle:UIAlertControllerStyleAlert];
    for(VehicleConfig *vehicleConfig in vehicleConfigs){
        NSString *msg = [NSString stringWithFormat:@"%@", [vehicleConfig qualifier]];
        UIAlertAction *act = [UIAlertAction actionWithTitle:msg style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            _selectedVehicleConfig = vehicleConfig;
            [self completeGo];
        }];
        [ctrAlert addAction:act];
    }
    [self presentViewController:ctrAlert animated:YES completion:nil];
}

- (void)completeGo{
    if(!_inService){
        if([_selectedVehicleConfig indirect]){
            UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_indirectSystem"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [ctrAlert addAction:actOk];
            [self presentViewController:ctrAlert animated:YES completion:nil];
        }else{
            NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_loadingResults"],  @"hideCancel":@0};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:dic];
            [[ModuleGetProdsByConfig sharedInstance] go:[_selectedVehicleConfig configID]];
        }
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenService" object:nil];
    }
}

- (IBAction)goToRam{
    for(int i = 0; i < [makes count]; i++){
        Make *make = [makes objectAtIndex:i];
        if([make identifier] == 1168){
            _selectedMake = make;
            [pck selectRow:i inComponent:1 animated:NO];
            [pck reloadComponent:2];
            [btnRam setHidden:YES];
            break;
        }
    }
}

@end
