//
//  VCProgram.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/30/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "ModuleDB.h"
#import "Make.h"
#import "Model.h"

@interface VCProgram : UIViewController{
    IBOutlet UIImageView *imvSensor;
    IBOutlet UIButton *btnCreate;
    IBOutlet UIButton *btnCopy;
    IBOutlet UIButton *btnCopySet;
}

- (IBAction)create;
- (IBAction)copy;
- (IBAction)copySet;

@end
