//
//  VCRegister.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/24/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCRegister.h"

@interface VCRegister ()

@end

@implementation VCRegister

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(registrationReturned:) name:@"ReturnRegistration" object:nil];
    [[btnGo layer] setCornerRadius:5];
    [btnGo setClipsToBounds:YES];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    [gradient setFrame:btnGo.bounds];
    [gradient setColors:@[(id)[[UIColor colorWithRed:250.0/255.0 green:0 blue:0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:200.0/255.0 green:0 blue:0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:150.0/255.0 green:0 blue:0 alpha:1.0] CGColor]]];
    [btnGo.layer insertSublayer:gradient atIndex:0];
    [txtName becomeFirstResponder];
    [txtName setPlaceholder:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_firstLastName"]];
    [txtCompanyName setPlaceholder:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_companyName"]];
    [txtAddressOne setPlaceholder:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_companyAddress"]];
    [txtAddressTwo setPlaceholder:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_companyAddressTwo"]];
    [txtCity setPlaceholder:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_companyCity"]];
    [txtState setPlaceholder:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_companyState"]];
    [txtZip setPlaceholder:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_companyZip"]];
    [txtPhone setPlaceholder:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_companyPhone"]];
    [txtEmail setPlaceholder:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_companyEmail"]];
    [btnGo setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_go"] forState:UIControlStateNormal];
    if([[NSUserDefaults standardUserDefaults] integerForKey:@"registered"] == 1){
        [txtName setText:[[NSUserDefaults standardUserDefaults] stringForKey:@"name"]];
        [txtCompanyName setText:[[NSUserDefaults standardUserDefaults] stringForKey:@"companyName"]];
        [txtAddressOne setText:[[NSUserDefaults standardUserDefaults] stringForKey:@"addressOne"]];
        [txtAddressTwo setText:[[NSUserDefaults standardUserDefaults] stringForKey:@"addressTwo"]];
        [txtCity setText:[[NSUserDefaults standardUserDefaults] stringForKey:@"city"]];
        [txtState setText:[[NSUserDefaults standardUserDefaults] stringForKey:@"state"]];
        [txtZip setText:[[NSUserDefaults standardUserDefaults] stringForKey:@"postal"]];
        [txtEmail setText:[[NSUserDefaults standardUserDefaults] stringForKey:@"email"]];
        [txtPhone setText:[[NSUserDefaults standardUserDefaults] stringForKey:@"phone"]];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self.view endEditing:YES];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField{
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string{
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    if([textField isEqual:txtName]){
        [txtCompanyName becomeFirstResponder];
    }else if([textField isEqual:txtCompanyName]){
        [txtAddressOne becomeFirstResponder];
    }else if([textField isEqual:txtAddressOne]){
        [txtAddressTwo becomeFirstResponder];
    }else if([textField isEqual:txtAddressTwo]){
        [txtCity becomeFirstResponder];
    }else if([textField isEqual:txtCity]){
        [txtState becomeFirstResponder];
    }else if([textField isEqual:txtState]){
        [txtZip becomeFirstResponder];
    }else if([textField isEqual:txtZip]){
        [txtPhone becomeFirstResponder];
    }else if([textField isEqual:txtPhone]){
        [txtEmail becomeFirstResponder];
    }else if([textField isEqual:txtEmail]){
        [textField resignFirstResponder];
    }
    return NO;
}

- (void)textFieldDidEndEditing:(UITextField *)textField{
    [textField resignFirstResponder];
}

- (IBAction)attemptRegistration{
    [self.view endEditing:YES];
    NSString *errMsg = @"";
    if(!([[txtName text] length] > 0)){
        errMsg = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_firstLastName"];
    }else if(!([[txtCompanyName text] length] > 0)){
        errMsg = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_companyName"];
    }else if(!([[txtAddressOne text] length] > 0)){
        errMsg = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_companyAddressOne"];
    }else if(!([[txtCity text] length] > 0)){
        errMsg = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_companyCity"];
    }else if(!([[txtState text] length] > 0)){
        errMsg = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_companyState"];
    }else if(!([[txtZip text] length] > 0)){
        errMsg = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_companyZip"];
    }else if(!([[txtPhone text] length] > 0)){
        errMsg = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_companyPhone"];
    }else if(!([[txtEmail text] length] > 0)){
        errMsg = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_companyEmail"];
    }
    if([errMsg isEqualToString:@""]){
        NSString *emailRegEx = @".+@.+\\.[A-Za-z]{2,4}";
        NSPredicate *testEmail = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
        if(![testEmail evaluateWithObject:[txtEmail text]]){
            UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_invalidEmail"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actOk = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [ctrAlert addAction:actOk];
            [self presentViewController:ctrAlert animated:YES completion:nil];
            return;
        }
    }
    if([errMsg length] > 0){
        errMsg = [NSString stringWithFormat:@"%@ %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_pleaseFillOut"], errMsg];
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:errMsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }else{
        [[NSUserDefaults standardUserDefaults] setObject:[txtName text] forKey:@"name"];
        [[NSUserDefaults standardUserDefaults] setObject:[txtCompanyName text] forKey:@"companyName"];
        [[NSUserDefaults standardUserDefaults] setObject:[txtAddressOne text] forKey:@"addressOne"];
        [[NSUserDefaults standardUserDefaults] setObject:[txtAddressTwo text] forKey:@"addressTwo"];
        [[NSUserDefaults standardUserDefaults] setObject:[txtCity text] forKey:@"city"];
        [[NSUserDefaults standardUserDefaults] setObject:[txtState text] forKey:@"state"];
        [[NSUserDefaults standardUserDefaults] setObject:[txtZip text] forKey:@"postal"];
        [[NSUserDefaults standardUserDefaults] setObject:[txtEmail text] forKey:@"email"];
        [[NSUserDefaults standardUserDefaults] setObject:[txtPhone text] forKey:@"phone"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_loadingResults"],  @"hideCancel":@1};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:dic];
        [[ModuleRegister sharedInstance] go];
    }
}

- (void)registrationReturned:(NSNotification *)notification{
    NSDictionary *dic = [notification object];
    NSString *msg;
    if([dic objectForKey:@"Successful"]){
        [[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"registered"];
        msg = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_regSuccessful"];
    }else{
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"registered"];
        msg = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_regUnsuccessful"];
    }
    [[NSUserDefaults standardUserDefaults] synchronize];
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:msg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
        [self close];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
}

- (IBAction)close{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [self.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:.25];
}

@end
