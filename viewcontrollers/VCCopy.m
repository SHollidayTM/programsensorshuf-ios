//
//  VCCopy.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 12/3/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCCopy.h"

@interface VCCopy ()

@end

@implementation VCCopy

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(copyResults:) name:@"CopyResults" object:nil];
    [[btnReadSensor layer] setCornerRadius:5];
    [btnReadSensor setClipsToBounds:YES];
    [[btnCopyTo layer] setCornerRadius:5];
    [btnCopyTo setClipsToBounds:YES];
    CAGradientLayer *gradient1 = [CAGradientLayer layer];
    [gradient1 setFrame:btnCopyTo.bounds];
    [gradient1 setColors:@[(id)[[UIColor colorWithRed:250.0/255.0 green:0 blue:0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:200.0/255.0 green:0 blue:0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:150.0/255.0 green:0 blue:0 alpha:1.0] CGColor]]];
    [btnCopyTo.layer insertSublayer:gradient1 atIndex:0];
    [btnCopyTo setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_copyToIntelliSens"] forState:UIControlStateNormal];
    [[btnRelearnInst layer] setCornerRadius:5];
    [btnRelearnInst setClipsToBounds:YES];
    CAGradientLayer *gradient2 = [CAGradientLayer layer];
    [gradient2 setFrame:btnRelearnInst.bounds];
    [gradient2 setColors:@[(id)[[UIColor colorWithRed:250.0/255.0 green:0 blue:0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:200.0/255.0 green:0 blue:0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:150.0/255.0 green:0 blue:0 alpha:1.0] CGColor]]];
    [btnRelearnInst.layer insertSublayer:gradient2 atIndex:0];
    [btnRelearnInst setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_instructions"] forState:UIControlStateNormal];
    [lblInst setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_placeHC1000Copy"]];
    [lblId setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_sensorID"]];
    [lblId2 setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_identifier"]];
    [lblMhz setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_mhz"]];
    [lblMhz2 setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_mhz"]];
}

- (void)viewDidAppear:(BOOL)animated{
    [vChooseSensor setTransform:CGAffineTransformMakeTranslation(0, vChooseSensor.frame.size.height+8)];
}

- (void)setup:(NSArray *)sentSensorNameFreqs rel:(Relearn *)sentRelearn{
    relearn = sentRelearn;
    if([sentSensorNameFreqs count] > 1){
        metal = [[NSMutableArray alloc] init];
        rubber = [[NSMutableArray alloc] init];
        for(SensorNameFreq *sNameFreq in sentSensorNameFreqs){
            if([sNameFreq valveMaterial]){
                if([[sNameFreq valveMaterial] isEqualToString:@"M"]){
                    [metal addObject:sNameFreq];
                }else{
                    [rubber addObject:sNameFreq];
                }
            }
        }
        [tblMetal reloadData];
        [tblRubber reloadData];
    }else{
        sensorNameFreq = [sentSensorNameFreqs objectAtIndex:0];
    }
    _state = 0;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int count = 0;
    if([tableView isEqual:tblMetal]){
        count = (int)[metal count];
    }else{
        count = (int)[rubber count];
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(2, 2, tableView.frame.size.width-4, 40)];
    [lbl setBackgroundColor:[UIColor colorWithRed:211.0/255.0 green:211.0/255.0 blue:211.0/255.0 alpha:.5]];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    SensorNameFreq *sNameFreq;
    if([tableView isEqual:tblMetal]){
        sNameFreq = [metal objectAtIndex:indexPath.row];
    }else{
        sNameFreq = [rubber objectAtIndex:indexPath.row];
    }
    [lbl setText:[sNameFreq name]];
    [cell.contentView addSubview:lbl];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([tableView isEqual:tblMetal]){
        sensorNameFreq = [metal objectAtIndex:indexPath.row];
    }else{
        sensorNameFreq = [rubber objectAtIndex:indexPath.row];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SensorNameFreqSelected" object:sensorNameFreq];
    [self program];
}

- (IBAction)copy{
    NSDictionary *dic = @{@"action":@2};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CopySensor" object:dic];
}

- (void)copyResults:(NSNotification *)notification{
    if([notification object]){ //from copy
        [lblInst setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_copyFromList"]];
        NSDictionary *dic = [notification object];
        NSData *resultData = [dic objectForKey:@"resultData"];
        const uint8_t *bytes = [resultData bytes];
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"hexDec"]){
            [lblId2 setText:[NSString stringWithFormat:@"%hhu%hhu%hhu%hhu", bytes[4], bytes[5], bytes[6], bytes[7]]];
        }else{
            [lblId2 setText:[[NSString stringWithFormat:@"%02x%02x%02x%02x", bytes[4], bytes[5], bytes[6], bytes[7]] uppercaseString]];
        }
        uint8_t mhz = (uint8_t)[[dic objectForKey:@"frequency"] unsignedCharValue];
        NSString *strFreq = @"315";
        if(mhz == 0x02){
            strFreq = @"433";
        }
        [lblMhz2 setText:strFreq];
        [btnCopyTo setHidden:NO];
    }else{ //from create
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_sensorCopied"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
        [btnCopyTo setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_copyAnother"] forState:UIControlStateNormal];
        [btnRelearnInst setHidden:NO];
        [self closeSensorSelect];
        _state = 1;
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoadSensor" object:nil];
}

- (IBAction)createFromCopy{
    if(!sensorNameFreq){
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.25];
        [vChooseSensor setTransform:CGAffineTransformMakeTranslation(0, 0)];
        [UIView commitAnimations];
    }else{
        [self program];
    }
}

- (void)program{
    NSDictionary *dic = @{@"message":[NSString stringWithFormat:@"%@ %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_programmingSensor"], [sensorNameFreq name]], @"hideCancel":@0};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoadSensor" object:dic];
    dic = @{@"action":@1};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CreateFromCopy" object:dic];
}

- (IBAction)relearnInst{
    if(relearn){
        if([relearn instructions]){
            NSDictionary *dic = @{@"html":[relearn instructions], @"links":@NO};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenPdfViewer" object:dic];
        }else{
            [self noRelearn];
        }
    }else{
        [self noRelearn];
    }
}

- (void)noRelearn{
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_noRelearnInst"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
}

- (IBAction)closeSensorSelect{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vChooseSensor setTransform:CGAffineTransformMakeTranslation(0, vChooseSensor.frame.size.height+8)];
    [UIView commitAnimations];
    [lblInst setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_placeHC1000Copy"]];
}

- (IBAction)openSensorSelect{
    [vChooseSensor setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [lblInst setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_copyFromList"]];
    [tblMetal reloadData];
    [tblRubber reloadData];
    _state = 0;
}

@end
