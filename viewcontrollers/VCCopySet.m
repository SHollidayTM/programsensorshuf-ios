//
//  VCCopySet.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 12/3/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCCopySet.h"

@interface VCCopySet ()

@end

@implementation VCCopySet

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(copySetResults:) name:@"CopySetResults" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(createFromSetResults:) name:@"CreateFromSetResults" object:nil];
    datIds = [[NSMutableArray alloc] initWithObjects:[NSNull null], [NSNull null], [NSNull null], [NSNull null], [NSNull null], nil];
}

- (void)viewDidAppear:(BOOL)animated{
    [vChooseSensor setTransform:CGAffineTransformMakeTranslation(0, vChooseSensor.frame.size.height+8)];
}

- (void)setup:(NSArray *)sentSensorNameFreqs rel:(Relearn *)sentRelearn{
    relearn = sentRelearn;
    if([sentSensorNameFreqs count] > 1){
        metal = [[NSMutableArray alloc] init];
        rubber = [[NSMutableArray alloc] init];
        for(SensorNameFreq *sNameFreq in sentSensorNameFreqs){
            if([sNameFreq valveMaterial]){
                if([[sNameFreq valveMaterial] isEqualToString:@"M"]){
                    [metal addObject:sNameFreq];
                }else{
                    [rubber addObject:sNameFreq];
                }
            }
        }
        [tblMetal reloadData];
        [tblRubber reloadData];
    }else{
        sensorNameFreq = [sentSensorNameFreqs objectAtIndex:0];
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    int count = 0;
    if([tableView isEqual:tblMetal]){
        count = (int)[metal count];
    }else{
        count = (int)[rubber count];
    }
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(2, 2, tableView.frame.size.width-4, 40)];
    [lbl setBackgroundColor:[UIColor colorWithRed:211.0/255.0 green:211.0/255.0 blue:211.0/255.0 alpha:.5]];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    SensorNameFreq *sNameFreq;
    if([tableView isEqual:tblMetal]){
        sNameFreq = [metal objectAtIndex:indexPath.row];
    }else{
        sNameFreq = [rubber objectAtIndex:indexPath.row];
    }
    [lbl setText:[sNameFreq name]];
    [cell.contentView addSubview:lbl];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    if([tableView isEqual:tblMetal]){
        sensorNameFreq = [metal objectAtIndex:indexPath.row];
    }else{
        sensorNameFreq = [rubber objectAtIndex:indexPath.row];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"SensorNameFreqSelected" object:sensorNameFreq];
    [self program];
}

- (IBAction)copy:(UIButton *)sender{
    if([datIds objectAtIndex:[sender tag]] == [NSNull null]){
        copySensor = (int)[sender tag];
        [self setupCopySet];
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_checkSensorAgain"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actCancel = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actCancel];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            copySensor = (int)[sender tag];
            [datIds replaceObjectAtIndex:copySensor withObject:[NSNull null]];
            [self setupCopySet];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

- (void)setupCopySet{
    checkCount = 0;
    [self doCopySet];
}

- (void)doCopySet{
    NSDictionary *dic = @{@"action":@2};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CopySetSensor" object:dic];
}

- (void)copySetResults:(NSNotification *)notification{
    NSDictionary *dic = [notification object];
    NSData *resultData = [dic objectForKey:@"resultData"];
    NSData *currId = [resultData subdataWithRange:NSMakeRange(4, 4)];
    BOOL duplicate = NO;
    for(NSData *prevID in datIds){
        if([currId isEqual:prevID]){
            if(checkCount < 2){
                checkCount++;
                [self doCopySet];
                return;
            }
            duplicate = YES;
            break;
        }
    }
    if(!duplicate){
        [datIds replaceObjectAtIndex:copySensor withObject:currId];
        switch(copySensor){
            case 0:
                [btnLeftFront setSelected:YES];
                [btnLeftFrontCopy setEnabled:YES];
                break;
            case 1:
                [btnRightFront setSelected:YES];
                [btnRightFrontCopy setEnabled:YES];
                break;
            case 2:
                [btnRightRear setSelected:YES];
                [btnRightRearCopy setEnabled:YES];
                break;
            case 3:
                [btnLeftRear setSelected:YES];
                [btnLeftRearCopy setEnabled:YES];
                break;
            case 4:
                [btnSpare setSelected:YES];
                [btnSpareCopy setEnabled:YES];
                break;
        }
        strMsg = [NSString stringWithFormat:@"%@: ", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_id"]];
        const uint8_t *bytes = [resultData bytes];
        if([[NSUserDefaults standardUserDefaults] boolForKey:@"hexDec"]){
            strMsg = [strMsg stringByAppendingString:[NSString stringWithFormat:@"%hhu%hhu%hhu%hhu", bytes[4], bytes[5], bytes[6], bytes[7]]];
        }else{
            strMsg = [strMsg stringByAppendingString:[[NSString stringWithFormat:@"%02x%02x%02x%02x", bytes[4], bytes[5], bytes[6], bytes[7]] uppercaseString]];
        }
        uint8_t mhz = (uint8_t)[[dic objectForKey:@"frequency"] unsignedCharValue];
        NSString *strFreq = @"315";
        if(mhz == 0x02){
            strFreq = @"433";
        }
        strMsg = [strMsg stringByAppendingString:[NSString stringWithFormat:@"\r\n%@: %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_mhz"], strFreq]];
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_readSuccessful"] message:strMsg preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actCopy = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_copySensor"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            [self createFromCopy];
        }];
        [ctrAlert addAction:actCopy];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:.25];
            switch(copySensor){
                case 0:
                    [txvLeftFront setText:strMsg];
                    [txvLeftFront setAlpha:1];
                    break;
                case 1:
                    [txvRightFront setText:strMsg];
                    [txvRightFront setAlpha:1];
                    break;
                case 2:
                    [txvRightRear setText:strMsg];
                    [txvRightRear setAlpha:1];
                    break;
                case 3:
                    [txvLeftRear setText:strMsg];
                    [txvLeftRear setAlpha:1];
                    break;
                case 4:
                    [txvSpare setText:strMsg];
                    [txvSpare setAlpha:1];
                    break;
            }
            [UIView commitAnimations];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_duplicateSensor"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoadSensor" object:nil];
}

- (IBAction)createFromButton:(UIButton *)sender{
    copySensor = (int)[sender tag];
    [self createFromCopy];
}

- (void)createFromCopy{
    if(!sensorNameFreq){
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:.25];
        [vChooseSensor setTransform:CGAffineTransformMakeTranslation(0, 0)];
        [UIView commitAnimations];
    }else{
        [self program];
    }
}

- (void)program{
    NSDictionary *dic = @{@"message":[NSString stringWithFormat:@"%@ %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_programmingSensor"], [sensorNameFreq name]], @"hideCancel":@0};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoadSensor" object:dic];
    dic = @{@"id":[datIds objectAtIndex:copySensor]};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CreateFromSet" object:dic];
}

- (void)relearnInst{
    if(relearn){
        if([relearn instructions]){
            NSDictionary *dic = @{@"html":[relearn instructions], @"links":@NO};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenPdfViewer" object:dic];
        }else{
            [self noRelearn];
        }
    }else{
        [self noRelearn];
    }
}

- (void)noRelearn{
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_noRelearnInst"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
}

- (IBAction)closeSensorSelect{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vChooseSensor setTransform:CGAffineTransformMakeTranslation(0, vChooseSensor.frame.size.height+8)];
    [UIView commitAnimations];
}

- (void)openSensorSelect{
    [vChooseSensor setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [tblMetal reloadData];
    [tblRubber reloadData];
}

- (void)createFromSetResults:(NSNotification *)notification{
    switch(copySensor){
        case 0: //LF
            [btnLeftFrontCopy setSelected:YES];
            break;
        case 1: //RF
            [btnRightFrontCopy setSelected:YES];
            break;
        case 2: //RR
            [btnRightRearCopy setSelected:YES];
            break;
        case 3: //LR
            [btnLeftRearCopy setSelected:YES];
            break;
        case 4: //SP
            [btnSpareCopy setSelected:YES];
            break;
    }
    NSDictionary *dic = [notification object];
    NSData *resultData = [dic objectForKey:@"resultData"];
    strMsg = [NSString stringWithFormat:@"%@: ", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_id"]];
    const uint8_t *bytes = [resultData bytes];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"hexDec"]){
        strMsg = [strMsg stringByAppendingString:[NSString stringWithFormat:@"%hhu%hhu%hhu%hhu", bytes[0], bytes[1], bytes[2], bytes[3]]];
    }else{
        strMsg = [strMsg stringByAppendingString:[[NSString stringWithFormat:@"%02x%02x%02x%02x", bytes[0], bytes[1], bytes[2], bytes[3]] uppercaseString]];
    }
    uint8_t mhz = (uint8_t)[[dic objectForKey:@"frequency"] unsignedCharValue];
    NSString *strFreq = @"315";
    if(mhz == 0x02){
        strFreq = @"433";
    }
    strMsg = [strMsg stringByAppendingString:[NSString stringWithFormat:@"\r\n%@: %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_mhz"], strFreq]];
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_sensorProgrammed"] message:strMsg preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actRelearn = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_relearn"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
        [self relearnInst];
    }];
    [ctrAlert addAction:actRelearn];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
    [self closeSensorSelect];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoadSensor" object:nil];
}

@end
