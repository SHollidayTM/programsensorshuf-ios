//
//  VCPdfViewer.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/25/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCPdfViewer.h"

@interface VCPdfViewer ()

@end

@implementation VCPdfViewer

- (void)viewDidLoad{
    [super viewDidLoad];
    [[btnClose layer] setCornerRadius:5];
    [btnClose setClipsToBounds:YES];
    [[btnPrint layer] setCornerRadius:5];
    [btnPrint setClipsToBounds:YES];
    [btnClose setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_close"] forState:UIControlStateNormal];
    [btnPrint setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_print"] forState:UIControlStateNormal];
}

- (void)loadHtml:(NSString *)sentHtml links:(BOOL)addLinks{
    isPdf = NO;
    if(addLinks){
        html = [NSString stringWithFormat:@"<p>%@</p></br>%@</br><p><a href=\"http://activate\">Activate Sensors</a></p><p>%@</p><a href=\"http://troubleshoot\">Troubleshooting</a></p>", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_obdComing"], sentHtml, [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_activationOnly"]];
    }else{
        html = sentHtml;
    }
    [webPdf loadHTMLString:html baseURL:nil];
}

- (void)loadPdf:(NSString *)sentName{
    isPdf = YES;
    name = sentName;
    NSURLRequest *request = [NSURLRequest requestWithURL:[NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:name ofType:@".pdf"]]];
    [webPdf loadRequest:request];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if(navigationType == UIWebViewNavigationTypeLinkClicked){
        if([[request URL].absoluteString isEqualToString:@"http://activate/"]){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenCheck" object:nil];
        }else if([[request URL].absoluteString isEqualToString:@"http://troubleshoot/"]){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"CheckAttachments" object:nil];
        }else{
            [[UIApplication sharedApplication] openURL:[request URL]];
        }
        return NO;
    }
    return YES;
}

- (IBAction)close{
    [self.view removeFromSuperview];
}

- (IBAction)print{
    UIPrintInteractionController *pic = [UIPrintInteractionController sharedPrintController];
    NSData *data;
    if(isPdf){
        data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:name ofType:@".pdf"]];
        [pic setPrintingItem:data];
    }else{
        UIPrintFormatter *formatter = [[UIMarkupTextPrintFormatter alloc] initWithMarkupText:html];
        [[UIPrintInteractionController sharedPrintController] setPrintFormatter:formatter];
    }
    [pic setDelegate:self];
    UIPrintInfo *printInfo = [UIPrintInfo printInfo];
    [printInfo setOutputType:UIPrintInfoOutputGeneral];
    [printInfo setJobName:[NSString stringWithFormat:@"%@ from %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_print"], [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_appName"]]];
    [printInfo setDuplex:UIPrintInfoDuplexLongEdge];
    [pic setPrintInfo:printInfo];
    [pic setShowsPageRange:YES];
    void (^completionHandler)(UIPrintInteractionController *, BOOL, NSError *) = ^(UIPrintInteractionController *pic, BOOL completed, NSError *error){
        if(!completed && error){
            NSLog(@"Print failed: %@.", error.localizedDescription);
        }
    };
    [pic presentAnimated:YES completionHandler:completionHandler];
}

@end
