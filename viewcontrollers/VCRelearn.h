//
//  VCRelearn.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 12/4/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "Relearn.h"

@interface VCRelearn : UIViewController{
    IBOutlet UILabel *lblInst;
    IBOutlet UITableView *tbl;
    IBOutlet UIButton *btnInstructions;
    IBOutlet UIButton *btnOBD;
    IBOutlet UIButton *btnTroubleshooting;
    Relearn *relearn;
}

- (void)relearnReturned:(Relearn *)sentRelearn;
- (IBAction)instructions;
- (IBAction)obd;
- (IBAction)troubleshooting;

@end
