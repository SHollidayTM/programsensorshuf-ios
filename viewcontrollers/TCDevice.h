//
//  TCDevice.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/24/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TCDevice : UITableViewCell{
    IBOutlet UIView *vBackground;
}

@property (nonatomic, strong) IBOutlet UIImageView *imvDevice;
@property (nonatomic, strong) IBOutlet UILabel *lblName;
@property (nonatomic, strong) IBOutlet UILabel *lblDesc;

@end
