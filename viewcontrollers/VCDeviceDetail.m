//
//  VCDeviceDetail.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/25/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCDeviceDetail.h"

@interface VCDeviceDetail ()

@end

@implementation VCDeviceDetail

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(program) name:@"OpenServiceFromDetail" object:nil];
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad){
        heightOne = 516;
        heightTwo = 537;
        heightThree = 691;
        heightFour = 720;
        heightFive = 788;
        heightSix = 836;
    }else{
        heightOne = 208;
        heightTwo = 237;
        heightThree = 391;
        heightFour = 420;
        heightFive = 488;
        heightSix = 536;
    }
}

- (void)setupSensor:(SensorTPMS *)sentSensor ymm:(BOOL)inYMM{
    sensor = sentSensor;
    name = [sensor partNumber];
    imgDevice = [self getImage:name];
    [imvDevice setImage:imgDevice];
    UILabel *lblToolCompat = [[UILabel alloc] initWithFrame:CGRectMake(8, heightOne, self.view.frame.size.width-16, 21)];
    [lblToolCompat setFont:[UIFont boldSystemFontOfSize:16]];
    [lblToolCompat setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_toolCompat"]];
    [vContent addSubview:lblToolCompat];
    UITextView *txtDesc = [[UITextView alloc] initWithFrame:CGRectMake(8, heightTwo, self.view.frame.size.width-16, 146)];
    [txtDesc setEditable:NO];
    [txtDesc setScrollEnabled:NO];
    [txtDesc setText:[NSString stringWithFormat:@"%@ Huf HC1000\nBartech Tech 400\nBartech Tech 500\nATEQ VT-55\nATEQ VT-56\nTecnomotor TPM-02", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_sensorRequiresTool"]]];
    [vContent addSubview:txtDesc];
    UILabel *lblTorqueReq = [[UILabel alloc] initWithFrame:CGRectMake(8, heightThree, self.view.frame.size.width-16, 21)];
    [lblTorqueReq setFont:[UIFont boldSystemFontOfSize:16]];
    [lblTorqueReq setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_torqueReqs"]];
    [vContent addSubview:lblTorqueReq];
    UITextView *txtDescTorque = [[UITextView alloc] initWithFrame:CGRectMake(8, heightFour, self.view.frame.size.width-16, 60)];
    [txtDescTorque setEditable:NO];
    [txtDescTorque setScrollEnabled:NO];
    [txtDescTorque setText:[NSString stringWithFormat:@"%@: %@\n%@: %@\n%@: %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_torqueNut"], [sensor torqueNut], [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_torqueScrew"], [sensor torqueScrew], [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_torqueCore"], [sensor torqueCore]]];
    [vContent addSubview:txtDescTorque];
    UIButton *btnTechDocs = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnTechDocs setFrame:CGRectMake(8, heightFive, self.view.frame.size.width-16, 40)];
    [btnTechDocs setBackgroundColor:[UIColor grayColor]];
    [[btnTechDocs layer] setCornerRadius:5];
    [btnTechDocs setClipsToBounds:YES];
    [btnTechDocs setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_techDocs"] forState:UIControlStateNormal];
    [btnTechDocs addTarget:self action:@selector(techDocs) forControlEvents:UIControlEventTouchUpInside];
    [vContent addSubview:btnTechDocs];
    if(inYMM){
        UIButton *btnProgram = [UIButton buttonWithType:UIButtonTypeCustom];
        [btnProgram setFrame:CGRectMake(8, heightSix, self.view.frame.size.width-16, 40)];
        [btnProgram setBackgroundColor:[UIColor grayColor]];
        [[btnProgram layer] setCornerRadius:5];
        [btnProgram setClipsToBounds:YES];
        [btnProgram setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_programWithHC1000"] forState:UIControlStateNormal];
        [btnProgram addTarget:self action:@selector(program) forControlEvents:UIControlEventTouchUpInside];
        [vContent addSubview:btnProgram];
    }
    [scrContent setScrollEnabled:YES];
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_sensorRequiresProg"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
    [self performSelector:@selector(dismissWarning) withObject:nil afterDelay:2];
}

- (void)dismissWarning{
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)setupDirectFit:(SensorTPMS *)sentSensor{
    sensor = sentSensor;
    name = [sensor partNumber];
    imgDevice = [self getImage:name];
    [imvDevice setImage:imgDevice];
    UILabel *lblTorqueReq = [[UILabel alloc] initWithFrame:CGRectMake(8, heightOne, self.view.frame.size.width-16, 21)];
    [lblTorqueReq setFont:[UIFont boldSystemFontOfSize:16]];
    [lblTorqueReq setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_torqueReqs"]];
    [vContent addSubview:lblTorqueReq];
    UITextView *txtDescTorque = [[UITextView alloc] initWithFrame:CGRectMake(8, heightTwo, self.view.frame.size.width-16, 146)];
    [txtDescTorque setEditable:NO];
    [txtDescTorque setScrollEnabled:NO];
    [txtDescTorque setText:[NSString stringWithFormat:@"%@: %@\n%@: %@\n%@: %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_torqueNut"], [sensor torqueNut], [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_torqueScrew"], [sensor torqueScrew], [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_torqueCore"], [sensor torqueCore]]];
    [vContent addSubview:txtDescTorque];
    UIButton *btnTechDocs = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnTechDocs setFrame:CGRectMake(8, heightThree, self.view.frame.size.width-16, 40)];
    [btnTechDocs setBackgroundColor:[UIColor grayColor]];
    [[btnTechDocs layer] setCornerRadius:5];
    [btnTechDocs setClipsToBounds:YES];
    [btnTechDocs setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_techDocs"] forState:UIControlStateNormal];
    [btnTechDocs addTarget:self action:@selector(techDocs) forControlEvents:UIControlEventTouchUpInside];
    [vContent addSubview:btnTechDocs];
    [scrContent setScrollEnabled:NO];
}

- (void)setupServiceKit:(ServiceKit *)sentServiceKit{
    serviceKit = sentServiceKit;
    name = [serviceKit partNumber];
    imgDevice = [self getImage:name];
    [imvDevice setImage:imgDevice];
    UILabel *lblType = [[UILabel alloc] initWithFrame:CGRectMake(8, heightOne, self.view.frame.size.width-16, 21)];
    [lblType setFont:[UIFont boldSystemFontOfSize:16]];
    NSMutableAttributedString *attType = [[NSMutableAttributedString alloc] initWithString:@"Type: Gen 2"];
    [attType addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(6, 5)];
    [lblType setAttributedText:attType];
    [vContent addSubview:lblType];
    UITextView *txtDesc = [[UITextView alloc] initWithFrame:CGRectMake(8, heightTwo, self.view.frame.size.width-16, 194)];
    [txtDesc setEditable:NO];
    [txtDesc setScrollEnabled:NO];
    NSMutableAttributedString *attDesc = [[NSMutableAttributedString alloc] initWithString:[serviceKit desc]];
    [attDesc addAttribute:NSFontAttributeName value:[UIFont boldSystemFontOfSize:14] range:NSMakeRange(0, 9)];
    [txtDesc setAttributedText:attDesc];
    [vContent addSubview:txtDesc];
    UIButton *btnTechDocs = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnTechDocs setFrame:CGRectMake(8, heightThree+48, self.view.frame.size.width-16, 40)];
    [btnTechDocs setBackgroundColor:[UIColor grayColor]];
    [[btnTechDocs layer] setCornerRadius:5];
    [btnTechDocs setClipsToBounds:YES];
    [btnTechDocs setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_techDocs"] forState:UIControlStateNormal];
    [btnTechDocs addTarget:self action:@selector(techDocs) forControlEvents:UIControlEventTouchUpInside];
    [vContent addSubview:btnTechDocs];
    [scrContent setScrollEnabled:NO];
}

- (UIImage *)getImage:(NSString *)partName{
    NSString *strDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    UIImage *img = [UIImage imageNamed:[NSString stringWithFormat:@"%@.jpg", partName]];
    if(!img){
        NSString *strPath = [strDir stringByAppendingPathComponent:[partName stringByAppendingString:@".jpg"]];
        img = [UIImage imageWithContentsOfFile:strPath];
        if(!img){
            img = [UIImage imageNamed:@"noimage.jpg"];
        }
    }
    return img;
}

- (IBAction)enlargeImage{
    vEnlarged = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [vEnlarged setBackgroundColor:[UIColor whiteColor]];
    UIImageView *imvEnlarged = [[UIImageView alloc] initWithFrame:vEnlarged.frame];
    [imvEnlarged setContentMode:UIViewContentModeScaleAspectFit];
    [imvEnlarged setImage:imgDevice];
    [vEnlarged addSubview:imvEnlarged];
    UIButton *btnCloseEnlarged = [UIButton buttonWithType:UIButtonTypeCustom];
    [btnCloseEnlarged setFrame:vEnlarged.frame];
    [btnCloseEnlarged addTarget:self action:@selector(closeEnlarged) forControlEvents:UIControlEventTouchUpInside];
    [vEnlarged addSubview:btnCloseEnlarged];
    [self.view addSubview:vEnlarged];
}

- (void)closeEnlarged{
    [vEnlarged removeFromSuperview];
}

- (void)techDocs{
    [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenTechDocs" object:nil];
}

- (void)program{
    if(![[ModuleBT sharedInstance] btObjectHC]){
        NSDictionary *dic = @{@"fromDetail":[NSNumber numberWithBool:YES]};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"ShowNoDevice" object:dic];
    }else{
        if(![[NSUserDefaults standardUserDefaults] integerForKey:@"registered"]){
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenRegister" object:nil];
        }else{
            [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenService" object:sensor];
        }
    }
}

@end
