//
//  TCDevice.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/24/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "TCDevice.h"

@implementation TCDevice

- (void)awakeFromNib{
    [super awakeFromNib];
    [[vBackground layer] setCornerRadius:5];
    [vBackground setClipsToBounds:YES];
    [self setAccessoryType:UITableViewCellAccessoryDisclosureIndicator];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated{
    [super setSelected:selected animated:animated];
}

@end
