//
//  VCLearningCenter.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/6/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "VCLearningCenter.h"

@interface VCLearningCenter ()

@end

@implementation VCLearningCenter

- (void)viewDidLoad {
    [super viewDidLoad];
    UIBarButtonItem *bbiBack = [[UIBarButtonItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_back"] style:UIBarButtonItemStylePlain target:nil action:nil];
    itmFirst = [[barNav items] objectAtIndex:0];
    [itmFirst setBackBarButtonItem:bbiBack];
    [[[barNav items] objectAtIndex:0] setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_learningCenter"]];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 4;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    [lbl setTextAlignment:NSTextAlignmentCenter];
    switch([indexPath row]){
        case 0:
            [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_training"]];
            break;
        case 1:
            [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_techDocs"]];
            break;
        case 2:
            [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_faq"]];
            break;
        case 3:
            [lbl setText:[NSString stringWithFormat:@"%@ HC1000", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_about"]]];
            break;
    }
    [cell.contentView addSubview:lbl];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch([indexPath row]){
        case 0:
            itmTraining = [[UINavigationItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_selectVideo"]];
            [barNav pushNavigationItem:itmTraining animated:YES];
            vcTraining = [[VCTraining alloc] initWithNibName:@"Training" bundle:nil];
            [vcTraining.view setFrame:CGRectMake(self.view.frame.size.width, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
            [self.view addSubview:vcTraining.view];
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:.25];
            [vcTraining.view setTransform:CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)];
            [UIView commitAnimations];
            break;
        case 1:{
            itmTechDocs = [[UINavigationItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_selectDocument"]];
            [barNav pushNavigationItem:itmTechDocs animated:YES];
            vcTechDocs = [[VCTechDocs alloc] initWithNibName:@"TechDocs" bundle:nil];
            [vcTechDocs.view setFrame:CGRectMake(self.view.frame.size.width, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
            [self.view addSubview:vcTechDocs.view];
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:.25];
            [vcTechDocs.view setTransform:CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)];
            [UIView commitAnimations];
        }
            break;
        case 2:
            itmFreqQues = [[UINavigationItem alloc] initWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_faq"]];
            [barNav pushNavigationItem:itmFreqQues animated:YES];
            vcFreqQues = [[VCFreqQues alloc] initWithNibName:@"FreqQues" bundle:nil];
            [vcFreqQues.view setFrame:CGRectMake(self.view.frame.size.width, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
            [self.view addSubview:vcFreqQues.view];
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:.25];
            [vcFreqQues.view setTransform:CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)];
            [UIView commitAnimations];
            break;
        case 3:
            itmHC1010 = [[UINavigationItem alloc] initWithTitle:[NSString stringWithFormat:@"%@ HC1000", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_about"]]];
            [barNav pushNavigationItem:itmHC1010 animated:YES];
            vcHC1010 = [[VCHC1010 alloc] initWithNibName:@"HC1000" bundle:nil];
            [vcHC1010.view setFrame:CGRectMake(self.view.frame.size.width, 44, self.view.frame.size.width, self.view.frame.size.height-44)];
            [self.view addSubview:vcHC1010.view];
            [UIView beginAnimations:nil context:nil];
            [UIView setAnimationDuration:.25];
            [vcHC1010.view setTransform:CGAffineTransformMakeTranslation(-self.view.frame.size.width, 0)];
            [UIView commitAnimations];
            break;
    }
}

- (BOOL)navigationBar:(UINavigationBar *)navigationBar shouldPopItem:(UINavigationItem *)item{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    if([itmTraining isEqual:item]){
        [vcTraining.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    }else if([itmTechDocs isEqual:item]){
        [vcTechDocs.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    }else if([itmFreqQues isEqual:item]){
        [vcFreqQues.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    }else if([itmHC1010 isEqual:item]){
        [vcHC1010.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    }
    [UIView commitAnimations];
    return YES;
}

- (void)popBack{
    [barNav popNavigationItemAnimated:YES];
}

- (IBAction)close{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [self.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
    [UIView commitAnimations];
    [self.view performSelector:@selector(removeFromSuperview) withObject:nil afterDelay:.25];
}

@end
