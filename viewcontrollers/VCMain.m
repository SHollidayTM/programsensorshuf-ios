//
//  VCMain.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/23/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCMain.h"

@interface VCMain ()

@end

@implementation VCMain

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startLoading:) name:@"StartLoading" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoading) name:@"StopLoading" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startLoadSensor:) name:@"StartLoadSensor" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLoadSensor) name:@"UpdateLoadSensor" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateLoadFirmware:) name:@"UpdateLoadFirmware" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(stopLoadSensor) name:@"StopLoadSensor" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openPdfViewer:) name:@"OpenPdfViewer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openWarnings:) name:@"OpenWarnings" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openRegister) name:@"OpenRegister" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openLearningCenter) name:@"OpenLearningCenter" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closeHolder) name:@"CloseHolder" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(attachmentsReturned:) name:@"ReturnAttachments" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sentActionsReturned) name:@"ReturnSentActions" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(vehiclesReturned:) name:@"ReturnVehicles" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(vehiclesError:) name:@"ReturnVehiclesError" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openTroubleshooting:) name:@"OpenTroubleshooting" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(openHistory) name:@"OpenHistory" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showNoNetwork) name:@"ShowNoNetwork" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showNoDevice:) name:@"ShowNoDevice" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(latestUpdate:) name:@"LatestUpdate" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(btConnected) name:@"BTConnected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(btDisconnected) name:@"BTDisconnected" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(closePdfViewer) name:@"ClosePdfViewer" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateFirmware) name:@"UpdateFirmware" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(firmwareUpdated) name:@"FirmwareUpdated" object:nil];
    [barMain setTintColor:[UIColor whiteColor]];
    currTab = 0;
    [barMain setSelectedItem:[[barMain items] objectAtIndex:currTab]];
    UISwipeGestureRecognizer *swpUp = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(moveButtonsUp)];
    [swpUp setDirection:UISwipeGestureRecognizerDirectionUp];
    [vUnderButtons addGestureRecognizer:swpUp];
    if(![[NSUserDefaults standardUserDefaults] stringForKey:@"securityKey"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"" forKey:@"securityKey"];
    }
    [[NSUserDefaults standardUserDefaults] setObject:@"2000-01-01" forKey:@"newVehicleDate"];
    if(![[NSUserDefaults standardUserDefaults] stringForKey:@"newVehicleDate"]){
        [[NSUserDefaults standardUserDefaults] setObject:@"2017-02-17" forKey:@"newVehicleDate"];
    }
    if(![[NSUserDefaults standardUserDefaults] integerForKey:@"language"]){
        [[NSUserDefaults standardUserDefaults] setInteger:0 forKey:@"language"];
    }
    //
    //[[NSUserDefaults standardUserDefaults] setInteger:1 forKey:@"registered"];
    //
    [[NSUserDefaults standardUserDefaults] synchronize];
    [btnOpenProd setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_prodSearch"] forState:UIControlStateNormal];
    [btnOpenTPMS setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_tpmsService"] forState:UIControlStateNormal];
    [btnByYMM setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_searchByYMM"] forState:UIControlStateNormal];
    [btnByVIN setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_searchByVIN"] forState:UIControlStateNormal];
    [btnByProd setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_searchByProdNum"] forState:UIControlStateNormal];
    [[[barMain items] objectAtIndex:0] setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_home"]];
    [[[barMain items] objectAtIndex:1] setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_settings"]];
    [[[barMain items] objectAtIndex:2] setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_support"]];
    [[[barMain items] objectAtIndex:3] setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_more"]];
    [btnCancel setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] forState:UIControlStateNormal];
}

- (void)viewDidAppear:(BOOL)animated{
    vcSettings = [[VCSettings alloc] initWithNibName:@"Settings" bundle:nil];
    [vcSettings.view.layer setShadowColor:[UIColor blackColor].CGColor];
    [vcSettings.view.layer setShadowOpacity:.5];
    [vcSettings.view.layer setShadowRadius:10];
    [vcSettings.view.layer setShadowOffset:CGSizeMake(0, 0)];
    [vcSettings.view setFrame:CGRectMake(8, 83, self.view.frame.size.width-16, self.view.frame.size.height-140)];
    [vcSettings.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    [self.view insertSubview:vcSettings.view belowSubview:vLoading];
    vcMore = [[VCMore alloc] initWithNibName:@"More" bundle:nil];
    [vcMore.view.layer setShadowColor:[UIColor blackColor].CGColor];
    [vcMore.view.layer setShadowOpacity:.5];
    [vcMore.view.layer setShadowRadius:10];
    [vcMore.view.layer setShadowOffset:CGSizeMake(0, 0)];
    [vcMore.view setFrame:CGRectMake(8, 83, self.view.frame.size.width-16, self.view.frame.size.height-140)];
    [vcMore.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    [self.view insertSubview:vcMore.view belowSubview:vLoading];
    reach = [Reachability reachabilityWithHostname:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"apiUrl"]];
    NetworkStatus status = [reach currentReachabilityStatus];
    if(status == NotReachable){
        [self showNoNetwork];
    }else{
        [webAds loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:@"http://portal.tpmsmanager.com/hufnews"]]];
        NSDate *dteLastChecked = [[NSUserDefaults standardUserDefaults] objectForKey:@"dteLastChecked"];
        NSDate *dtePlusAWeek = [NSDate dateWithTimeIntervalSinceNow:604800];
        if(!dteLastChecked){
            [[NSUserDefaults standardUserDefaults] setObject:dtePlusAWeek forKey:@"dteLastChecked"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }else if([dteLastChecked compare:[NSDate date]] == NSOrderedAscending){
            [self checkData];
            [[NSUserDefaults standardUserDefaults] setObject:dtePlusAWeek forKey:@"dteLastChecked"];
            [[NSUserDefaults standardUserDefaults] synchronize];
        }
    }
}

- (void)btConnected{
    [imvBluetooth setHighlighted:YES];
    if(!fromDetail){
        if(!holderIsOpenAndInService){
            [self openTPMSService];
        }
    }else{
        [[NSNotificationCenter defaultCenter] postNotificationName:@"OpenServiceFromDetail" object:nil];
    }
}

- (void)btDisconnected{
    [imvBluetooth setHighlighted:NO];
}

- (void)checkData{
    NetworkStatus status = [reach currentReachabilityStatus];
    if(status != NotReachable){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^{
            [[ModuleGetLatestUpdate sharedInstance] go];
            dispatch_async(dispatch_get_main_queue(), ^{
                NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_checkNewData"], @"hideCancel":@0};
                NSNotification *notification = [[NSNotification alloc] initWithName:@"StartLoading" object:dic userInfo:nil];
                [self startLoading:notification];
            });
        });
    }
}

- (BOOL)prefersStatusBarHidden{
    return YES;
}

- (void)startLoading:(NSNotification *)notification{
    stopFetching = NO;
    [vcHolder setStopFetching:NO];
    NSDictionary *dic = [notification object];
    NSString *message = [dic objectForKey:@"message"];
    [lblLoading setText:message];
    [btnCancel setHidden:[[dic objectForKey:@"hideCancel"] intValue]];
    [vLoading setHidden:NO];
}

- (void)stopLoading{
    dispatch_async(dispatch_get_main_queue(), ^{
        [vLoading setHidden:YES];
    });
}

- (IBAction)cancelLoading{
    [self stopLoading];
    stopFetching = YES;
    [vcHolder setStopFetching:YES];
}

- (void)startLoadSensor:(NSNotification *)notification{
    NSDictionary *dic = [notification object];
    NSString *message = [dic objectForKey:@"message"];
    [lblLoadSensor setText:message];
    [prgSensor setProgress:0];
    [btnCancelSensor setHidden:[[dic objectForKey:@"hideCancel"] intValue]];
    [vLoadSensor setHidden:NO];
}

- (void)updateLoadSensor{
    float prog = .0025 + [prgSensor progress];
    [prgSensor setProgress:prog];
    if(![[ModuleBT sharedInstance] creating]){
        if([prgSensor progress] == 1){
            [self cancelLoadSensor];
            UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_errorTimeout"] preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                [self dismissViewControllerAnimated:YES completion:nil];
            }];
            [ctrAlert addAction:actOk];
            [self presentViewController:ctrAlert animated:YES completion:nil];
        }
    }
}

- (void)updateLoadFirmware:(NSNotification *)notification{
    NSDictionary *dic = [notification object];
    float prog = [[dic objectForKey:@"value"] floatValue];
    [prgSensor setProgress:prog];
}

- (void)stopLoadSensor{
    dispatch_async(dispatch_get_main_queue(), ^{
        [vLoadSensor setHidden:YES];
    });
}

- (IBAction)cancelLoadSensor{
    [[ModuleBT sharedInstance] cancel];
    [self stopLoadSensor];
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    if(navigationType == UIWebViewNavigationTypeLinkClicked){
        [[UIApplication sharedApplication] openURL:[request URL]];
        return NO;
    }
    return YES;
}

- (void)webViewDidStartLoad:(UIWebView *)webView{
    [vWebLoading setHidden:NO];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView{
    [vWebLoading setHidden:YES];
}

- (void)createHolder{
    vcHolder = [[VCHolder alloc] initWithNibName:@"Holder" bundle:nil];
    [vcHolder.view.layer setShadowColor:[UIColor blackColor].CGColor];
    [vcHolder.view.layer setShadowOpacity:.5];
    [vcHolder.view.layer setShadowRadius:10];
    [vcHolder.view.layer setShadowOffset:CGSizeMake(0, 0)];
    [vcHolder.view setFrame:CGRectMake(8, 83, self.view.frame.size.width-16, self.view.frame.size.height-140)];
    [vcHolder.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    [self.view insertSubview:vcHolder.view belowSubview:vcSettings.view];
}

- (IBAction)openProdSearch{
    reach = [Reachability reachabilityWithHostname:[[NSBundle mainBundle] objectForInfoDictionaryKey:@"apiUrl"]];
    NetworkStatus status = [reach currentReachabilityStatus];
    if(status == NotReachable){
        [self showNoNetwork];
    }else{
        [self createHolder];
        [vcHolder setInService:NO];
        [btnByProd setHidden:NO];
        [imvByProd setHidden:NO];
        [self moveButtonsDown];
    }
}

- (IBAction)openTPMSService{
    //if([[ModuleBT sharedInstance] btObjectHC]){
    //    if([[[ModuleBT sharedInstance] btObjectHC] connected]){
    //        if(![[NSUserDefaults standardUserDefaults] integerForKey:@"registered"]){
    //            [self openRegister];
    //        }else{
                [self createHolder];
                [vcHolder setInService:YES];
                [btnByProd setHidden:YES];
                [imvByProd setHidden:YES];
                [self moveButtonsDown];
    //        }
    //    }else{
    //        [self showNoDevice:nil];
    //    }
    //}else{
    //    [self showNoDevice:nil];
    //}
}

- (void)openRegister{
    vcRegister = [[VCRegister alloc] initWithNibName:@"Register" bundle:nil];
    [vcRegister.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view insertSubview:vcRegister.view belowSubview:vLoading];
    [vcRegister.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcRegister.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [UIView commitAnimations];
}

- (void)openLearningCenter{
    vcLearningCenter = [[VCLearningCenter alloc] initWithNibName:@"LearningCenter" bundle:nil];
    [vcLearningCenter.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view insertSubview:vcLearningCenter.view belowSubview:vLoading];
    [vcLearningCenter.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcLearningCenter.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [UIView commitAnimations];
}

- (void)openHistory{
    vcHistory = [[VCHistory alloc] initWithNibName:@"History" bundle:nil];
    [vcHistory.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [self.view insertSubview:vcHistory.view belowSubview:vLoading];
    [vcHistory.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcHistory.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [UIView commitAnimations];
}

- (IBAction)clickBanner{
    UITabBarItem *itm = [barMain.items objectAtIndex:(0)];
    [self tabBar:barMain didSelectItem:itm];
}

- (void)tabBar:(UITabBar *)tabBar didSelectItem:(UITabBarItem *)item{
    if(!(currTab == [item tag])){
        switch([item tag]){
            case 0:
                [self closeEverything];
                [barMain setSelectedItem:[[barMain items] objectAtIndex:[item tag]]];
                break;
            case 1:
                [self closeEverything];
                [barMain setSelectedItem:[[barMain items] objectAtIndex:[item tag]]];
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:.25];
                [vcSettings.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
                [UIView commitAnimations];
                [vcSettings setVersions];
                break;
            case 2:{
                UIAlertController *ctrAlert;
                /*
                if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone){
                    ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_contactHufSupport"] message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_callSupportOne"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *actCall = [UIAlertAction actionWithTitle:@"Call" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                        [self dismissViewControllerAnimated:YES completion:nil];
                        [barMain setSelectedItem:[[barMain items] objectAtIndex:prevTab]];
                        currTab = prevTab;
                        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"tel:1-855-483-8767"]];
                    }];
                    [ctrAlert addAction:actCall];
                }else{
                    ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_contactHufSupport"] message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_callSupportTwo"] preferredStyle:UIAlertControllerStyleAlert];
                }
                */
                ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_contactHufSupport"] message:@"Email support at huf@tpmssupport.com." preferredStyle:UIAlertControllerStyleAlert];
                //
                UIAlertAction *actEmail = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_email"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [barMain setSelectedItem:[[barMain items] objectAtIndex:prevTab]];
                    currTab = prevTab;
                    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"mailto:huf@tpmssupport.com"]];
                }];
                [ctrAlert addAction:actEmail];
                UIAlertAction *actCancel = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
                    [self dismissViewControllerAnimated:YES completion:nil];
                    [barMain setSelectedItem:[[barMain items] objectAtIndex:prevTab]];
                    currTab = prevTab;
                }];
                [ctrAlert addAction:actCancel];
                [self presentViewController:ctrAlert animated:YES completion:nil];
            }
                break;
            case 3:{
                [self closeEverything];
                [barMain setSelectedItem:[[barMain items] objectAtIndex:[item tag]]];
                [UIView beginAnimations:nil context:nil];
                [UIView setAnimationDuration:.25];
                [vcMore.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
                [UIView commitAnimations];
            }
                break;
        }
        prevTab = currTab;
        currTab = (int)[item tag];
    }else if([item tag] == 0){
        [self closeHolder];
        [self moveButtonsUp];
    }
}

- (void)moveButtonsDown{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vButtons setTransform:CGAffineTransformMakeTranslation(0, vButtons.frame.size.height+8)];
    [UIView commitAnimations];
    buttonsMoved = YES;
}

- (void)moveButtonsUp{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vButtons setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [UIView commitAnimations];
    buttonsMoved = NO;
}

- (void)closeEverything{
    [self closeSettings];
    [self closeMore];
}

- (IBAction)byYMM{
    if(![vcHolder inService]){
        NetworkStatus status = [reach currentReachabilityStatus];
        if(status == NotReachable){
            [self showNoNetwork];
        }else{
            holderIsOpenAndInService = YES;
            [vcHolder openYMM];
        }
    }else{
        holderIsOpenAndInService = YES;
        [vcHolder openYMM];
    }
}

- (IBAction)byVIN{
    NetworkStatus status = [reach currentReachabilityStatus];
    if(status == NotReachable){
        [self showNoNetwork];
    }else{
        holderIsOpenAndInService = YES;
        [vcHolder openVIN];
    }
}

- (IBAction)byProduct{
    NetworkStatus status = [reach currentReachabilityStatus];
    if(status == NotReachable){
        [self showNoNetwork];
    }else{
        [vcHolder openProd];
    }
}

- (void)closeHolder{
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcHolder.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    [UIView commitAnimations];
    if([vcHolder inService]){
        currTab = 0;
        [barMain setSelectedItem:[[barMain items] objectAtIndex:currTab]];
    }
    holderIsOpenAndInService = NO;
}

- (void)closeSettings{
    //[UIView beginAnimations:nil context:nil];
    //[UIView setAnimationDuration:.25];
    [vcSettings.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    //[UIView commitAnimations];
}

- (void)closeMore{
    //[UIView beginAnimations:nil context:nil];
    //[UIView setAnimationDuration:.25];
    [vcMore.view setTransform:CGAffineTransformMakeTranslation(self.view.frame.size.width, 0)];
    //[UIView commitAnimations];
}

- (void)openPdfViewer:(NSNotification *)notification{
    vcPdfViewer = [[VCPdfViewer alloc] initWithNibName:@"PdfViewer" bundle:nil];
    [vcPdfViewer.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    if([[notification object] isKindOfClass:[NSDictionary class]]){
        NSDictionary *dic = [notification object];
        [vcPdfViewer loadHtml:[dic objectForKey:@"html"] links:[[dic objectForKey:@"links"] boolValue]];
    }else{
        [vcPdfViewer loadPdf:[notification object]];
    }
    [self.view addSubview:vcPdfViewer.view];
    [vcPdfViewer.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcPdfViewer.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [UIView commitAnimations];
}

- (void)openTroubleshooting:(NSNotification *)notification{
    vcTroubleshooting = [[VCPdfViewer alloc] initWithNibName:@"PdfViewer" bundle:nil];
    [vcTroubleshooting.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [vcTroubleshooting loadHtml:[[notification object] objectForKey:@"html"] links:NO];
    [self.view addSubview:vcTroubleshooting.view];
    [vcTroubleshooting.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcTroubleshooting.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [UIView commitAnimations];
}

- (void)closePdfViewer{
    if(vcPdfViewer){
        [vcPdfViewer close];
    }
}

- (void)openWarnings:(NSNotification *)notification{
    vcWarnings = [[VCWarnings alloc] initWithNibName:@"Warnings" bundle:nil];
    [vcWarnings.view setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [vcWarnings loadWeb:[[notification object] objectForKey:@"html"]];
    [self.view addSubview:vcWarnings.view];
    [vcWarnings.view setTransform:CGAffineTransformMakeTranslation(0, self.view.frame.size.height)];
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:.25];
    [vcWarnings.view setTransform:CGAffineTransformMakeTranslation(0, 0)];
    [UIView commitAnimations];
}

- (void)attachmentsReturned:(NSNotification *)notification{
    if(stopFetching){
        return;
    }
    NSArray *attachments = [notification object];
    if([attachments count] > 0){
        [[ModuleDB sharedInstance] addAttachments:attachments];
    }
}

- (void)vehiclesReturned:(NSNotification *)notification{
    if(stopFetching){
        return;
    }
    NSArray *vehicles = [notification object];
    if([vehicles count] > 0){
        [[ModuleDB sharedInstance] addVehicles:vehicles];
    }
    NSDateFormatter *dFormat = [[NSDateFormatter alloc] init];
    [dFormat setDateFormat:@"yyyy-MM-dd"];
    NSString *date = [dFormat stringFromDate:[NSDate date]];
    [[NSUserDefaults standardUserDefaults] setObject:date forKey:@"newVehicleDate"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    [self stopLoading];
    NSArray *actions = [[ModuleDB sharedInstance] getActions];
    if([actions count] > 0){
        NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_updating"], @"hideCancel":@1};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:dic];
        [[ModuleSendActions sharedInstance] go:actions];
    }
}

- (void)sentActionsReturned{
    [[ModuleDB sharedInstance] deleteHistory];
    [self stopLoading];
}

- (void)latestUpdate:(NSNotification *)notification{
    if(stopFetching){
        return;
    }
    NSDictionary *dic = [notification object];
    NSDate *lastUpdate = [dic objectForKey:@"lastUpdate"];
    NSDate *localDate = [dic objectForKey:@"localDate"];
    if([lastUpdate laterDate:localDate] == lastUpdate){
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_newVehicleData"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actCancel = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actCancel];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_loadingResults"],  @"hideCancel":@0};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:dic];
            [[ModuleGetNewVehicles sharedInstance] go];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

- (void)updateFirmware{
    if([[ModuleBT sharedInstance] btObjectHC]){
        [[ModuleBT sharedInstance] updateFirmware];
    }else{
        [self showNoDevice:nil];
        [self stopLoading];
    }
}

- (void)firmwareUpdated{
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_firmwareUpdateComplete"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
        [self stopLoadSensor];
    });
}

- (void)vehiclesError:(NSNotification *)notification{
    if(stopFetching){
        return;
    }
    NSDictionary *dic = [notification object];
    dispatch_async(dispatch_get_main_queue(), ^{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:[dic objectForKey:@"title"] message:[dic objectForKey:@"text"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
        [self stopLoading];
    });
}

- (void)showNoNetwork{
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_noNetworkTitle"] message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_noNetworkText"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
}

- (void)showNoDevice:(NSNotification *)notification{
    //NSData *dataBTObject = [[NSUserDefaults standardUserDefaults] objectForKey:@"btObject"];
    //if(dataBTObject){
    //    [self connectToDevice:dataBTObject];
    //}else{
        fromDetail = NO;
        NSDictionary *dic = [notification object];
        if(dic){
            fromDetail = YES;
        }
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_notConnected"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actCancel = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actCancel];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            [self connectToDevice:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    //}
}

- (void)connectToDevice:(NSData *)dataBTObject{
    NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_connectToDevice"], @"hideCancel":@1};
    NSNotification *notification = [[NSNotification alloc] initWithName:@"StartLoading" object:dic userInfo:nil];
    [self startLoading:notification];
    [[ModuleBT sharedInstance] searchForDevices:dataBTObject obd:NO];
}

@end
