//
//  VCCheckCell.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/30/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCCheckCell.h"

@interface VCCheckCell ()

@end

@implementation VCCheckCell

- (void)viewDidLoad{
    [super viewDidLoad];
    [[_btn layer] setCornerRadius:5];
    [_btn setClipsToBounds:YES];
}

- (void)setup:(int)loc typ:(int)sentType{
    type = sentType;
    switch(type){
        case 0:
        case 1:
            [_btn setBackgroundImage:[UIImage imageNamed:@"btn_transmit.png"] forState:UIControlStateNormal];
            break;
    }
    [_btn setTag:loc];
    switch(loc){
        case 0:
            [lblTire setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_leftFront"]];
            break;
        case 1:
            [lblTire setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_rightFront"]];
            break;
        case 2:
            [lblTire setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_rightRear"]];
            break;
        case 3:
            [lblTire setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_leftRear"]];
            break;
        case 4:
            [lblTire setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_spare"]];
            break;
    }
    [lblPsi setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_poundsSquareInch"]];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"psiBar"]){
        [lblPsi setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_bar"]];
    }
}

- (IBAction)go{
    switch(type){
        case 0:
        case 1:
            [_btn setBackgroundImage:[UIImage imageNamed:@"btn_transmit.png"] forState:UIControlStateNormal];
            break;
    }
}

- (void)showResult:(NSData *)resultData freq:(NSString *)strFreq dupe:(BOOL)duplicate{
    const uint8_t *bytes = [resultData bytes];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"hexDec"]){
        [lblId setText:[NSString stringWithFormat:@"%hhu%hhu%hhu%hhu", bytes[4], bytes[5], bytes[6], bytes[7]]];
    }else{
        [lblId setText:[[NSString stringWithFormat:@"%02x%02x%02x%02x", bytes[4], bytes[5], bytes[6], bytes[7]] uppercaseString]];
    }
    float fPressure;
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"psiBar"]){
        fPressure = (float)(bytes[8] * .001);
    }else{
        fPressure = (float)(bytes[8] * 1.45038);
    }
    [lblPsi setText:[NSString stringWithFormat:@"%.2f", fPressure]];
    NSString *strTemp = [NSString stringWithFormat:@"%d", (int)bytes[9]];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"fahCel"]){
        float fTemp = (float)([strTemp intValue] - 32) * .555;
        int cTemp = (int)fTemp;
        strTemp = [NSString stringWithFormat:@"%d", cTemp];
    }
    [lblTemp setText:strTemp];
    [lblFreq setText:strFreq];
    NSString *strBat = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_low"];
    if(bytes[10] > 0){
        strBat = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"];
    }
    [lblBat setText:strBat];
    if(!duplicate){
        if(!_hasBeenRead){
            [self makeGreen];
        }else{
            [_btn setBackgroundColor:[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:0.0 alpha:1.0]];
            [prg setTrackTintColor:[UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:0.0 alpha:1.0]];
        }
    }else{
        [_btn setBackgroundColor:[UIColor colorWithRed:200.0/255.0 green:0 blue:0 alpha:1.0]];
        [prg setTrackTintColor:[UIColor colorWithRed:200.0/255.0 green:0 blue:0 alpha:1.0]];
    }
}

- (void)makeGreen{
    [_btn setBackgroundColor:[UIColor colorWithRed:0.0 green:100.0/255.0 blue:0.0 alpha:1.0]];
    [prg setTrackTintColor:[UIColor colorWithRed:0.0 green:100.0/255.0 blue:0.0 alpha:1.0]];
}

@end
