//
//  VCCreate.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/30/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "Relearn.h"
#import "SensorNameFreq.h"

@interface VCCreate : UIViewController{
    IBOutlet UILabel *lblInst;
    IBOutlet UILabel *lblId;
    IBOutlet UILabel *lblMhz;
    IBOutlet UILabel *lblId2;
    IBOutlet UILabel *lblMhz2;
    IBOutlet UIButton *btnProgram;
    IBOutlet UIButton *btnRelearnInst;
    NSMutableArray *metal;
    NSMutableArray *rubber;
    IBOutlet UIView *vChooseSensor;
    IBOutlet UILabel *lblMetal;
    IBOutlet UILabel *lblRubber;
    IBOutlet UITableView *tblMetal;
    IBOutlet UITableView *tblRubber;
    Relearn *relearn;
    SensorNameFreq *sensorNameFreq;
}

@property (assign) int state;

- (void)setup:(NSArray *)sentSensorNameFreqs rel:(Relearn *)sentRelearn;
- (IBAction)program;
- (IBAction)relearnInst;
- (void)closeSensorSelect;
- (void)openSensorSelect;

@end
