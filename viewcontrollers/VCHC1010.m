//
//  VCHC1010.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/6/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "VCHC1010.h"

@interface VCHC1010 ()

@end

@implementation VCHC1010

- (void)viewDidLoad {
    [super viewDidLoad];
    [[btnInteractive layer] setCornerRadius:5];
    [btnInteractive setClipsToBounds:YES];
    [[btnWhereBuy layer] setCornerRadius:5];
    [btnWhereBuy setClipsToBounds:YES];
    NSString *string = [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_aboutHC1000"];
    string = [string stringByReplacingOccurrencesOfString:@"\\'" withString:@"'"];
    [txvAbout setText:string];
    [btnWhereBuy setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_whereToBuy"] forState:UIControlStateNormal];
}

- (IBAction)whereToBuy{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.intellisens.com/distributor-locator/"]];
}

@end
