//
//  VCOBD.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/30/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import "VCOBD.h"

@interface VCOBD ()

@end

@implementation VCOBD

- (void)viewDidLoad{
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(checkResults:) name:@"CheckResults" object:nil];
    [lblLoc setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_location"]];
    [lblId setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_id"]];
    [lblPSI setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_poundsSquareInch"]];
    [lblMhz setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_mhz"]];
    [lblTemp setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_temp"]];
    [lblBat setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_battery"]];
    if([[NSUserDefaults standardUserDefaults] boolForKey:@"psiBar"]){
        [lblPSI setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_bar"]];
    }
    checkCells = [[NSMutableArray alloc] init];
    for(int i = 0; i < 5; i++){
        VCCheckCell *vcCheckCell = [[VCCheckCell alloc] initWithNibName:@"CheckCell" bundle:nil];
        [vcCheckCell.view setFrame:CGRectMake(0, 0, 398, 60)];
        [[vcCheckCell btn] setTag:i];
        [[vcCheckCell btn] addTarget:self action:@selector(check:) forControlEvents:UIControlEventTouchUpInside];
        [stkCheckCells addArrangedSubview:vcCheckCell.view];
        [vcCheckCell setup:i typ:0];
        [checkCells addObject:vcCheckCell];
    }
    [[btnRelearn layer] setCornerRadius:5];
    [btnRelearn setClipsToBounds:YES];
    CAGradientLayer *gradient = [CAGradientLayer layer];
    [gradient setFrame:btnRelearn.bounds];
    [gradient setColors:@[(id)[[UIColor colorWithRed:250.0/255.0 green:0 blue:0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:200.0/255.0 green:0 blue:0 alpha:1.0] CGColor], (id)[[UIColor colorWithRed:150.0/255.0 green:0 blue:0 alpha:1.0] CGColor]]];
    [btnRelearn.layer insertSublayer:gradient atIndex:0];
    [btnRelearn setTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_relearn"] forState:UIControlStateNormal];
    datIds = [[NSMutableArray alloc] initWithObjects:[NSNull null], [NSNull null], [NSNull null], [NSNull null], [NSNull null], nil];
}

- (void)check:(UIButton *)sender{
    if([datIds objectAtIndex:[sender tag]] == [NSNull null]){
        checkSensor = (int)[sender tag];
        [self setupCheck];
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_checkSensorAgain"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actCancel = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actCancel];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
            checkSensor = (int)[sender tag];
            [datIds replaceObjectAtIndex:checkSensor withObject:[NSNull null]];
            [self setupCheck];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
}

- (void)setupCheck{
    NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_checkingSensor"],  @"hideCancel":@1};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:dic];
    checkCount = 0;
    [self doCheck];
}

- (void)doCheck{
    NSDictionary *dic = @{@"action":@0};
    [[NSNotificationCenter defaultCenter] postNotificationName:@"CheckSensor" object:dic];
}

- (void)checkResults:(NSNotification *)notification{
    NSDictionary *dic = [notification object];
    NSData *resultData = [dic objectForKey:@"resultData"];
    NSData *currId = [resultData subdataWithRange:NSMakeRange(4, 4)];
    BOOL duplicate = NO;
    for(NSData *prevID in datIds){
        if([currId isEqual:prevID]){
            if(checkCount < 2){
                checkCount++;
                [self doCheck];
                return;
            }
            duplicate = YES;
            break;
        }
    }
    if(!duplicate){
        [datIds replaceObjectAtIndex:checkSensor withObject:currId];
    }else{
        UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:nil message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_duplicateSensor"] preferredStyle:UIAlertControllerStyleAlert];
        UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            [self dismissViewControllerAnimated:YES completion:nil];
        }];
        [ctrAlert addAction:actOk];
        [self presentViewController:ctrAlert animated:YES completion:nil];
    }
    VCCheckCell *vcCheckCell = [checkCells objectAtIndex:checkSensor];
    uint8_t mhz = (uint8_t)[[dic objectForKey:@"frequency"] unsignedCharValue];
    NSString *strFreq = @"315";
    if(mhz == 0x02){
        strFreq = @"433";
    }
    [vcCheckCell showResult:[dic objectForKey:@"resultData"] freq:strFreq dupe:duplicate];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"StopLoading" object:nil];
}

- (IBAction)relearn{
    [[ModuleBT sharedInstance] setDatIds:datIds];
    if([[ModuleBT sharedInstance] btObjectOBD]){
        if([[[ModuleBT sharedInstance] btObjectOBD] connected]){
            NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_performingRelearn"],  @"hideCancel":@1};
            [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:dic];
            [[ModuleBT sharedInstance] doRelearn];
        }else{
            [self showAlert];
        }
    }else{
        [self showAlert];
    }
}

- (void)showAlert{
    UIAlertController *ctrAlert = [UIAlertController alertControllerWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_obd"] message:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_connectOBD"] preferredStyle:UIAlertControllerStyleAlert];
    UIAlertAction *actCancel = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_cancel"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
    }];
    [ctrAlert addAction:actCancel];
    UIAlertAction *actOk = [UIAlertAction actionWithTitle:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
        [self dismissViewControllerAnimated:YES completion:nil];
        NSDictionary *dic = @{@"message":[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_connectToDevice"], @"hideCancel":@1};
        [[NSNotificationCenter defaultCenter] postNotificationName:@"StartLoading" object:dic];
        [[ModuleBT sharedInstance] searchForDevices:nil obd:YES];
    }];
    [ctrAlert addAction:actOk];
    [self presentViewController:ctrAlert animated:YES completion:nil];
}

@end
