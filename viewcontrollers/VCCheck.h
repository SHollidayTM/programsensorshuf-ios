//
//  VCCheck.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/30/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "VCCheckCell.h"

@interface VCCheck : UIViewController{
    IBOutlet UIButton *btnLeftFront;
    IBOutlet UIButton *btnRightFront;
    IBOutlet UIButton *btnRightRear;
    IBOutlet UIButton *btnLeftRear;
    IBOutlet UIButton *btnSpare;
    IBOutlet UITextView *txvLeftFront;
    IBOutlet UITextView *txvRightFront;
    IBOutlet UITextView *txvRightRear;
    IBOutlet UITextView *txvLeftRear;
    IBOutlet UITextView *txvSpare;
    int checkSensor;
    int checkCount;
    NSMutableArray *datIds;
    NSString *strTitle;
    NSString *strMsg;
}

- (IBAction)check:(UIButton *)sender;

@end
