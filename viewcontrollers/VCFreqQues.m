//
//  VCFAQ.m
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/6/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import "VCFreqQues.h"

@interface VCFreqQues ()

@end

@implementation VCFreqQues

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 9;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *holder = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    [holder setBackgroundColor:[UIColor blackColor]];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, tableView.frame.size.width-10, 40)];
    [lbl setTextColor:[UIColor whiteColor]];
    [lbl setFont:[UIFont boldSystemFontOfSize:12]];
    [lbl setNumberOfLines:2];
    switch(section){
        case 0:
            [lbl setText:[NSString stringWithFormat:@"1. %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_queOne"]]];
            break;
        case 1:
            [lbl setText:[NSString stringWithFormat:@"2. %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_queTwo"]]];
            break;
        case 2:
            [lbl setText:[NSString stringWithFormat:@"3. %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_queThree"]]];
            break;
        case 3:
            [lbl setText:[NSString stringWithFormat:@"4. %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_queFour"]]];
            break;
        case 4:
            [lbl setText:[NSString stringWithFormat:@"5. %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_queFive"]]];
            break;
        case 5:
            [lbl setText:[NSString stringWithFormat:@"6. %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_queSix"]]];
            break;
        case 6:
            [lbl setText:[NSString stringWithFormat:@"7. %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_queSeven"]]];
            break;
        case 7:
            [lbl setText:[NSString stringWithFormat:@"8. %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_queEight"]]];
            break;
        case 8:
            [lbl setText:[NSString stringWithFormat:@"9. %@", [[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_queNine"]]];
            break;
    }
    [holder addSubview:lbl];
    return holder;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell"];
    if(cell == nil){
        cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, 44)];
    }
    [cell setSelectionStyle:UITableViewCellSelectionStyleNone];
    UILabel *lbl = [[UILabel alloc] initWithFrame:CGRectMake(5, 2, tableView.frame.size.width-10, 84)];
    [lbl setFont:[UIFont systemFontOfSize:10]];
    [lbl setNumberOfLines:6];
    switch([indexPath section]){
        case 0:
            [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ansOne"]];
            break;
        case 1:
            [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ansTwo"]];
            break;
        case 2:
            [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ansThree"]];
            break;
        case 3:
            [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ansFour"]];
            break;
        case 4:
            [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ansFive"]];
            break;
        case 5:
            [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ansSix"]];
            break;
        case 6:
            [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ansSeven"]];
            break;
        case 7:
            [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ansEight"]];
            break;
        case 8:
            [lbl setText:[[[ModuleLanguage sharedInstance] phrases] objectForKey:@"m_ansNine"]];
            break;
    }
    [cell.contentView addSubview:lbl];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    switch([indexPath section]){
        case 5:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.products.intellisens.com/ConfigureIntelliSens"]];
            break;
        case 8:
            [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.intellisens.com/index.php?id=342&no_cache=1"]];
            break;
    }
}

@end
