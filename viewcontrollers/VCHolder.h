//
//  VCHolder.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 3/30/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"
#import "ModuleLanguage.h"
#import "VCByYMM.h"
#import "VCByVIN.h"
#import "VCByProduct.h"
#import "VCDeviceResults.h"
#import "VCDeviceDetail.h"
#import "VCService.h"
#import "VCCheck.h"
#import "VCProgram.h"
#import "VCSelect.h"
#import "VCCreate.h"
#import "VCCopy.h"
#import "VCCopySet.h"
#import "VCRelearn.h"
#import "VCTechDocs.h"
#import "VCOBD.h"
#import "SensorTPMS.h"
#import "ModuleGetRelearn.h"
#import "Attachment.h"
#import "ModuleBT.h"

@interface VCHolder : UIViewController <UINavigationBarDelegate>{
    IBOutlet UINavigationBar *barNav;
    Reachability *reach;
    UINavigationItem *itmFirst;
    BOOL inYMM;
    VCByYMM *vcByYMM;
    VCByVIN *vcByVIN;
    VCByProduct *vcByProduct;
    UINavigationItem *itmDeviceResults;
    VCDeviceResults *vcDeviceResults;
    UINavigationItem *itmDeviceDetail;
    VCDeviceDetail *vcDeviceDetail;
    UINavigationItem *itmService;
    UINavigationItem *itmCheck;
    VCCheck *vcCheck;
    UINavigationItem *itmProgram;
    VCProgram *vcProgram;
    UINavigationItem *itmSelect;
    VCSelect *vcSelect;
    UINavigationItem *itmCreate;
    VCCreate *vcCreate;
    UINavigationItem *itmCopy;
    VCCopy *vcCopy;
    UINavigationItem *itmCopySet;
    VCCopySet *vcCopySet;
    UINavigationItem *itmRelearn;
    VCRelearn *vcRelearn;
    UINavigationItem *itmTechDocs;
    VCTechDocs *vcTechDocs;
    UINavigationItem *itmOBD;
    VCOBD *vcOBD;
    Vehicle *vehicle;
    SensorTPMS *selectedSensor;
    SensorNameFreq *sensorNameFreq;
    Relearn *relearn;
    NSArray *programmables;
    NSArray *nonprogrammables;
    NSArray *serviceKits;
    NSArray *commandWords;
    uint8_t mhz;
    BOOL freqChosen;
    int behavior;
}

@property (nonatomic, strong) VCService *vcService;
@property (assign) BOOL inService;
@property (assign) BOOL stopFetching;

- (void)openYMM;
- (void)openVIN;
- (void)openProd;
- (void)popBack;
- (IBAction)close;

@end
