//
//  VCSelect.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/30/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "SensorNameFreq.h"
#import "ModuleDB.h"
#import "Make.h"
#import "Model.h"

@interface VCSelect : UIViewController{
    IBOutlet UIPickerView *pck;
    IBOutlet UIButton *btnGo;
    NSArray *sensorNameFreqs;
    SensorNameFreq *chosenSensorNameFreq;
}

- (void)setup:(NSArray *)sentSensorNameFreqs;
- (IBAction)go;

@end
