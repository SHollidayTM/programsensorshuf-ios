//
//  VCDeviceResults.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 11/24/15.
//  Copyright © 2015 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"
#import "Make.h"
#import "Model.h"
#import "SensorTPMS.h"
#import "ServiceKit.h"
#import "TCDevice.h"

@interface VCDeviceResults : UIViewController{
    int type;
    IBOutlet UISegmentedControl *segSort;
    IBOutlet UITableView *tbl;
    IBOutlet NSLayoutConstraint *tblTopConstraint;
    NSArray *heldProgrammables;
    NSArray *heldNonprogrammables;
    NSMutableArray *programmables;
    NSMutableArray *nonprogrammables;
    NSArray *serviceKits;
    NSString *frequency;
}

- (void)setup:(NSArray *)sentProgrammables non:(NSArray *)sentNonprogrammables kit:(NSArray *)sentServiceKits typ:(int)sentType;
- (IBAction)filterFreq:(UISegmentedControl *)seg;

@end
