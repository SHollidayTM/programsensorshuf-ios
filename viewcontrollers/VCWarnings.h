//
//  VCWarnings.h
//  progsensors-iphone
//
//  Created by Scott Holliday on 4/22/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ModuleLanguage.h"

@interface VCWarnings : UIViewController{
    IBOutlet UIWebView *webWarnings;
    IBOutlet UIButton *btnClose;
}

- (void)loadWeb:(NSString *)sentHtml;
- (IBAction)close;

@end
