//
//  main.m
//  progsensors-ipad
//
//  Created by Scott Holliday on 4/8/16.
//  Copyright © 2016 Tiremetrix. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
